<?php
/**
 * FooBoox Google Analytics Class
 */

class FooBox_Google_Analytics {

	/**
	 * Initialize the class
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		if ( is_admin() ) {
			add_action( 'foobox_pre_tab', array( $this, 'add_ga_settings_tab' ) );
		}
		add_filter( 'foobox_extra_scripts_post', array( $this, 'add_script' ) );
	}

	public function add_script( $js ) {
		//if we have disabled all scripts then do nothing
		if (!apply_filters('foobox_enqueue_scripts', true)) return $js;

		$js .= $this->generate_javascript();

		return $js;
	}

	public function add_ga_settings_tab( $tab_id ) {
		if ( $tab_id === 'demo' ) {
			$foobox = $GLOBALS['foobox'];
			if ( $foobox === false ) {
				return;
			}

			$foobox->admin_settings_add_tab( 'ga', __( 'Google Analytics', 'foobox' ) );

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_track_pageviews',
					'title'   => __( 'Enable Pageview Tracking', 'foobox' ),
					'desc'    => __( 'If enabled, a pageview will be recorded when an image is opened in FooBox.', 'foobox' ),
					'default' => 'on',
					'type'    => 'checkbox',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_deeplink_pageviews',
					'title'   => __( 'Track Deeplink Pageviews', 'foobox' ),
					'desc'    => __( 'If both pageview tracking and FooBox deeplinking is enabled, then the deeplink URL will be recorded in Google Analytics.', 'foobox' ),
					'default' => 'on',
					'type'    => 'checkbox',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_track_events',
					'title'   => __( 'Enable Event Tracking', 'foobox' ),
					'desc'    => __( 'If enabled, a custom event will be recorded when an image is opened in FooBox.', 'foobox' ),
					'default' => 'on',
					'type'    => 'checkbox',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_event_category',
					'title'   => 'Event Category',
					'desc'    => __( 'Used in event tracking, this is the name for the group of objects you want to track. In this scenario, the group of objects are your images shown within FooBox.', 'foobox' ),
					'default' => 'Images',
					'type'    => 'text',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_event_action',
					'title'   => 'Event Action',
					'desc'    => __( 'Used in event tracking, this is the name for the type of user interaction. In this scenario, viewing the image within FooBox.', 'foobox' ),
					'default' => 'View',
					'type'    => 'text',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_track_social',
					'title'   => __( 'Enable Social Tracking', 'foobox' ),
					'desc'    => __( 'If enabled, all social shares from FooBox will be tracked as an event in Google Analytics.', 'foobox' ),
					'default' => 'on',
					'type'    => 'checkbox',
					'tab'     => 'ga'
				)
			);

			$foobox->admin_settings_add(
				array(
					'id'      => 'ga_social_category',
					'title'   => 'Social Category',
					'desc'    => __( 'Used in social tracking, this is the category used when tracking social share events from FooBox.', 'foobox' ),
					'default' => 'Social Share',
					'type'    => 'text',
					'tab'     => 'ga'
				)
			);

			if ( $foobox->is_option_checked( 'enable_debug' ) ) {
				$foobox->admin_settings_add(
					array(
						'id'    => 'ga_output',
						'title' => __( 'Javscript Output (Debug)', 'foobox' ),
						'type'  => 'html',
						'desc'  => '<pre>' . htmlentities( $this->generate_javascript() ) . '</pre>',
						'tab'   => 'ga'
					)
				);
			}
		}
	}

	public function generate_javascript() {
		$foobox = $GLOBALS['foobox'];
		if ( $foobox === false ) {
			return '';
		}

		$track_pageviews = $foobox->is_option_checked( 'ga_track_pageviews', true );
		$track_events    = $foobox->is_option_checked( 'ga_track_events', true );
		$track_deeplinks = ! $foobox->is_option_checked( 'disble_deeplinking' ) && $foobox->is_option_checked( 'ga_deeplink_pageviews', true );
		$event_category  = $foobox->get_option( 'ga_event_category', 'Images' );
		$event_action    = $foobox->get_option( 'ga_event_action', 'View' );
		$track_social    = $foobox->is_option_checked( 'ga_track_social', true );
		$social_category = $foobox->get_option( 'ga_social_category', 'Social Share' );

		if ( $track_pageviews === false && $track_events === false && $track_social === false ) {
			//got nothing to do here
			return '';
		}

		$ga_js        = '';
		$gaq_js       = '';
		$ga_listeners = array();

		if ( $track_pageviews === true ) {
			$ga_listeners['pages'] = "\"foobox.afterLoad\": FOOBOX.ga.afterLoad";
			if ( $track_deeplinks ) {
				$ga_js  .= "ga('send', 'pageview', location.pathname + location.search  + location.hash);
				";
				$gaq_js .= "_gaq.push(['_trackPageview', location.pathname + location.search  + location.hash]);
				";
			} else {
				$ga_js  .= "ga('send', 'pageview', trackUrl);
				";
				$gaq_js .= "_gaq.push(['_trackPageview', trackUrl]);
				";
			}
		}

		if ( $track_events === true ) {
			if ( ! array_key_exists( 'pages', $ga_listeners ) ) {
				$ga_listeners['pages'] = "\"foobox.afterLoad\": FOOBOX.ga.afterLoad";
			}
			$ga_js  .= "ga('send', 'event', '$event_category', '$event_action', e.fb.item.url);";
			$gaq_js .= "_gaq.push(['_trackEvent', '$event_category', '$event_action', e.fb.item.url]);";
		}

		if ( $track_social === true ) {
			$ga_listeners['social'] = "\"foobox.socialClicked\": FOOBOX.ga.socialClicked";
		} else {
			$ga_social = '';
		}

		$base_url = untrailingslashit( home_url() );

		$listeners = implode( ',', $ga_listeners );

		$js = "
/* FooBox Google Analytics code */
if (!FOOBOX.ga){
	FOOBOX.ga = {
		init: function(){
			var listeners = {
				{$listeners}
			};
			$(\".fbx-instance\").off(listeners).on(listeners);
		},
		afterLoad: function(e){
			var trackUrl = e.fb.item.url.replace('{$base_url}', '');
			if (typeof ga != 'undefined') {
				{$ga_js}
			} else if (typeof _gaq != 'undefined') {
				{$gaq_js}
			}
		},
		socialClicked: function(e){
			if (typeof ga != 'undefined') {
				ga('send', 'event', '{$social_category}', e.fb.network, e.fb.item.url);
			} else if (typeof _gaq != 'undefined') {
				_gaq.push(['_trackEvent', '{$social_category}', e.fb.network, e.fb.item.url]);
			}
		}
	};
}
FOOBOX.ga.init();		
";

		return $js;
	}
}