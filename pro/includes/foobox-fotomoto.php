<?php
/*
Adds Fotomoto compatibility
*/

if ( !class_exists( 'Foobox_Fotomoto' ) ) {

	class Foobox_Fotomoto {

		const JS  = 'foobox-fotomoto.js';
		const CSS = 'foobox-fotomoto.css';

		function __construct() {

			if ( !is_admin() ) {
				add_action( 'plugins_loaded', array( $this, 'frontend_init' ) );
			}

			add_filter( 'foobox_extra_scripts_post', array( $this, 'add_script' ) );
		}

		function frontend_init() {
			//if fotomoto plugin is not installed get out
			if ( !function_exists( 'fotomoto_is_scripted' ) ) return;

			add_action( 'wp_enqueue_scripts', array($this, 'frontend_scripts'), 99 );
			add_action( 'wp_enqueue_scripts', array($this, 'frontend_styles'), 99 );
		}

		public function add_script( $js ) {
			//if fotomoto plugin is not installed get out
			if ( !function_exists( 'fotomoto_is_scripted' ) ) return $js;

			//if we have disabled all scripts then do nothing
			if (!apply_filters('foobox_enqueue_scripts', true)) return $js;

			$js .= '
/* FooBox Fotomoto code */
if (!FOOBOX.fotomoto){
    FOOBOX.fotomoto = {
        beforeLoad: function(e){
            e.fb.modal.find(\'.FotomotoToolbarClass\').remove();
        },
        afterLoad: function(e){
            var item = e.fb.item,
                container = e.fb.modal.find(\'.fbx-item-current\');
            if (window.FOOBOX_FOTOMOTO){
                FOOBOX_FOTOMOTO.show(item, container);
            }
        },
        init: function(){
            jQuery(\'.fbx-instance\').off(\'foobox.beforeLoad\', FOOBOX.fotomoto.beforeLoad)
                .on(\'foobox.beforeLoad\', FOOBOX.fotomoto.beforeLoad);

            jQuery(\'.fbx-instance\').off(\'foobox.afterLoad\', FOOBOX.fotomoto.afterLoad)
                .on(\'foobox.afterLoad\', FOOBOX.fotomoto.afterLoad);
        }
    }
}
FOOBOX.fotomoto.init();
			';

			return $js;
		}

		function change_foobox_options($options) {

			$foobox = $GLOBALS['foobox'];
			if ($foobox === false) return;
			$fotomoto_position = $foobox->get_option('fotomoto_position', 'top');

			if ( !array_key_exists('modalClass', $options) ) {
				$options['modalClass'] = 'modalClass: "fbx-fotomoto-' . $fotomoto_position . '"';
			} else {
				$existing_option = $options['modalClass'];
				$existing_option = str_replace('modalClass: "', 'modalClass: "fbx-fotomoto-' . $fotomoto_position . ' ', $existing_option);
				$options['modalClass'] = $existing_option;
			}

			return $options;
		}

		function add_settings_tab($tab_id) {
			if ($tab_id === 'advanced') {
				$foobox = Foobox_Free::get_instance();
				if ($foobox === false) return;

				$foobox->settings()->add_tab('fotomoto', __('Fotomoto', 'foobox-fotomoto'));

				$foobox->settings()->add_section_to_tab('fotomoto', 'fotomoto-general', __('Fotomoto', 'foobox-fotomoto'));

				$position_choices           = array();
				$position_choices['top']    = __( 'Top', 'foobox-fotomoto' );
				$position_choices['bottom'] = __( 'Bottom', 'foobox-fotomoto' );

				$foobox->settings()->add_setting( array(
					'id'      => 'fotomoto_position',
					'title'   => __( 'Fotomoto Link Position', 'foobox-fotomoto' ),
					'desc'    => __('The Fotomoto links can either be at the top or the bottom of the FooBox modal.<br /><br /><strong>PLEASE NOTE:</strong> You will need to change the link position so that they do not conflict with the FooBox captions or social icons.<br />It is recommedended to set the social buttons vertical position to "Above", leave the captions and set the Fotomoto link position to "top".', 'foobox-fotomoto'),
					'default' => 'top',
					'type'    => 'radio',
					'section' => 'fotomoto-general',
					'choices' => $position_choices,
					'tab'     => 'FotoMoto'
				) );
			}
		}

		function frontend_scripts() {
			//if foobox scripts not added get out
			if ( !apply_filters( 'foobox_enqueue_scripts', true ) ) return;

			$js_src_url = plugins_url( 'js/' . self::JS, FOOBOX_FILE );

			wp_register_script(
				$handle = 'foobox-fotomoto',
				$src = $js_src_url,
				$ver = FOOBOX_BASE_VERSION );

			wp_enqueue_script( 'foobox-fotomoto' );
		}

		function frontend_styles() {
			//if foobox styles not added get out
			if ( !apply_filters( 'foobox_enqueue_styles', true ) ) return;

			//if fotomoto scripts not added to page, get out
			//if ( !fotomoto_is_scripted() ) return;

			$css_src_url = plugins_url( 'css/' . self::CSS, FOOBOX_FILE );

			wp_register_style(
				$handle = 'foobox-fotomoto',
				$src = $css_src_url,
				$ver = FOOBOX_BASE_VERSION );

			wp_enqueue_style( 'foobox-fotomoto' );
		}
	}
}