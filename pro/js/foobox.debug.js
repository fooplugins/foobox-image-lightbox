/**!
 * FooBox - A jQuery responsive lightbox plugin
 * @version 2.4.6
 * @link http://fooplugins.com/plugins/foobox-jquery
 * @copyright Steven Usher & Brad Vincent 2020
 * @license Released under the MIT license.
 * You are free to use FooBox jQuery in personal projects as long as this copyright header is left intact.
 */
/*jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
(function ($, window, undefined) {
	if (!$ || !window) { return; } // if jquery or no window object exists exit

	/**
	 * The core namespace the plugin code is registered with.
	 * @global
	 * @namespace FooBox
	 */
	window.FooBox = {};

	/**
	 * The instance of jQuery the plugin is registered with.
	 * @memberof FooBox.
	 * @name $
	 * @type {jQuery}
	 */
	FooBox.$ = $;

	/**
	 * The plugin version.
	 * @memberof FooBox.
	 * @name version
	 * @type {string}
	 */
	FooBox.version = '2.4.6';

	/**
	 * The instances of the plugin in the page
	 * @memberof FooBox.
	 * @name instances
	 * @type {[]}
	 */
	FooBox.instances = [];

	/**
	 * The default options for the plugin.
	 * @memberof FooBox.
	 * @name defaults
	 * @type {Object}
	 */
	FooBox.defaults = {
		/** @type {Object} - An object containing affiliate related options. */
		affiliate: {
			/** @type {boolean} - A Boolean indicating whether or not to enable the affiliate link for FooBox. */
			enabled: true,
			/** @type {string} - A String to use as the prefix for the affiliate link. */
			prefix: 'Powered by ',
			/** @type {string} - A String containing the affiliate URL. */
			url: 'http://fooplugins.com/plugins/foobox/'
		},
		/** @type {boolean} - A Boolean indicating whether or not to always init the plugin instead of using reinit. */
		alwaysInit: true,
		/** @type {boolean} - A Boolean indicating whether or not to close FooBox when the overlay is clicked. */
		closeOnOverlayClick: true,
		/** @type {string} - A String used as a class name that will be added to every instance of FooBox that is created */
		containerCssClass: 'fbx-instance',
		/** @type {string} - A String used to format the image counter. The tokens "%index" and "%total" will be used to substitute the current values. */
		countMessage: 'item %index of %total',
		/** @type {string} -  */
		error: 'Could not load the item',
		/** @type {string} - A Selector used to further filter the elements found by the selector option. */
		excludes: '.fbx-link, .nofoobox',
		/** @type {string} - A Selector used to find elements that need to open FooBox instances, these elements can be placed any where in the DOM as long as they contain a data-foobox attribute with a valid selector. */
		externalSelector: 'a[data-foobox],input[data-foobox]',
		/** @type {boolean} - A Boolean indicating whether or not images are scaled up to fit to the screen size (this could look awkward when set to true with low resolution images). */
		fitToScreen: false,
		/** @type {boolean} - A Boolean indicating whether or not to hide the default browser scroll bars when FooBox is visible. */
		hideScrollbars: true,
		/** @type {number} - A Number determining the amount of milliseconds to add to each item. This is primarily used only in development however it is exposed as an option that you can adjust. */
		loadDelay: 0,
		/** @type {number} - A Number determining the minimum amount of milliseconds before the loader is displayed. If an item takes longer than X milliseconds to load then the loader will be shown. */
		loaderTimeout: 600,
		/** @type {string} - One or more space-separated classes to be added to the class attribute of the FooBox modal element. */
		modalClass: '',
		/** @type {boolean} -  */
		preload: false,
		/** @type {string} - The rel attribute String value used during the initialization of a FooBox to find any additional elements to include in the current instance. */
		rel: null,
		/** @type {number} -  */
		resizeSpeed: 300,
		/** @type {string} - A Selector used to match an item to display in FooBox. */
		selector: 'a',
		/** @type {boolean} - A Boolean indicating whether or not to show the previous and next buttons. */
		showButtons: true,
		/** @type {boolean} - A Boolean indicating whether or not to show the countMessage in FooBox. */
		showCount: true,
		/** @type {string} - The FooBox style class to use. */
		style: 'fbx-rounded',
		/** @type {string} - The FooBox theme class to use. */
		theme: 'fbx-light',
		/** @type {number} - A Number determining the amount of milliseconds the transition in animation takes to complete. */
		transitionInSpeed: 200,
		/** @type {number} - A Number determining the amount of milliseconds the transition out animation takes to complete. */
		transitionOutSpeed: 200,
		/** @type {function} - A callback function that is called after foobox is initialized. */
		initCallback: null,
		/** @type {boolean} - Whether or not to allow the looping of images using the previous and next buttons. */
		loop: true
	};

	/**
	 * This small helper function was created due to the instanceof method of checking an object failing on 1 particular site....
	 * @param {Object} obj - The object to check if it's a jQuery object.
	 * @returns {boolean}
	 */
	FooBox.isjQuery = function(obj){
		return (obj instanceof jQuery) ? true : (obj && obj.jquery);
	};

	/**
	 * Checks if the supplied value is a function.
	 * @memberof FooBox.
	 * @function isFn
	 * @param {*} value - The value to check.
	 * @returns {boolean}
	 */
	FooBox.isFn = function(value){
		return value === window.alert || '[object Function]' === Object.prototype.toString.call(value);
	};

	/**
	 * Checks if the supplied value is an array.
	 * @memberof FooBox.
	 * @function isArray
	 * @param {*} value - The value to check.
	 * @returns {boolean}
	 */
	FooBox.isArray = function(value){
		return '[object Array]' === Object.prototype.toString.call(value);
	};

	/**
	 * Checks if the value exists in the array.
	 * @memberof FooBox.
	 * @function inArray
	 * @param {*} needle - The value to look for.
	 * @param {[]} haystack - The array to search.
	 * @returns {number}
	 */
	FooBox.inArray = function(needle, haystack){
		if (!FooBox.isArray(haystack)) return -1;
		return haystack.indexOf(needle);
	};

	/**
	 * @summary Removes whitespace from both ends of the target string.
	 * @memberof FooBox.
	 * @function trim
	 * @param {string} target - The string to trim.
	 * @returns {string}
	 */
	FooBox.trim = function(target){
		return '[object String]' === Object.prototype.toString.call(target) ? target.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '') : '';
	};

	/**
	 * Small ready function to circumvent external errors blocking jQuery's ready.
	 * @memberof FooBox.
	 * @function ready
	 * @param {Function} callback - The function to call when the document is ready.
	 */
	FooBox.ready = function(callback) {
		function onready(){
			try { callback.call(window, FooBox.$); }
			catch(err) { console.error(err); }
		}
		if (Function('/*@cc_on return true@*/')() ? document.readyState === "complete" : document.readyState !== "loading") onready();
		else document.addEventListener('DOMContentLoaded', onready, false);
	};

	/**
	 * Imitates .NET's String.format method, arguments that are not strings will be auto-converted to strings.
	 * @param {string} formatString - The format string to use.
	 * @param {*} arg1 - An argument to format the string with.
	 * @param {*} [argN] - Additional arguments to format the string with.
	 * @returns {string}
	 */
	FooBox.format = function (formatString, arg1, argN) {
		var s = arguments[0], i, reg;
		for (i = 0; i < arguments.length - 1; i++) {
			reg = new RegExp("\\{" + i + "\\}", "gm");
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};

	/** @namespace - Contains logic to determine the browser. */
	FooBox.browser = {
		/** @type {boolean} - A Boolean indicating whether or not the current browser is Internet Explorer. */
		isIE: false,
		/** @type {boolean} - A Boolean indicating whether or not the current browser is Chrome. */
		isChrome: false,
		/** @type {boolean} - A Boolean indicating whether or not the current browser is Safari. */
		isSafari: false,
		isLtSafari10: false,
		/** @type {number} - A Number indicating the current Internet Explorer browser version. */
		version: 0,
		/** @type {string} - A String containing classes to be appended to the modal depending on the browser. */
		css: '',
		/** @type {boolean} - A boolean indicating whether or not we are on an iOS device. */
		iOS: false,
		/** @type {boolean} - A boolean indicating whether or not we are on a Mac device. */
		Mac: false,
		/** @type {boolean} - A boolean indicating whether or not we are on a mobile device. */
		isMobile: (function(a){
			return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)
			||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)));
		})(navigator.userAgent||navigator.vendor||window.opera),
		/**
		 * Checks the browser vendor and version and sets certain flags and classes if it's Internet Explorer.
		 */
		check: function () {
			var app = navigator.appVersion.toLowerCase(), ua = navigator.userAgent.toLowerCase(), p = navigator.platform;
			FooBox.browser.iOS = /(iPad|iPhone|iPod)/g.test(p);
            FooBox.browser.iOS12OrBelow = false;
			if (FooBox.browser.iOS) {
				FooBox.browser.css += ' fbx-ios';
                FooBox.browser.iOS12OrBelow = (function () {
                    var v = app.match(/OS (\d+)_(\d+)_?(\d+)?/i);
                    v = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
                    return v[0] <= 12;
                })();
                if (FooBox.browser.iOS12OrBelow){
                    FooBox.browser.css += ' fbx-ios-12-or-below';
                }
            }
			FooBox.browser.Mac = /(Mac)/g.test(p);
			if (FooBox.browser.Mac) {
				FooBox.browser.css += ' fbx-mac';
			}
			FooBox.browser.isChrome = app.indexOf("chrome") > -1 || app.indexOf("crios") > -1;
			if (FooBox.browser.isChrome){
				FooBox.browser.css += ' fbx-chrome';
			}
			FooBox.browser.isSafari = app.indexOf("safari") > -1 && !FooBox.browser.isChrome;
			if (FooBox.browser.isSafari) {
				FooBox.browser.css += ' fbx-safari';
				FooBox.browser.isLtSafari10 = /constructor/i.test(window.HTMLElement);
			}
			FooBox.browser.isFirefox = ua.indexOf("firefox") > -1;
			if (FooBox.browser.isFirefox){
				FooBox.browser.css += ' fbx-firefox';
			}
			var msie = ua.indexOf('msie '), trident = ua.indexOf('trident/'), edge = ua.indexOf('edge/');
			FooBox.browser.isIE = msie > -1 || trident > -1 || edge > -1;
			if (FooBox.browser.isIE) {
				if (msie > -1) FooBox.browser.version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				else if (trident > -1) {
					var rv = ua.indexOf('rv:');
					FooBox.browser.version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				} else if (edge > -1) FooBox.browser.version = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				FooBox.browser.css = 'fbx-ie fbx-ie' + FooBox.browser.version;
			}
		},
		/**
		 * Checks if the current browser supports CSS3 transitions.
		 * @returns {boolean} - True if the browser supports CSS3 transitions.
		 */
		supportsTransitions: function() {
			var b = document.body || document.documentElement;
			var s = b.style;
			var p = 'transition', v;
			if(typeof s[p] == 'string') {return true; }

			// Tests for vendor specific prop
			v = ['Moz', 'Webkit', 'Khtml', 'O', 'ms'];
			p = p.charAt(0).toUpperCase() + p.substr(1);
			for(var i=0; i<v.length; i++) {
				if(typeof s[v[i] + p] == 'string') { return true; }
			}
			return false;
		},
		/**
		 * Retrieves the correct transitionend event to use for the current browser.
		 * @returns {(string|null)}
		 */
		transitionEnd: function(){
			var t;
			var el = document.createElement('fakeelement');
			var transitions = {
					'transition':'transitionend',
					'OTransition':'oTransitionEnd',
					'MozTransition':'transitionend',
					'WebkitTransition':'webkitTransitionEnd'
			};

			for(t in transitions){
				if (!transitions.hasOwnProperty(t)) { continue; }
				if( el.style[t] !== undefined ){
						return transitions[t];
					}
			}
			return null;
		}
	};
	FooBox.browser.check();

	/**
	 * Takes the supplied URL (partial or full) an returns a fully qualified url for it.
	 * @param {string} url - The url to fully qualify.
	 * @returns {string}
	 */
	FooBox.qualifiedURL = function(url){
		var a = document.createElement('a');
		a.href = url;
		return a.href;
	};

	/**
	 * Augmented jQuery.Event object containing additional FooBox properties.
	 * @type {{instance: FooBox.Instance, modal: FooBox.Modal, options: Object, handled: boolean}}
	 */
	jQuery.Event.prototype.fb = {
		instance: null,
		modal: null,
		options: null,
		handled: false
	};

	/**
	 * Raises an event on the given instance of FooBox appending the args to the event.fb namespace.
	 * @param {FooBox.Instance} instance - The instance of FooBox to raise the event on.
	 * @param {string} event - The name of the event to raise, this can include namespaces.
	 * @param {Object} [args] - An object containing additional values to be merged into the event.fb namespace.
	 * @returns {jQuery.Event} - The jQuery.Event object used to raise the event.
	 */
	FooBox.raise = function (instance, event, args) {
		args = args || {};
		var e = $.Event(event);
		e.fb = {};
		e.fb.instance = instance;
		e.fb.modal = instance.modal.element;
		e.fb.options = instance.options;
		$.extend(true, e.fb, args);
		instance.element.one(event,function (e) {e.stopPropagation();}).trigger(e);
		return e;
	};

	/** @namespace - Common logic for merging objects and getting and setting there values. */
	FooBox.options = {
		/**
		 * Determine whether or not the specified name is a multipart property name, basically just checking if the name contains the separator.
		 * @param {string} name - The name to check is multipart.
		 * @param {string} separator - The separator used to determine multiple parts.
		 * @returns {boolean} - True if the name contains the separator.
		 */
		isMultipart: function (name, separator) {
			return typeof name === 'string' && name.length > 0 && name.indexOf(separator) !== -1;
		},
		/**
		 * Checks if the supplied obj has any of its own properties, i.e. non inherited properties.
		 * @param {Object} obj - The object to check for non-inherited properties.
		 * @returns {boolean} - True if the obj has its own non-inherited properties.
		 */
		hasProperties: function (obj) {
			if (typeof obj !== 'object') { return false; }
			var prop;
			for (prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					return true;
				}
			}
			return false;
		},
		/**
		 * Gets the value of the property specified by the name from the supplied obj.
		 * @param {Object} obj - The object to retrieve the property value from.
		 * @param {string} name - The name of the property to get. Child properties are delimited with a period [.]
		 * @returns {*} - The value of the property retrieved.
		 */
		get: function (obj, name) {
			if (FooBox.options.isMultipart(name, '.')) {
				var propName = name.substring(0, name.indexOf('.'));
				var remainder = name.substring(name.indexOf('.') + 1);
				obj[propName] = obj[propName] || {};
				return FooBox.options.get(obj[propName], remainder);
			}
			return obj[name];
		},
		/**
		 * Sets the value of the property specified by the name on the supplied obj.
		 * @param {Object} obj - The object to set the property value on.
		 * @param {string} name - The name of the property to set. Child properties are delimited with a period [.]
		 * @param {*} value - The value to set the property to.
		 */
		set: function (obj, name, value) {
			if (FooBox.options.isMultipart(name, '.')) {
				var propName = name.substring(0, name.indexOf('.'));
				var remainder = name.substring(name.indexOf('.') + 1);
				obj[propName] = obj[propName] || {};
				FooBox.options.set(obj[propName], remainder, value);
			} else {
				obj[name] = value;
			}
		},
		/**
		 * Wrote this as jQuery.extend merges arrays by index rather than overwriting them. This will not merge nested arrays.
		 * @param {Object} base - An object that will receive the new properties if additional objects are passed in.
		 * @param {Object} object1 - An object containing additional properties to merge in.
		 * @param {Object} [objectN] - Additional objects containing properties to merge in.
		 * @returns {Object} - The modified base object is returned.
		 */
		merge: function (base, object1, objectN) {
			var args = Array.prototype.slice.call(arguments), i;
			base = args.shift();
			object1 = args.shift();

			FooBox.options._merge(base, object1);
			for (i = 0; i < args.length; i++) {
				objectN = args[i];
				FooBox.options._merge(base, objectN);
			}

			return base;
		},
		/** @private */
		_merge: function (base, changes) {
			var prop;
			for (prop in changes) {
				if (changes.hasOwnProperty(prop)) {
					if (FooBox.options.hasProperties(changes[prop]) && !$.isArray(changes[prop])) {
						base[prop] = base[prop] || {};
						FooBox.options._merge(base[prop], changes[prop]);
					} else if ($.isArray(changes[prop])) {
						base[prop] = [];
						$.extend(true, base[prop], changes[prop]);
					} else {
						base[prop] = changes[prop];
					}
				}
			}
		}
	};

	/** @namespace - All the core logic for loading and managing objects is contained here. */
	FooBox.objects = {
		/** @type {Object} - An object containing all registered object arrays. */
		registered: {},
		ensure: function(name){
			return FooBox.objects.registered[name] = FooBox.objects.registered[name] || [];
		},
		/**
		 * Registers an obj and its default options with FooBox.
		 * @param {string} name - The name of the collection to register the obj under.
		 * @param {function} obj - The obj to register.
		 * @param {Object} [defaults] - The default options to merge with the FooBox's default options.
		 * @returns {boolean} - True if the addon is registered.
		 */
		register: function (name, obj, defaults) {
			var arr = FooBox.objects.ensure(name);
			arr.push(obj);
			if (typeof defaults === 'object') { $.extend(true, FooBox.defaults, defaults); }
			return true;
		},
		/**
		 * Loops through the specified registered objects and inits new instances for the supplied FooBox.
		 * @param {string} name - The name of the collection to register the obj under.
		 * @param {FooBox.Instance} instance - The instance of FooBox to load objects for.
		 * @returns {Array} - An array containing all the loaded objects for the instance.
		 */
		load: function (name, instance) {
			var loaded = [], registered, i, arr = FooBox.objects.ensure(name);
			for (i = 0; i < arr.length; i++) {
				try {
					registered = arr[i];
					loaded.push(new registered(instance));
				} catch (err) {
					console.error(err);
				}
			}
			instance.objects = instance.objects || {};
			instance.objects[name] = loaded;
		},
		/**
		 * Loops through the specified registered objects for the FooBox instance and calls the method passing in any additional arguments.
		 * @param {string} name - The name of the collection to register the obj under.
		 * @param {FooBox.Instance} instance - The instance of FooBox to call the method on.
		 * @param {string} method - The method to call.
		 * @param {*} [arg1] - An optional argument to pass through to the method.
		 * @param {*} [argN] - Additional optional arguments.
		 */
		call: function (name, instance, method, arg1, argN) {
			var args = Array.prototype.slice.call(arguments), obj;
			name = args.shift();
			instance = args.shift();
			method = args.shift();

			var arr = instance.objects[name] || [];
			for (var i = 0; i < arr.length; i++) {
				try {
					obj = arr[i];
					if (!FooBox.isFn(obj[method])) { continue; }
					obj[method].apply(obj, args);
				} catch (err) {
					console.error(err);
				}
			}
		},
		/**
		 * Retrieves an instance of an object from the supplied FooBox by type.
		 * @param {string} name - The name of the collection to get the obj from.
		 * @param {FooBox.Instance} instance - The instance of FooBox to retrieve the handler from.
		 * @param {function} where - The function to use to find the object to retrieve. The first parameter passed is the current iterated item.
		 * @returns {Object}
		 */
		get: function (name, instance, where) {
			var i, arr = instance.objects[name];
			if (!arr || !FooBox.isFn(where)) { return null; }
			for (i = 0; i < arr.length; i++) {
				if (where(arr[i])) {
					return arr[i];
				}
			}
			return null;
		}
	};

	/** @namespace - The code for loading and managing addons is contained here. */
	FooBox.addons = {
		_ns: 'addons',
		/**
		 * Simple validation of the addon to make sure any members called by FooBox actually exist.
		 * @param {function} obj - The function containing the addon logic.
		 * @returns {boolean} - True if the addon is valid.
		 */
		validate: function (obj) {
			if (!FooBox.isFn(obj)) {
				console.error(FooBox.format('Expected type "function", received type "{0}".', typeof obj));
				return false;
			}
			return true;
		},
		register: function(obj, defaults){
			if (!FooBox.addons.validate(obj)) {
				console.error('Failed to register the addon.');
				return false;
			}
			return FooBox.objects.register(FooBox.addons._ns, obj, defaults);
		},
		load: function(instance){
			FooBox.objects.load(FooBox.addons._ns, instance);
		},
		call: function(instance, method, arg1, argN){
			var args = Array.prototype.slice.call(arguments);
			args.unshift(FooBox.addons._ns);
			FooBox.objects.call.apply(this, args);
		}
	};

	/** @namespace - The code for loading and managing handlers is contained here. */
	FooBox.handlers = {
		_ns: 'handlers',
		/**
		 * Simple validation of the handler to make sure any members called by FooBox actually exist.
		 * @param {function} obj - The function containing the handler logic.
		 * @returns {boolean} - True if the handler is valid.
		 */
		validate: function (obj) {
			if (!FooBox.isFn(obj)) {
				console.error(FooBox.format('Expected type "function", received type "{0}".', typeof obj));
				return false;
			}
			var test = new obj();
			if (!FooBox.isFn(test.handles)) {
				console.error('The required "handles" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.defaults)) {
				console.error('The required "defaults" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.parse)) {
				console.error('The required "parse" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.load)) {
				console.error('The required "load" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.getSize)) {
				console.error('The required "getSize" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.hasChanged)) {
				console.error('The required "hasChanged" method is not implemented.');
				return false;
			}
			if (!FooBox.isFn(test.preload)) {
				console.error('The required "preload" method is not implemented.');
				return false;
			}
			return true;
		},
		/**
		 * Registers a handler and its default options with FooBox.
		 * @param {function} obj - The handler to register.
		 * @param {Object} [defaults] - The default options to merge with the FooBox's defaults.
		 * @returns {boolean} - True if the handler is registered.
		 */
		register: function(obj, defaults){
			if (!FooBox.handlers.validate(obj)) {
				console.error('Failed to register the handler.');
				return false;
			}
			return FooBox.objects.register(FooBox.handlers._ns, obj, defaults);
		},
		/**
		 * Loops through all registered handlers and initializes new instances for the supplied FooBox.
		 * @param {FooBox.Instance} instance - The instance of FooBox to load handlers for.
		 */
		load: function(instance){
			FooBox.objects.load(FooBox.handlers._ns, instance);
		},
		/**
		 * Loops through all handlers for the FooBox instance and calls the method passing in any additional arguments.
		 * @param {FooBox.Instance} instance - The instance of FooBox to call the handler method on.
		 * @param {string} method - The method to call on the handlers.
		 * @param {*} [arg1] - An optional argument to pass through to the method.
		 * @param {*} [argN] - Additional optional arguments.
		 */
		call: function(instance, method, arg1, argN){
			var args = Array.prototype.slice.call(arguments);
			args.unshift(FooBox.handlers._ns);
			FooBox.objects.call.apply(this, args);
		},
		/**
		 * Retrieves an instance of a handler from the supplied FooBox by type.
		 * @param {FooBox.Instance} instance - The instance of FooBox to retrieve the handler from.
		 * @param {string} type - The type to retrieve.
		 * @returns {Object}
		 */
		get: function (instance, type) {
			return FooBox.objects.get(FooBox.handlers._ns, instance, function(item){ return item.type == type; });
		}
	};

	/**
	 * A item object used by FooBox.
	 * @param {string} type - The type of item.
	 * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element the item is based on.
	 * @param {Object} handler - The handler for the item, this should match the type.
	 * @returns {FooBox.Item}
	 * @constructor
	 */
	FooBox.Item = function (type, element, handler) {
		/** @type {string} - The type of item. */
		this.type = type;
		/** @type {jQuery} - The jQuery or DOM element the item is based on. */
		this.element = FooBox.isjQuery(element) ? element : $(element);
		/** @type {Object} - The handler for the item, this should match the type. */
		this.handler = handler;
		/** @type {number} - The width of this item in pixels. */
		this.width = null;
		/** @type {number} - The height of this item in pixels. */
		this.height = null;
		/** @type {string} - The url of item. */
		this.url = null;
		/** @type {boolean} - Whether or not this item will overflow it's content when it is to big to be displayed. */
		this.overflow = false;
		/** @type {boolean} - Whether or not this item has been preloaded. */
		this.preloaded = false;
		/** @type {boolean} - Whether or not this item will maintain it's proportion when being resized. */
		this.proportion = false;
		/** @type {boolean} - Whether or not this item is an error item. */
		this.error = false;
		return this;
	};

	/**
	 * A simple size object used by FooBox.
	 * @param {number} width
	 * @param {number} height
	 * @returns {FooBox.Size}
	 * @constructor
	 */
	FooBox.Size = function (width, height) {
		/** @type {number} - The width of this size object. */
		this.width = (typeof width === "number") ? width : parseInt(width, 0);
		/** @type {number} - The height of this size object. */
		this.height = (typeof height === "number") ? height : parseInt(height, 0);
		/**
		 * Checks if this size is equal to the provided size.
		 * @param {FooBox.Size} size - The size to compare.
		 * @returns {boolean}
		 */
		this.equalTo = function(size){
			return this.width == size.width && this.height == size.height;
		};
		return this;
	};

	FooBox.Size.check = function (value) {
		if (typeof value === 'number') return value;
		if (!isNaN(parseInt(value))) return parseInt(value);
		return null;
	};

	/**
	 * A simple timer object created around setTimeout that is used by FooBox.
	 * @returns {FooBox.Timer}
	 * @constructor
	 */
	FooBox.Timer = function () {
		/** @type {number} - The id returned by the setTimeout function. */
		this.id = null;
		/** @type {boolean} - Whether or not the timer is currently counting down. */
		this.busy = false;

		/**
		 * @type {FooBox.Timer} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * Starts the timer and waits the specified amount of milliseconds before executing the supplied function.
		 * @param {function} func - The function to execute once the timer runs out.
		 * @param {number} milliseconds - The time in milliseconds to wait before executing the supplied function.
		 * @param {*} [thisArg] - The value of this within the scope of the function.
		 */
		this.start = function (func, milliseconds, thisArg) {
			thisArg = thisArg || func;
			_this.stop();
			_this.id = setTimeout(function () {
				func.call(thisArg);
				_this.id = null;
				_this.busy = false;
			}, milliseconds);
			_this.busy = true;
		};

		/**
		 * Stops the timer if its running and resets it back to its starting state.
		 */
		this.stop = function () {
			if (_this.id === null || _this.busy === false) { return; }
			clearTimeout(_this.id);
			_this.id = null;
			_this.busy = false;
		};

		return this;
	};

	/**
	 * Registers FooBox with jQuery. When used FooBox is initialized on the selected objects using the optional arguments.
	 * @returns {jQuery}
	 */
	$.fn.foobox = function (options) {
		options = options || {};
		return this.each(function () {
			var $this = $(this), fbx = $this.data('fbx_instance'), fg = $this.data('__FooGallery__'), o = $.extend(true, {}, options, $this.data('foobox'));
			if (fbx instanceof FooBox.Instance) {
				if (fbx.options.alwaysInit === true || fg){
					var opts = fbx.options;
					opts.alwaysInit = true;
					fbx.destroy();
					fbx = new FooBox.Instance(fbx.id);
					fbx.init(this, $.extend(true, opts, o));
				} else {
					fbx.reinit(o); // Otherwise reinitialize the plugin using the new options
				}
			} else {
				// If this is already an item in another FooBox ignore it.
				fbx = $(this).data('fbx_p_instance');
				if (fbx instanceof FooBox.Instance){ return; }
				// init a new instance if one doesn't exist
				fbx = new FooBox.Instance();
				fbx.init(this, o);
			}
		});
	};

	/**
	 * Shortcut for opening a FooBox.
	 * @param {(number|Object)} arg - The index of the instance of FooBox to open or an object containing options for a new instance of FooBox.
	 * @returns {(FooBox.Instance|null)} - The opened instance of FooBox or null if no action was taken.
	 */
	FooBox.open = function (arg) {
		var fbx = null, i;
		if (typeof arg == 'object'){
			var element = document.createElement('a');
			$(element).foobox(arg);
			fbx = $(element).data('fbx_instance');
		} else {
			if (typeof arg == 'number'){ i = arg;	}
			else {
				i = parseInt(arg || 0);
				i = isNaN(i) ? 0 : i;
			}
			i = i > FooBox.instances.length - 1 ? FooBox.instances.length - 1 : i < 0 ? 0 : i;
			fbx = FooBox.instances[i];
		}
		if (fbx == null || !(fbx.modal instanceof FooBox.Modal)){ return null; }
		fbx.modal.show(true);
		return fbx;
	};

	/**
	 * Shortcut for closing all open FooBox's.
	 */
	FooBox.close = function () {
		var instances = [], i, fbx = null;
		for (i = 0; i < FooBox.instances.length; i++){
			fbx = FooBox.instances[i];
			if (fbx == null || !(fbx.modal instanceof FooBox.Modal) || !FooBox.isjQuery(fbx.modal.element) || !fbx.modal.element.hasClass('fbx-show')){ continue; }
			instances.push(fbx);
		}
		for (i = 0; i < instances.length; i++){
			fbx = instances[i];
			fbx.modal.close();
		}
	};

	/**
	 * Causes the FooBox to resize to the supplied dimensions. Both parameters are optional and setting just one will cause only that one to be changed.
	 * If both are not set this will simply trigger the default resize method of FooBox.
	 * @param {number} [width]
	 * @param {number} [height]
	 */
	FooBox.resize = function(width, height){
		if (width || height){
			var i, fbx, item, size;
			for (i = 0; i < FooBox.instances.length; i++){
				fbx = FooBox.instances[i];
				if (fbx == null || !(fbx.modal instanceof FooBox.Modal) || !FooBox.isjQuery(fbx.modal.element) || !fbx.modal.element.hasClass('fbx-show')){ continue; }
				item = fbx.items.current();
				size = new FooBox.Size((width || item.width || 0), (height || item.height || 0));
				item.width = size.width;
				item.height = size.height;
				fbx.modal.resize(size, fbx.modal.element.find('.fbx-item-current'));
			}
		} else {
			$(window).trigger('resize.foobox');
		}
	};

	/**
	 * Used when content is loaded into a container after the page is loaded (think Ajax).
	 * @param {string} selector - The selector to to late bind on. This element must exist in the page at the time the page is loaded.
	 * @param {object} [options] - The options to use with FooBox. If not supplied this method falls back to FOOBOX.o (from the WP plugin) or the defaults.
	 */
	FooBox.lateBind = function(selector, options){
		var o = $.extend(true, {}, FooBox.defaults, options || (window.FOOBOX && FOOBOX.o) || {}), $container = $(selector);
		$container.on('click.foobox', o.selector, function(e){
			e.preventDefault();
			var fbx = $container.data('fbx_instance');
			if (!fbx) fbx = $container.foobox(o).data('fbx_instance');
			if (fbx instanceof FooBox.Instance && typeof this.index === 'number'){
				fbx.items.indexes.set(this.index);
				fbx.modal.show(true);
			}
		});
	};

})(jQuery, window);;(function($, _){

    _.Icons = function(){
        this.className = "fbx-icon";
        this.registered = {
            "default": {
                "close": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[CURRENT_ICON_CLASS]-default" d="M13.405 11.404q0 0.357-0.25 0.607l-1.214 1.214q-0.25 0.25-0.607 0.25t-0.607-0.25l-2.625-2.625-2.625 2.625q-0.25 0.25-0.607 0.25t-0.607-0.25l-1.214-1.214q-0.25-0.25-0.25-0.607t0.25-0.607l2.625-2.625-2.625-2.625q-0.25-0.25-0.25-0.607t0.25-0.607l1.214-1.214q0.25-0.25 0.607-0.25t0.607 0.25l2.625 2.625 2.625-2.625q0.25-0.25 0.607-0.25t0.607 0.25l1.214 1.214q0.25 0.25 0.25 0.607t-0.25 0.607l-2.625 2.625 2.625 2.625q0.25 0.25 0.25 0.607z"></path><path class="[CURRENT_ICON_CLASS]-flat" d="M12.667 3q0.286 0 0.477 0.19t0.19 0.477q0 0.281-0.193 0.474l-4.198 4.193 4.198 4.193q0.193 0.193 0.193 0.474 0 0.286-0.19 0.477t-0.477 0.19q-0.281 0-0.474-0.193l-4.193-4.198-4.193 4.198q-0.193 0.193-0.474 0.193-0.286 0-0.477-0.19t-0.19-0.477q0-0.281 0.193-0.474l4.198-4.193-4.198-4.193q-0.193-0.193-0.193-0.474 0-0.286 0.19-0.477t0.477-0.19q0.281 0 0.474 0.193l4.193 4.198 4.193-4.198q0.193-0.193 0.474-0.193z"></path></svg>',
                "prev": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[ICON_CLASS]-arrows-default" d="M14.57 8.643q0 0.473-0.29 0.808t-0.754 0.335h-6.286l2.616 2.625q0.339 0.321 0.339 0.804t-0.339 0.804l-0.67 0.679q-0.33 0.33-0.804 0.33-0.464 0-0.813-0.33l-5.813-5.821q-0.33-0.33-0.33-0.804 0-0.464 0.33-0.813l5.813-5.804q0.339-0.339 0.813-0.339 0.464 0 0.804 0.339l0.67 0.661q0.339 0.339 0.339 0.813t-0.339 0.813l-2.616 2.616h6.286q0.464 0 0.754 0.335t0.29 0.808z"></path><path class="[ICON_CLASS]-arrows-flat" d="M6.667 3q0.276 0 0.471 0.195t0.195 0.471q0 0.281-0.198 0.474l-3.526 3.526h10.391q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-10.391l3.526 3.526q0.198 0.193 0.198 0.474 0 0.276-0.195 0.471t-0.471 0.195q-0.281 0-0.474-0.193l-4.667-4.667q-0.193-0.203-0.193-0.474t0.193-0.474l4.667-4.667q0.198-0.193 0.474-0.193z"></path><path class="[ICON_CLASS]-arrows-1" d="M10 16l3-3-5-5 5-5-3-3-8 8z"></path><path class="[ICON_CLASS]-arrows-2" d="M0 8l16 7-3-7 3-7z"></path><path class="[ICON_CLASS]-arrows-3" d="M12 0l-8 8 8 8-4-8z"></path><path class="[ICON_CLASS]-arrows-4" d="M10 4l-4 4 4 4z"></path><path class="[ICON_CLASS]-arrows-5" d="M0 8l8 8v-5h8l0-6h-8v-5l-8 8zM2.375 7.039l0.25-0.25v2.422l-0.25-0.25v-1.922zM2.125 8.711l-0.25-0.25v-0.922l0.25-0.25v1.422zM2.875 6.539l0.25-0.25v3.422l-0.25-0.25v-2.922zM3.375 6.039l0.25-0.25v4.422l-0.25-0.25v-3.922zM3.875 5.539l0.25-0.25v5.422l-0.25-0.25v-4.922zM4.375 5.039l0.25-0.25v6.422l-0.25-0.25v-5.922zM4.875 4.539l0.25-0.25v7.422l-0.25-0.25v-6.922zM5.375 4.039l0.25-0.25v8.422l-0.25-0.25v-7.922zM5.875 3.539l0.25-0.25v9.422l-0.25-0.25v-8.922zM6.375 3.039l0.25-0.25v10.422l-0.25-0.25v-9.922zM1.625 7.789v0.422l-0.211-0.211 0.211-0.211zM7.375 10v-4h0.25v4h-0.25zM7.875 10v-4h0.25v4h-0.25zM8.375 10v-4h0.25v4h-0.25zM8.875 10v-4h0.25v4h-0.25zM9.375 10v-4h0.25v4h-0.25zM9.875 10v-4h0.25v4h-0.25zM10.375 10v-4h0.25v4h-0.25zM10.875 10v-4h0.25v4h-0.25zM11.375 10v-4h0.25v4h-0.25zM11.875 10v-4h0.25v4h-0.25zM12.375 10v-4h0.25v4h-0.25zM12.875 10v-4h0.25v4h-0.25zM13.375 10v-4h0.25v4h-0.25zM13.875 10v-4h0.25v4h-0.25zM14.375 10v-4h0.25v4h-0.25zM15 10h-0.125v-4h0.125l-0 4zM7 6h0.125v4h-0.125v3.586l-0.125-0.125v-10.922l0.125-0.125v3.586z"></path><path class="[ICON_CLASS]-arrows-6" d="M0 8l8 8v-5h8l0-6h-8v-5z"></path><path class="[ICON_CLASS]-arrows-7" d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zM3.293 8.707l4 4c0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293c0.391-0.391 0.391-1.024 0-1.414l-2.293-2.293h5.586c0.552 0 1-0.448 1-1s-0.448-1-1-1h-5.586l2.293-2.293c0.391-0.391 0.391-1.024 0-1.414s-1.024-0.391-1.414 0l-4 4c-0.391 0.391-0.391 1.024-0 1.414z"></path><path class="[ICON_CLASS]-arrows-8" d="M0 16h16v-16h-16l0 16zM14 2v12h-12l-0-12h12zM7 11.5l-3.5-3.5 3.5-3.5v2.5h5v2h-5v2.5z"></path><path class="[ICON_CLASS]-arrows-9" d="M15 5h1v6h-1zM13 5h1v6h-1zM11 5h1v6h-1zM0 8l7-7v4h3v6h-3v4z"></path><path class="[ICON_CLASS]-arrows-10" d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zM7 11.5v-2.5h5v-2h-5v-2.5l-3.5 3.5 3.5 3.5z"></path><path class="[ICON_CLASS]-arrows-11" d="M8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8 3.582-8 8-8zM8 14.5c3.59 0 6.5-2.91 6.5-6.5s-2.91-6.5-6.5-6.5-6.5 2.91-6.5 6.5 2.91 6.5 6.5 6.5zM3.293 7.293l4-4c0.391-0.391 1.024-0.391 1.414 0s0.391 1.024 0 1.414l-2.293 2.293h5.586c0.552 0 1 0.448 1 1s-0.448 1-1 1h-5.586l2.293 2.293c0.391 0.391 0.391 1.024 0 1.414-0.195 0.195-0.451 0.293-0.707 0.293s-0.512-0.098-0.707-0.293l-4-4c-0.39-0.391-0.39-1.024 0-1.414z"></path></svg>',
                "next": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[ICON_CLASS]-arrows-default" d="M14.57 8.071q0 0.482-0.33 0.813l-5.813 5.813q-0.348 0.33-0.813 0.33-0.455 0-0.804-0.33l-0.67-0.67q-0.339-0.339-0.339-0.813t0.339-0.813l2.616-2.616h-6.286q-0.464 0-0.754-0.335t-0.29-0.808v-1.143q0-0.473 0.29-0.808t0.754-0.335h6.286l-2.616-2.625q-0.339-0.321-0.339-0.804t0.339-0.804l0.67-0.67q0.339-0.339 0.804-0.339 0.473 0 0.813 0.339l5.813 5.813q0.33 0.313 0.33 0.804z"></path><path class="[ICON_CLASS]-arrows-flat" d="M9.333 3q0.281 0 0.474 0.193l4.667 4.667q0.193 0.193 0.193 0.474t-0.193 0.474l-4.667 4.667q-0.193 0.193-0.474 0.193-0.286 0-0.477-0.19t-0.19-0.477q0-0.281 0.193-0.474l3.531-3.526h-10.391q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195h10.391l-3.531-3.526q-0.193-0.193-0.193-0.474 0-0.286 0.19-0.477t0.477-0.19z"></path><path class="[ICON_CLASS]-arrows-1" d="M6 0l-3 3 5 5-5 5 3 3 8-8z"></path><path class="[ICON_CLASS]-arrows-2" d="M16 8l-16-7 3 7-3 7z"></path><path class="[ICON_CLASS]-arrows-3" d="M4 16l8-8-8-8 4 8z"></path><path class="[ICON_CLASS]-arrows-4" d="M6 12l4-4-4-4z"></path><path class="[ICON_CLASS]-arrows-5" d="M16 8l-8-8v5h-8l-0 6h8v5l8-8zM13.625 8.961l-0.25 0.25v-2.422l0.25 0.25v1.922zM13.875 7.289l0.25 0.25v0.922l-0.25 0.25v-1.422zM13.125 9.461l-0.25 0.25v-3.422l0.25 0.25v2.922zM12.625 9.961l-0.25 0.25v-4.422l0.25 0.25v3.922zM12.125 10.461l-0.25 0.25v-5.422l0.25 0.25v4.922zM11.625 10.961l-0.25 0.25v-6.422l0.25 0.25v5.922zM11.125 11.461l-0.25 0.25v-7.422l0.25 0.25v6.922zM10.625 11.961l-0.25 0.25v-8.422l0.25 0.25v7.922zM10.125 12.461l-0.25 0.25v-9.422l0.25 0.25v8.922zM9.625 12.961l-0.25 0.25v-10.422l0.25 0.25v9.922zM14.375 8.211v-0.422l0.211 0.211-0.211 0.211zM8.625 6v4h-0.25v-4h0.25zM8.125 6v4h-0.25v-4h0.25zM7.625 6v4h-0.25v-4h0.25zM7.125 6v4h-0.25v-4h0.25zM6.625 6v4h-0.25v-4h0.25zM6.125 6v4h-0.25v-4h0.25zM5.625 6v4h-0.25v-4h0.25zM5.125 6v4h-0.25v-4h0.25zM4.625 6v4h-0.25v-4h0.25zM4.125 6v4h-0.25v-4h0.25zM3.625 6v4h-0.25v-4h0.25zM3.125 6v4h-0.25v-4h0.25zM2.625 6v4h-0.25v-4h0.25zM2.125 6v4h-0.25v-4h0.25zM1.625 6v4h-0.25v-4h0.25zM1 6h0.125v4h-0.125l0-4zM9 10h-0.125v-4h0.125v-3.586l0.125 0.125v10.922l-0.125 0.125v-3.586z"></path><path class="[ICON_CLASS]-arrows-6" d="M16 8l-8-8v5h-8l-0 6h8v5z"></path><path class="[ICON_CLASS]-arrows-7" d="M8 0c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM12.707 8.707l-4 4c-0.195 0.195-0.451 0.293-0.707 0.293s-0.512-0.098-0.707-0.293c-0.391-0.391-0.391-1.024 0-1.414l2.293-2.293h-5.586c-0.552 0-1-0.448-1-1s0.448-1 1-1h5.586l-2.293-2.293c-0.391-0.391-0.391-1.024 0-1.414s1.024-0.391 1.414 0l4 4c0.391 0.391 0.391 1.024 0 1.414z"></path><path class="[ICON_CLASS]-arrows-8" d="M16 16h-16v-16h16l-0 16zM2 2v12h12l0-12h-12zM9 11.5l3.5-3.5-3.5-3.5v2.5h-5v2h5v2.5z"></path><path class="[ICON_CLASS]-arrows-9" d="M0 5h1v6h-1zM2 5h1v6h-1zM4 5h1v6h-1zM16 8l-7-7v4h-3v6h3v4z"></path><path class="[ICON_CLASS]-arrows-10" d="M8 0c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM9 11.5v-2.5h-5v-2h5v-2.5l3.5 3.5-3.5 3.5z"></path><path class="[ICON_CLASS]-arrows-11" d="M8 0c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM8 14.5c-3.59 0-6.5-2.91-6.5-6.5s2.91-6.5 6.5-6.5 6.5 2.91 6.5 6.5-2.91 6.5-6.5 6.5zM12.707 7.293l-4-4c-0.39-0.391-1.024-0.391-1.414 0s-0.391 1.024 0 1.414l2.293 2.293h-5.586c-0.552 0-1 0.448-1 1s0.448 1 1 1h5.586l-2.293 2.293c-0.391 0.391-0.391 1.024 0 1.414 0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293l4-4c0.39-0.391 0.39-1.024 0-1.414z"></path></svg>',
                "error": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M8 1.5c-1.736 0-3.369 0.676-4.596 1.904s-1.904 2.86-1.904 4.596c0 1.736 0.676 3.369 1.904 4.596s2.86 1.904 4.596 1.904c1.736 0 3.369-0.676 4.596-1.904s1.904-2.86 1.904-4.596c0-1.736-0.676-3.369-1.904-4.596s-2.86-1.904-4.596-1.904zM8 0v0c4.418 0 8 3.582 8 8s-3.582 8-8 8c-4.418 0-8-3.582-8-8s3.582-8 8-8zM7 11h2v2h-2zM7 3h2v6h-2z"></path></svg>',
                "spinner": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[CURRENT_ICON_CLASS]-default" d="M3 8c0-0.19 0.011-0.378 0.032-0.563l-2.89-0.939c-0.092 0.487-0.141 0.989-0.141 1.502 0 2.3 0.971 4.374 2.526 5.833l1.786-2.458c-0.814-0.889-1.312-2.074-1.312-3.375zM13 8c0 1.301-0.497 2.486-1.312 3.375l1.786 2.458c1.555-1.459 2.526-3.533 2.526-5.833 0-0.513-0.049-1.015-0.141-1.502l-2.89 0.939c0.021 0.185 0.032 0.373 0.032 0.563zM9 3.1c1.436 0.292 2.649 1.199 3.351 2.435l2.89-0.939c-1.144-2.428-3.473-4.188-6.241-4.534v3.038zM3.649 5.535c0.702-1.236 1.914-2.143 3.351-2.435v-3.038c-2.769 0.345-5.097 2.105-6.241 4.534l2.89 0.939zM10.071 12.552c-0.631 0.288-1.332 0.448-2.071 0.448s-1.44-0.16-2.071-0.448l-1.786 2.458c1.144 0.631 2.458 0.99 3.857 0.99s2.713-0.359 3.857-0.99l-1.786-2.458z"></path><path class="[CURRENT_ICON_CLASS]-flat" d="M10.833 10.495q0.271 0 0.469 0.198l1.885 1.885q0.198 0.198 0.198 0.474 0 0.271-0.198 0.469t-0.469 0.198q-0.276 0-0.474-0.198l-1.885-1.885q-0.193-0.193-0.193-0.474 0-0.276 0.195-0.471t0.471-0.195zM5.172 10.495q0.276 0 0.471 0.195t0.195 0.471-0.198 0.474l-1.885 1.885q-0.198 0.198-0.469 0.198-0.276 0-0.471-0.195t-0.195-0.471q0-0.281 0.193-0.474l1.885-1.885q0.198-0.198 0.474-0.198zM1.333 7.667h2.667q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2.667q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195zM8 11.667q0.276 0 0.471 0.195t0.195 0.471v2.667q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2.667q0-0.276 0.195-0.471t0.471-0.195zM3.286 2.948q0.271 0 0.469 0.198l1.885 1.885q0.198 0.198 0.198 0.469 0 0.276-0.195 0.471t-0.471 0.195q-0.281 0-0.474-0.193l-1.885-1.885q-0.193-0.193-0.193-0.474 0-0.276 0.195-0.471t0.471-0.195zM12 7.667h2.667q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2.667q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195zM8 1q0.276 0 0.471 0.195t0.195 0.471v2.667q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2.667q0-0.276 0.195-0.471t0.471-0.195zM12.719 2.948q0.271 0 0.469 0.198t0.198 0.469q0 0.276-0.198 0.474l-1.885 1.885q-0.193 0.193-0.469 0.193-0.286 0-0.477-0.19t-0.19-0.477q0-0.276 0.193-0.469l1.885-1.885q0.198-0.198 0.474-0.198z"></path><path class="[CURRENT_ICON_CLASS]-1" d="M5.569 12q0 0.536-0.379 0.911t-0.906 0.375q-0.536 0-0.911-0.375t-0.375-0.911 0.375-0.911 0.911-0.375q0.527 0 0.906 0.375t0.379 0.911zM9.426 13.714q0 0.473-0.335 0.808t-0.808 0.335-0.808-0.335-0.335-0.808 0.335-0.808 0.808-0.335 0.808 0.335 0.335 0.808zM3.857 8q0 0.589-0.42 1.009t-1.009 0.42-1.009-0.42-0.42-1.009 0.42-1.009 1.009-0.42 1.009 0.42 0.42 1.009zM13.283 12q0 0.411-0.295 0.705t-0.705 0.295-0.705-0.295-0.295-0.705 0.295-0.705 0.705-0.295 0.705 0.295 0.295 0.705zM5.854 4q0 0.652-0.46 1.112t-1.112 0.46-1.112-0.46-0.46-1.112 0.46-1.112 1.112-0.46 1.112 0.46 0.46 1.112zM9.997 2.286q0 0.714-0.5 1.214t-1.214 0.5-1.214-0.5-0.5-1.214 0.5-1.214 1.214-0.5 1.214 0.5 0.5 1.214zM14.844 8q0 0.357-0.25 0.607t-0.607 0.25-0.607-0.25-0.25-0.607 0.25-0.607 0.607-0.25 0.607 0.25 0.25 0.607zM12.997 4q0 0.295-0.21 0.504t-0.504 0.21-0.504-0.21-0.21-0.504 0.21-0.504 0.504-0.21 0.504 0.21 0.21 0.504z"></path><path class="[CURRENT_ICON_CLASS]-2" d="M16 8c-0.020-1.045-0.247-2.086-0.665-3.038-0.417-0.953-1.023-1.817-1.766-2.53s-1.624-1.278-2.578-1.651c-0.953-0.374-1.978-0.552-2.991-0.531-1.013 0.020-2.021 0.24-2.943 0.646-0.923 0.405-1.758 0.992-2.449 1.712s-1.237 1.574-1.597 2.497c-0.361 0.923-0.533 1.914-0.512 2.895 0.020 0.981 0.234 1.955 0.627 2.847 0.392 0.892 0.961 1.7 1.658 2.368s1.523 1.195 2.416 1.543c0.892 0.348 1.851 0.514 2.799 0.493 0.949-0.020 1.89-0.227 2.751-0.608 0.862-0.379 1.642-0.929 2.287-1.604s1.154-1.472 1.488-2.335c0.204-0.523 0.342-1.069 0.415-1.622 0.019 0.001 0.039 0.002 0.059 0.002 0.552 0 1-0.448 1-1 0-0.028-0.001-0.056-0.004-0.083h0.004zM14.411 10.655c-0.367 0.831-0.898 1.584-1.55 2.206s-1.422 1.112-2.254 1.434c-0.832 0.323-1.723 0.476-2.608 0.454-0.884-0.020-1.759-0.215-2.56-0.57-0.801-0.354-1.526-0.867-2.125-1.495s-1.071-1.371-1.38-2.173c-0.31-0.801-0.457-1.66-0.435-2.512s0.208-1.694 0.551-2.464c0.342-0.77 0.836-1.468 1.441-2.044s1.321-1.029 2.092-1.326c0.771-0.298 1.596-0.438 2.416-0.416s1.629 0.202 2.368 0.532c0.74 0.329 1.41 0.805 1.963 1.387s0.988 1.27 1.272 2.011c0.285 0.74 0.418 1.532 0.397 2.32h0.004c-0.002 0.027-0.004 0.055-0.004 0.083 0 0.516 0.39 0.94 0.892 0.994-0.097 0.544-0.258 1.075-0.481 1.578z"></path><path class="[CURRENT_ICON_CLASS]-3" d="M7 4c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM8 0c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM8 13c-0.552 0-1-0.448-1-1s0.448-1 1-1c0.552 0 1 0.448 1 1s-0.448 1-1 1zM8 8c-1.795 0-3.25 1.455-3.25 3.25s1.455 3.25 3.25 3.25c-3.59 0-6.5-2.91-6.5-6.5s2.91-6.5 6.5-6.5c1.795 0 3.25 1.455 3.25 3.25s-1.455 3.25-3.25 3.25z"></path><path class="[CURRENT_ICON_CLASS]-4" d="M6 2c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM10.243 3.757c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM13 8c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM11.243 12.243c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM7 14c0 0 0 0 0 0 0-0.552 0.448-1 1-1s1 0.448 1 1c0 0 0 0 0 0 0 0.552-0.448 1-1 1s-1-0.448-1-1zM2.757 12.243c0 0 0 0 0 0 0-0.552 0.448-1 1-1s1 0.448 1 1c0 0 0 0 0 0 0 0.552-0.448 1-1 1s-1-0.448-1-1zM2.257 3.757c0 0 0 0 0 0 0-0.828 0.672-1.5 1.5-1.5s1.5 0.672 1.5 1.5c0 0 0 0 0 0 0 0.828-0.672 1.5-1.5 1.5s-1.5-0.672-1.5-1.5zM0.875 8c0-0.621 0.504-1.125 1.125-1.125s1.125 0.504 1.125 1.125c0 0.621-0.504 1.125-1.125 1.125s-1.125-0.504-1.125-1.125z"></path><path class="[CURRENT_ICON_CLASS]-5" d="M8 0c-4.355 0-7.898 3.481-7.998 7.812 0.092-3.779 2.966-6.812 6.498-6.812 3.59 0 6.5 3.134 6.5 7 0 0.828 0.672 1.5 1.5 1.5s1.5-0.672 1.5-1.5c0-4.418-3.582-8-8-8zM8 16c4.355 0 7.898-3.481 7.998-7.812-0.092 3.779-2.966 6.812-6.498 6.812-3.59 0-6.5-3.134-6.5-7 0-0.828-0.672-1.5-1.5-1.5s-1.5 0.672-1.5 1.5c0 4.418 3.582 8 8 8z"></path><path class="[CURRENT_ICON_CLASS]-6" d="M0.001 8.025l-0 0c0 0 0 0.001 0 0.003 0.002 0.061 0.009 0.12 0.021 0.177 0.003 0.027 0.007 0.057 0.011 0.090 0.003 0.029 0.007 0.061 0.011 0.095 0.006 0.040 0.012 0.083 0.019 0.128 0.013 0.090 0.028 0.189 0.045 0.296 0.021 0.101 0.044 0.21 0.068 0.326 0.011 0.058 0.028 0.117 0.044 0.178s0.032 0.123 0.049 0.188c0.009 0.032 0.016 0.065 0.027 0.097s0.021 0.065 0.031 0.098 0.043 0.134 0.065 0.203c0.006 0.017 0.011 0.035 0.017 0.052s0.013 0.034 0.019 0.052 0.026 0.070 0.039 0.105c0.027 0.070 0.053 0.142 0.081 0.215 0.031 0.071 0.062 0.144 0.094 0.218 0.016 0.037 0.032 0.074 0.048 0.111s0.035 0.073 0.053 0.111 0.073 0.148 0.11 0.224c0.039 0.075 0.081 0.149 0.123 0.224 0.021 0.037 0.042 0.075 0.063 0.113s0.045 0.074 0.068 0.112 0.093 0.149 0.14 0.224c0.198 0.295 0.417 0.587 0.66 0.864 0.245 0.275 0.511 0.535 0.792 0.775 0.284 0.236 0.582 0.452 0.886 0.642 0.306 0.188 0.619 0.349 0.928 0.487 0.078 0.032 0.156 0.063 0.232 0.095 0.038 0.015 0.076 0.032 0.115 0.046s0.077 0.027 0.115 0.041 0.151 0.054 0.226 0.078c0.075 0.022 0.15 0.044 0.224 0.066 0.037 0.011 0.073 0.022 0.109 0.031s0.073 0.018 0.109 0.027 0.143 0.035 0.213 0.052c0.070 0.014 0.139 0.027 0.207 0.040 0.034 0.006 0.067 0.013 0.101 0.019 0.017 0.003 0.033 0.006 0.049 0.009s0.033 0.005 0.049 0.007c0.066 0.009 0.13 0.018 0.192 0.027 0.031 0.004 0.062 0.009 0.093 0.013s0.061 0.006 0.091 0.009 0.118 0.010 0.174 0.015c0.056 0.005 0.111 0.011 0.164 0.012 0.004 0 0.007 0 0.011 0 0.010 0.544 0.453 0.982 1 0.982 0.008 0 0.017-0 0.025-0.001v0c0 0 0.001-0 0.004-0 0.061-0.002 0.12-0.009 0.177-0.021 0.027-0.003 0.057-0.007 0.090-0.011 0.029-0.003 0.061-0.007 0.095-0.011 0.040-0.006 0.083-0.012 0.128-0.019 0.090-0.013 0.189-0.028 0.296-0.045 0.101-0.021 0.21-0.044 0.326-0.068 0.058-0.011 0.117-0.028 0.178-0.044s0.123-0.033 0.188-0.049c0.032-0.009 0.065-0.016 0.097-0.027s0.065-0.021 0.098-0.031 0.134-0.043 0.203-0.065c0.017-0.006 0.035-0.011 0.052-0.017s0.034-0.013 0.052-0.019 0.070-0.026 0.105-0.039c0.070-0.027 0.142-0.053 0.215-0.081 0.071-0.031 0.144-0.062 0.218-0.094 0.037-0.016 0.074-0.032 0.111-0.048s0.073-0.035 0.111-0.053 0.148-0.073 0.224-0.11c0.075-0.039 0.149-0.081 0.224-0.123 0.037-0.021 0.075-0.042 0.113-0.063s0.074-0.045 0.112-0.068 0.149-0.093 0.224-0.14c0.295-0.197 0.587-0.417 0.864-0.66 0.275-0.245 0.535-0.511 0.775-0.792 0.236-0.284 0.452-0.582 0.642-0.886 0.188-0.306 0.349-0.619 0.487-0.928 0.032-0.078 0.063-0.156 0.095-0.232 0.015-0.038 0.032-0.076 0.046-0.115s0.027-0.077 0.040-0.115 0.054-0.151 0.078-0.226c0.022-0.075 0.044-0.15 0.066-0.224 0.011-0.037 0.022-0.073 0.031-0.109s0.018-0.073 0.027-0.109 0.035-0.143 0.052-0.213c0.014-0.070 0.027-0.139 0.040-0.207 0.006-0.034 0.013-0.067 0.019-0.101 0.003-0.017 0.006-0.033 0.009-0.049s0.005-0.033 0.007-0.050c0.009-0.065 0.018-0.13 0.027-0.192 0.004-0.031 0.009-0.062 0.013-0.093s0.006-0.061 0.009-0.091 0.010-0.118 0.015-0.174c0.005-0.056 0.011-0.111 0.012-0.165 0-0.008 0.001-0.016 0.001-0.025 0.55-0.002 0.996-0.449 0.996-1 0-0.008-0-0.017-0.001-0.025h0c0 0-0-0.001-0-0.003-0.002-0.061-0.009-0.12-0.021-0.177-0.003-0.027-0.007-0.057-0.011-0.090-0.003-0.029-0.007-0.061-0.011-0.095-0.006-0.040-0.012-0.083-0.019-0.128-0.013-0.090-0.028-0.189-0.045-0.296-0.021-0.101-0.044-0.21-0.068-0.326-0.011-0.058-0.028-0.117-0.044-0.178s-0.032-0.123-0.049-0.188c-0.009-0.032-0.016-0.065-0.027-0.097s-0.021-0.065-0.031-0.098-0.043-0.134-0.065-0.203c-0.005-0.017-0.011-0.035-0.017-0.052s-0.013-0.034-0.019-0.052-0.026-0.070-0.039-0.105c-0.027-0.070-0.053-0.142-0.081-0.215-0.031-0.071-0.062-0.144-0.094-0.218-0.016-0.037-0.032-0.074-0.048-0.111s-0.035-0.073-0.053-0.111-0.073-0.148-0.11-0.224c-0.039-0.075-0.081-0.149-0.123-0.224-0.021-0.037-0.042-0.075-0.063-0.113s-0.045-0.074-0.068-0.112-0.093-0.149-0.14-0.224c-0.197-0.295-0.417-0.587-0.66-0.864-0.245-0.275-0.511-0.535-0.792-0.775-0.284-0.236-0.582-0.452-0.886-0.642-0.306-0.188-0.619-0.349-0.928-0.487-0.078-0.032-0.156-0.063-0.232-0.095-0.038-0.015-0.076-0.032-0.115-0.046s-0.077-0.027-0.115-0.040-0.151-0.054-0.226-0.078c-0.075-0.022-0.15-0.044-0.224-0.066-0.037-0.010-0.073-0.022-0.109-0.031s-0.073-0.018-0.109-0.027-0.143-0.035-0.213-0.052c-0.070-0.014-0.139-0.027-0.207-0.040-0.034-0.006-0.067-0.013-0.101-0.019-0.017-0.003-0.033-0.006-0.049-0.009s-0.033-0.005-0.049-0.007c-0.066-0.009-0.13-0.018-0.192-0.027-0.031-0.004-0.062-0.009-0.093-0.013s-0.061-0.006-0.091-0.009-0.118-0.010-0.174-0.015c-0.056-0.005-0.111-0.011-0.164-0.012-0.013-0-0.026-0.001-0.039-0.001-0.010-0.543-0.454-0.981-0.999-0.981-0.008 0-0.017 0-0.025 0.001l-0-0c0 0-0.001 0-0.003 0-0.061 0.002-0.12 0.009-0.177 0.021-0.027 0.003-0.057 0.007-0.090 0.011-0.029 0.003-0.061 0.007-0.095 0.011-0.040 0.006-0.083 0.012-0.128 0.019-0.090 0.013-0.189 0.028-0.296 0.045-0.101 0.021-0.21 0.044-0.326 0.068-0.058 0.011-0.117 0.028-0.178 0.044s-0.123 0.033-0.188 0.049c-0.032 0.009-0.065 0.016-0.097 0.027s-0.065 0.021-0.098 0.031-0.134 0.043-0.203 0.065c-0.017 0.006-0.035 0.011-0.052 0.017s-0.034 0.013-0.052 0.019-0.070 0.026-0.105 0.039c-0.070 0.027-0.142 0.053-0.215 0.081-0.071 0.031-0.144 0.062-0.218 0.094-0.037 0.016-0.074 0.032-0.111 0.048s-0.073 0.035-0.111 0.053-0.148 0.073-0.224 0.11c-0.075 0.039-0.149 0.081-0.224 0.123-0.037 0.021-0.075 0.042-0.113 0.063s-0.074 0.045-0.112 0.068-0.149 0.093-0.224 0.14c-0.295 0.198-0.587 0.417-0.864 0.66-0.275 0.245-0.535 0.511-0.775 0.792-0.236 0.284-0.452 0.582-0.642 0.886-0.188 0.306-0.349 0.619-0.487 0.928-0.032 0.078-0.063 0.156-0.095 0.232-0.015 0.038-0.032 0.076-0.046 0.115s-0.027 0.077-0.040 0.115-0.054 0.151-0.078 0.226c-0.022 0.075-0.044 0.15-0.066 0.224-0.011 0.037-0.022 0.073-0.032 0.109s-0.018 0.073-0.027 0.109-0.035 0.143-0.052 0.213c-0.014 0.070-0.027 0.139-0.040 0.207-0.006 0.034-0.013 0.067-0.019 0.101-0.003 0.017-0.006 0.033-0.009 0.049s-0.005 0.033-0.007 0.050c-0.009 0.065-0.018 0.13-0.027 0.192-0.004 0.031-0.009 0.062-0.013 0.093s-0.006 0.061-0.009 0.091-0.010 0.118-0.015 0.174c-0.005 0.056-0.011 0.111-0.012 0.165-0 0.009-0.001 0.017-0.001 0.025-0.537 0.017-0.967 0.458-0.967 0.999 0 0.008 0 0.017 0.001 0.025zM1.149 7.011c0.001-0.003 0.001-0.006 0.002-0.009 0.010-0.051 0.026-0.102 0.040-0.155s0.030-0.107 0.045-0.163c0.008-0.028 0.015-0.056 0.024-0.084s0.019-0.057 0.028-0.086 0.038-0.116 0.058-0.176c0.005-0.015 0.010-0.030 0.015-0.045s0.012-0.030 0.017-0.045 0.023-0.060 0.035-0.091 0.048-0.123 0.073-0.186c0.028-0.062 0.056-0.125 0.084-0.189 0.014-0.032 0.028-0.064 0.043-0.096s0.032-0.064 0.048-0.096 0.065-0.128 0.098-0.194c0.034-0.065 0.073-0.128 0.109-0.194 0.018-0.032 0.037-0.065 0.056-0.098s0.040-0.064 0.061-0.096c0.041-0.064 0.082-0.129 0.124-0.194 0.176-0.255 0.369-0.506 0.583-0.744 0.217-0.236 0.451-0.459 0.697-0.665 0.25-0.202 0.511-0.385 0.776-0.547 0.268-0.159 0.541-0.294 0.808-0.41 0.068-0.027 0.135-0.053 0.202-0.079 0.033-0.013 0.066-0.027 0.099-0.038s0.067-0.022 0.1-0.033 0.131-0.045 0.196-0.065c0.065-0.018 0.13-0.036 0.194-0.054 0.032-0.009 0.063-0.019 0.095-0.026s0.063-0.014 0.094-0.021 0.123-0.028 0.184-0.042c0.061-0.011 0.12-0.021 0.179-0.032 0.029-0.005 0.058-0.010 0.087-0.015 0.014-0.003 0.029-0.005 0.043-0.008s0.029-0.003 0.043-0.005c0.056-0.007 0.112-0.014 0.166-0.020 0.027-0.003 0.053-0.007 0.080-0.010s0.053-0.004 0.078-0.006 0.102-0.007 0.15-0.011c0.049-0.003 0.095-0.008 0.142-0.008 0.091-0.002 0.177-0.004 0.256-0.006 0.073 0.003 0.14 0.005 0.2 0.007 0.030 0.001 0.058 0.002 0.085 0.002 0.033 0.002 0.064 0.004 0.093 0.006 0.033 0.002 0.063 0.004 0.091 0.006 0.051 0.008 0.103 0.012 0.156 0.012 0.007 0 0.015-0 0.022-0.001 0.002 0 0.004 0 0.004 0v-0c0.487-0.012 0.887-0.372 0.962-0.84 0.008 0.002 0.017 0.004 0.025 0.006 0.051 0.010 0.102 0.026 0.155 0.040s0.107 0.030 0.163 0.045c0.028 0.008 0.056 0.015 0.084 0.024s0.057 0.019 0.086 0.028 0.116 0.038 0.176 0.058c0.015 0.005 0.030 0.010 0.045 0.015s0.030 0.012 0.045 0.017 0.060 0.023 0.091 0.035 0.123 0.048 0.186 0.073c0.062 0.028 0.125 0.056 0.189 0.084 0.032 0.014 0.064 0.028 0.096 0.043s0.064 0.032 0.096 0.048 0.128 0.065 0.194 0.098c0.065 0.034 0.129 0.073 0.194 0.109 0.032 0.018 0.065 0.037 0.098 0.056s0.064 0.040 0.096 0.061 0.129 0.082 0.194 0.124c0.255 0.176 0.506 0.369 0.744 0.583 0.236 0.217 0.459 0.451 0.665 0.697 0.202 0.25 0.385 0.511 0.547 0.776 0.159 0.268 0.294 0.541 0.41 0.808 0.027 0.068 0.053 0.135 0.079 0.202 0.013 0.033 0.027 0.066 0.038 0.099s0.022 0.067 0.033 0.1 0.045 0.131 0.065 0.196c0.018 0.065 0.036 0.13 0.054 0.194 0.009 0.032 0.019 0.063 0.026 0.095s0.014 0.063 0.021 0.094 0.028 0.123 0.042 0.184c0.011 0.061 0.021 0.12 0.032 0.179 0.005 0.029 0.010 0.058 0.015 0.087 0.003 0.014 0.005 0.029 0.008 0.043s0.003 0.029 0.005 0.043c0.007 0.056 0.014 0.112 0.020 0.166 0.003 0.027 0.007 0.053 0.010 0.080s0.004 0.053 0.006 0.078 0.007 0.102 0.011 0.15c0.003 0.049 0.008 0.095 0.008 0.142 0.002 0.091 0.004 0.177 0.006 0.256-0.003 0.073-0.005 0.14-0.007 0.2-0.001 0.030-0.002 0.058-0.002 0.085-0.002 0.033-0.004 0.064-0.006 0.093-0.002 0.033-0.004 0.063-0.006 0.091-0.008 0.051-0.012 0.103-0.012 0.156 0 0.007 0 0.015 0.001 0.022-0 0.002-0 0.004-0 0.004h0c0.012 0.481 0.363 0.877 0.823 0.959-0.001 0.005-0.002 0.009-0.003 0.014-0.010 0.051-0.025 0.102-0.040 0.155s-0.030 0.107-0.045 0.163c-0.008 0.028-0.015 0.056-0.024 0.084s-0.019 0.057-0.028 0.086-0.039 0.116-0.058 0.176c-0.005 0.015-0.010 0.030-0.015 0.045s-0.012 0.030-0.017 0.045-0.023 0.060-0.035 0.091-0.048 0.123-0.073 0.186c-0.028 0.062-0.056 0.125-0.084 0.189-0.014 0.032-0.028 0.064-0.043 0.096s-0.032 0.064-0.048 0.096-0.065 0.128-0.098 0.194c-0.034 0.065-0.073 0.129-0.109 0.194-0.018 0.032-0.037 0.065-0.056 0.098s-0.040 0.064-0.061 0.096-0.082 0.129-0.124 0.194c-0.176 0.255-0.369 0.506-0.583 0.744-0.217 0.236-0.451 0.459-0.697 0.665-0.25 0.202-0.511 0.385-0.776 0.547-0.268 0.159-0.541 0.294-0.808 0.41-0.068 0.027-0.135 0.053-0.202 0.079-0.033 0.013-0.066 0.027-0.099 0.038s-0.067 0.022-0.1 0.033-0.131 0.045-0.196 0.065c-0.065 0.018-0.13 0.036-0.194 0.054-0.032 0.009-0.063 0.019-0.095 0.026s-0.063 0.014-0.094 0.021-0.123 0.028-0.184 0.042c-0.061 0.011-0.12 0.021-0.179 0.032-0.029 0.005-0.058 0.010-0.087 0.015-0.014 0.003-0.028 0.005-0.043 0.008s-0.029 0.003-0.043 0.005c-0.056 0.007-0.112 0.014-0.166 0.020-0.027 0.003-0.053 0.007-0.080 0.010s-0.053 0.004-0.078 0.006-0.102 0.007-0.15 0.011c-0.049 0.003-0.095 0.008-0.142 0.008-0.091 0.002-0.177 0.004-0.256 0.006-0.073-0.003-0.14-0.005-0.2-0.007-0.030-0.001-0.058-0.002-0.085-0.002-0.033-0.002-0.064-0.004-0.093-0.006-0.033-0.002-0.063-0.004-0.091-0.006-0.051-0.008-0.103-0.012-0.156-0.012-0.007 0-0.015 0-0.022 0.001-0.002-0-0.003-0-0.003-0v0c-0.484 0.012-0.883 0.369-0.961 0.834-0.050-0.010-0.101-0.025-0.153-0.039s-0.107-0.030-0.163-0.045c-0.028-0.008-0.056-0.015-0.084-0.024s-0.057-0.019-0.086-0.028-0.116-0.039-0.176-0.058c-0.015-0.005-0.030-0.010-0.045-0.015s-0.030-0.012-0.045-0.017-0.060-0.023-0.091-0.035-0.123-0.048-0.186-0.073c-0.062-0.028-0.125-0.056-0.189-0.084-0.032-0.014-0.064-0.028-0.096-0.043s-0.064-0.032-0.096-0.048-0.128-0.065-0.194-0.098c-0.065-0.034-0.129-0.073-0.194-0.109-0.032-0.018-0.065-0.037-0.098-0.056s-0.064-0.040-0.096-0.061c-0.064-0.041-0.129-0.082-0.194-0.124-0.255-0.175-0.506-0.369-0.744-0.583-0.236-0.217-0.459-0.451-0.665-0.697-0.202-0.25-0.385-0.511-0.547-0.776-0.159-0.268-0.294-0.541-0.41-0.808-0.027-0.068-0.053-0.135-0.079-0.202-0.013-0.033-0.027-0.066-0.038-0.099s-0.022-0.067-0.033-0.1-0.045-0.131-0.065-0.196c-0.018-0.065-0.036-0.13-0.054-0.194-0.009-0.032-0.019-0.063-0.026-0.095s-0.014-0.063-0.021-0.094-0.028-0.123-0.042-0.184c-0.011-0.061-0.021-0.12-0.032-0.179-0.005-0.029-0.010-0.058-0.015-0.087-0.003-0.014-0.005-0.028-0.008-0.043s-0.003-0.029-0.005-0.043c-0.007-0.056-0.014-0.112-0.020-0.166-0.003-0.027-0.007-0.053-0.010-0.080s-0.004-0.053-0.006-0.078-0.007-0.101-0.011-0.15c-0.003-0.049-0.008-0.095-0.008-0.142-0.002-0.091-0.004-0.177-0.006-0.256 0.003-0.073 0.005-0.14 0.007-0.2 0.001-0.030 0.002-0.058 0.002-0.085 0.002-0.033 0.004-0.064 0.006-0.093 0.002-0.033 0.004-0.063 0.006-0.091 0.008-0.051 0.012-0.103 0.012-0.156 0-0.007-0-0.015-0.001-0.022 0-0.002 0-0.003 0-0.003h-0c-0.012-0.49-0.377-0.893-0.851-0.964z"></path><path class="[CURRENT_ICON_CLASS]-7" d="M16 6.5l-3.527-0.353 2.245-2.743-2.121-2.121-2.743 2.245-0.353-3.527h-3l-0.353 3.527-2.743-2.245-2.121 2.121 2.245 2.743-3.527 0.353v3l3.527 0.353-2.245 2.743 2.121 2.121 2.743-2.245 0.353 3.527h3l0.353-3.527 2.743 2.245 2.121-2.121-2.245-2.743 3.527-0.353v-3zM11 6.757v2.485l-1.757 1.757h-2.485l-1.757-1.757v-2.485l1.757-1.757h2.485l1.757 1.757z"></path><path class="[CURRENT_ICON_CLASS]-8" d="M13.657 2.343c-1.448-1.448-3.447-2.343-5.657-2.343-3.429 0-6.355 2.158-7.492 5.19l1.873 0.702c0.853-2.274 3.047-3.893 5.619-3.893 1.657 0 3.157 0.672 4.242 1.758l-2.242 2.242h6v-6l-2.343 2.343zM8 14c-1.657 0-3.157-0.672-4.242-1.758l2.242-2.242h-6v6l2.344-2.344c1.448 1.448 3.447 2.344 5.656 2.344 3.43 0 6.355-2.158 7.492-5.19l-1.873-0.702c-0.853 2.274-3.047 3.893-5.619 3.893z"></path><path class="[CURRENT_ICON_CLASS]-9" d="M8 3.25c-1.566 0-2.962 0.731-3.849 1.863l-1.2-0.9c1.161-1.496 2.992-2.464 5.048-2.464v-1.75l2.5 2.5-2.5 2.5v-1.75zM8 12.75c1.566 0 2.962-0.731 3.849-1.863l1.2 0.9c-1.161 1.496-2.992 2.464-5.048 2.464v1.75l-2.5-2.5 2.5-2.5v1.75zM3.15 8c0 1.568 0.78 2.961 1.98 3.827l-0.9 1.2c-1.564-1.139-2.58-2.968-2.58-5.026h-1.65l2.5-2.5 2.5 2.5h-1.85zM14.35 8h1.65l-2.5 2.5-2.5-2.5h1.85c0-1.568-0.78-2.961-1.98-3.827l0.9-1.2c1.564 1.139 2.58 2.968 2.58 5.026z"></path><path class="[CURRENT_ICON_CLASS]-10" d="M12.999 3.23v0 0zM13.032 8c1.631-1.56 2.423-3.087 1.896-4-0.301-0.521-0.99-0.77-1.929-0.77v0.75c0.659 0 1.137 0.148 1.28 0.395 0.113 0.195 0.050 0.496-0.023 0.713-0.139 0.417-0.442 0.917-0.876 1.444-0.263 0.32-0.567 0.643-0.904 0.963-0.465-0.402-0.982-0.801-1.544-1.188-0.055-0.679-0.142-1.327-0.257-1.93 0.878-0.26 1.675-0.397 2.323-0.397l0-0.75c-0 0-0 0-0 0-0.706 0-1.553 0.141-2.482 0.413-0.535-2.192-1.462-3.642-2.516-3.642s-1.981 1.45-2.516 3.642c-0.929-0.271-1.777-0.412-2.482-0.412-0.939 0-1.629 0.249-1.93 0.77-0.527 0.913 0.265 2.44 1.896 4-1.631 1.559-2.423 3.087-1.896 4 0.301 0.521 0.99 0.77 1.93 0.77 0.706 0 1.553-0.141 2.482-0.412 0.535 2.192 1.462 3.642 2.516 3.642s1.981-1.45 2.516-3.642c0.929 0.271 1.777 0.412 2.482 0.412 0.939 0 1.629-0.249 1.93-0.77 0.527-0.913-0.265-2.441-1.896-4zM11.915 8c-0.291 0.248-0.601 0.493-0.927 0.734 0.008-0.242 0.013-0.487 0.013-0.734s-0.004-0.492-0.013-0.734c0.327 0.241 0.637 0.486 0.927 0.734zM9.958 11.389c-0.36-0.128-0.728-0.274-1.098-0.435 0.213-0.113 0.426-0.232 0.64-0.355s0.424-0.25 0.63-0.378c-0.045 0.403-0.103 0.793-0.172 1.169zM9.125 9.949c-0.372 0.214-0.748 0.416-1.125 0.603-0.377-0.187-0.753-0.389-1.125-0.603-0.375-0.216-0.737-0.441-1.086-0.672-0.026-0.417-0.039-0.844-0.039-1.277s0.013-0.859 0.039-1.276c0.349-0.231 0.711-0.456 1.086-0.672 0.372-0.215 0.748-0.416 1.125-0.603 0.377 0.187 0.753 0.389 1.125 0.603 0.375 0.216 0.737 0.441 1.086 0.672 0.026 0.417 0.039 0.844 0.039 1.276s-0.013 0.859-0.039 1.277c-0.349 0.231-0.711 0.456-1.086 0.672zM5.87 10.22c0.205 0.128 0.415 0.254 0.63 0.378s0.427 0.242 0.64 0.355c-0.37 0.162-0.738 0.307-1.098 0.435-0.069-0.375-0.127-0.766-0.172-1.168zM5.013 8.734c-0.327-0.241-0.637-0.486-0.927-0.734 0.291-0.248 0.601-0.494 0.927-0.734-0.008 0.242-0.013 0.487-0.013 0.734s0.004 0.492 0.013 0.734zM6.042 4.612c0.36 0.128 0.728 0.274 1.098 0.435-0.213 0.113-0.426 0.232-0.64 0.355s-0.424 0.25-0.63 0.378c0.045-0.403 0.103-0.793 0.172-1.168zM9.5 5.402c-0.214-0.123-0.427-0.242-0.64-0.355 0.37-0.162 0.738-0.307 1.098-0.435 0.069 0.375 0.127 0.766 0.172 1.168-0.205-0.128-0.415-0.254-0.63-0.378zM6.581 2.607c0.24-0.639 0.521-1.151 0.813-1.48 0.152-0.172 0.381-0.376 0.607-0.376s0.454 0.205 0.607 0.376c0.292 0.329 0.573 0.841 0.813 1.48 0.146 0.388 0.273 0.812 0.382 1.265-0.58 0.202-1.185 0.45-1.801 0.743-0.616-0.292-1.221-0.541-1.801-0.743 0.109-0.453 0.237-0.877 0.382-1.265zM2.62 6.532c-0.434-0.527-0.736-1.026-0.876-1.444-0.073-0.218-0.135-0.518-0.023-0.713 0.143-0.247 0.621-0.395 1.28-0.395h0c0.648 0 1.445 0.137 2.323 0.397-0.115 0.603-0.202 1.251-0.257 1.93-0.561 0.387-1.079 0.787-1.544 1.188-0.338-0.321-0.641-0.643-0.904-0.963zM3.002 12.020c-0.659 0-1.137-0.148-1.28-0.395-0.113-0.195-0.050-0.496 0.023-0.713 0.139-0.417 0.442-0.917 0.876-1.444 0.263-0.32 0.567-0.643 0.904-0.963 0.465 0.402 0.982 0.801 1.544 1.188 0.055 0.679 0.142 1.327 0.257 1.93-0.878 0.26-1.675 0.397-2.323 0.397zM9.419 13.393c-0.24 0.639-0.521 1.151-0.813 1.48-0.152 0.172-0.381 0.376-0.607 0.376s-0.454-0.205-0.607-0.376c-0.292-0.329-0.573-0.841-0.813-1.48-0.146-0.388-0.273-0.812-0.382-1.265 0.58-0.202 1.185-0.45 1.801-0.743 0.616 0.292 1.221 0.541 1.801 0.743-0.109 0.453-0.236 0.877-0.382 1.265zM14.256 10.912c0.073 0.218 0.135 0.518 0.023 0.713-0.143 0.248-0.622 0.395-1.28 0.395-0.648 0-1.445-0.137-2.323-0.397 0.115-0.603 0.202-1.251 0.257-1.93 0.561-0.387 1.079-0.787 1.544-1.188 0.338 0.321 0.641 0.643 0.904 0.963 0.434 0.527 0.736 1.027 0.876 1.444zM6.75 8c0-0.69 0.56-1.25 1.25-1.25s1.25 0.56 1.25 1.25c0 0.69-0.56 1.25-1.25 1.25s-1.25-0.56-1.25-1.25z"></path><path class="[CURRENT_ICON_CLASS]-11" d="M7.282 8.718l-5-5-2.282 2.282v-6h6l-2.282 2.282 5 5zM8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8c-4.418 0-8-3.582-8-8h2c0 3.314 2.686 6 6 6s6-2.686 6-6c0-3.314-2.686-6-6-6v-2z"></path></svg>',
                "slideshow": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[ICON_CLASS]-play-default" d="M15.28 8.277l-11.857 6.589q-0.205 0.116-0.353 0.027t-0.147-0.321v-13.143q0-0.232 0.147-0.321t0.353 0.027l11.857 6.589q0.205 0.116 0.205 0.277t-0.205 0.277z"></path><path class="[ICON_CLASS]-play-flat" d="M4.8 3.677l7.313 4.656-7.313 4.656v-9.313z"></path><path class="[ICON_CLASS]-pause-default" d="M14.86 1.714v12.571q0 0.232-0.17 0.402t-0.402 0.17h-4.571q-0.232 0-0.402-0.17t-0.17-0.402v-12.571q0-0.232 0.17-0.402t0.402-0.17h4.571q0.232 0 0.402 0.17t0.17 0.402zM6.841 1.714v12.571q0 0.232-0.17 0.402t-0.402 0.17h-4.571q-0.232 0-0.402-0.17t-0.17-0.402v-12.571q0-0.232 0.17-0.402t0.402-0.17h4.571q0.232 0 0.402 0.17t0.17 0.402z"></path><path class="[ICON_CLASS]-pause-flat" d="M9.344 3.677h2.656v9.313h-2.656v-9.313zM4 12.99v-9.313h2.656v9.313h-2.656z"></path></svg>',
                "fullscreen": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path class="[ICON_CLASS]-maximize-default" d="M16 0v6l-2.16-2.16-3.313 3.313-1.679-1.679 3.313-3.313-2.16-2.16zM3.84 2.16l3.313 3.313-1.679 1.679-3.313-3.313-2.16 2.16v-6h6zM13.84 12.16l2.16-2.16v6h-6l2.16-2.16-3.313-3.313 1.679-1.679zM7.152 10.527l-3.313 3.313 2.16 2.16h-6v-6l2.16 2.16 3.313-3.313z"></path><path class="[ICON_CLASS]-maximize-flat" d="M11.333 1h2q0.828 0 1.414 0.586t0.586 1.414v2q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2q0-0.276-0.195-0.471t-0.471-0.195h-2q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195zM1.333 11q0.276 0 0.471 0.195t0.195 0.471v2q0 0.276 0.195 0.471t0.471 0.195h2q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2q-0.828 0-1.414-0.586t-0.586-1.414v-2q0-0.276 0.195-0.471t0.471-0.195zM2.667 1h2q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2q-0.276 0-0.471 0.195t-0.195 0.471v2q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2q0-0.828 0.586-1.414t1.414-0.586zM14.667 11q0.276 0 0.471 0.195t0.195 0.471v2q0 0.828-0.586 1.414t-1.414 0.586h-2q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195h2q0.276 0 0.471-0.195t0.195-0.471v-2q0-0.276 0.195-0.471t0.471-0.195z"></path><path class="[ICON_CLASS]-minimize-default" d="M1 9h6v6l-2.16-2.16-3.156 3.156-1.679-1.679 3.156-3.156zM12.84 11.16l3.156 3.156-1.679 1.679-3.156-3.156-2.16 2.16v-6h6zM15 7h-6v-6l2.16 2.16 3.156-3.156 1.679 1.679-3.156 3.156zM4.84 3.16l2.16-2.16v6h-6l2.16-2.16-3.156-3.156 1.679-1.679z"></path><path class="[ICON_CLASS]-minimize-flat" d="M11.333 1q0.276 0 0.471 0.195t0.195 0.471v2q0 0.276 0.195 0.471t0.471 0.195h2q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2q-0.828 0-1.414-0.586t-0.586-1.414v-2q0-0.276 0.195-0.471t0.471-0.195zM1.333 11h2q0.828 0 1.414 0.586t0.586 1.414v2q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2q0-0.276-0.195-0.471t-0.471-0.195h-2q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195zM4.667 1q0.276 0 0.471 0.195t0.195 0.471v2q0 0.828-0.586 1.414t-1.414 0.586h-2q-0.276 0-0.471-0.195t-0.195-0.471 0.195-0.471 0.471-0.195h2q0.276 0 0.471-0.195t0.195-0.471v-2q0-0.276 0.195-0.471t0.471-0.195zM12.667 11h2q0.276 0 0.471 0.195t0.195 0.471-0.195 0.471-0.471 0.195h-2q-0.276 0-0.471 0.195t-0.195 0.471v2q0 0.276-0.195 0.471t-0.471 0.195-0.471-0.195-0.195-0.471v-2q0-0.828 0.586-1.414t1.414-0.586z"></path></svg>',
                "social": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M13.5 11c-0.706 0-1.342 0.293-1.797 0.763l-6.734-3.367c0.021-0.129 0.032-0.261 0.032-0.396s-0.011-0.267-0.032-0.396l6.734-3.367c0.455 0.47 1.091 0.763 1.797 0.763 1.381 0 2.5-1.119 2.5-2.5s-1.119-2.5-2.5-2.5-2.5 1.119-2.5 2.5c0 0.135 0.011 0.267 0.031 0.396l-6.734 3.367c-0.455-0.47-1.091-0.763-1.797-0.763-1.381 0-2.5 1.119-2.5 2.5s1.119 2.5 2.5 2.5c0.706 0 1.343-0.293 1.797-0.763l6.734 3.367c-0.021 0.129-0.031 0.261-0.031 0.396 0 1.381 1.119 2.5 2.5 2.5s2.5-1.119 2.5-2.5c0-1.381-1.119-2.5-2.5-2.5z"></path></svg>',
                "buffer": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M0 12.448q0.176-0.24 0.504-0.408t0.784-0.32 0.648-0.248q0.304 0 0.536 0.072t0.536 0.24 0.4 0.2q0.752 0.336 4.16 1.904 0.304 0.064 0.568 0t0.632-0.28 0.384-0.232q0.32-0.144 1.224-0.552t1.4-0.632q0.064-0.032 0.664-0.336t0.968-0.384q0.208-0.032 0.44 0.016t0.376 0.12 0.368 0.208 0.288 0.168 0.248 0.096 0.296 0.128 0.176 0.176q0.048 0.064 0.064 0.224-0.16 0.208-0.496 0.384t-0.816 0.352-0.64 0.256q-0.688 0.32-2.056 0.984t-2.056 0.984q-0.112 0.048-0.336 0.184t-0.376 0.208-0.408 0.176-0.44 0.112-0.472-0.024l-4.224-1.968q-0.096-0.048-0.512-0.224t-0.824-0.352-0.856-0.384-0.744-0.376-0.344-0.264q-0.064-0.064-0.064-0.208zM0 8.16q0.176-0.24 0.504-0.4t0.8-0.32 0.664-0.24q0.304 0 0.544 0.072t0.552 0.24 0.408 0.216q0.672 0.304 2.024 0.928t2.040 0.944q0.304 0.080 0.592 0.008t0.624-0.272 0.4-0.232q1.088-0.512 2.56-1.152 0.176-0.080 0.504-0.264t0.616-0.312 0.576-0.176q0.256-0.048 0.504 0.016t0.6 0.272 0.368 0.208q0.080 0.048 0.248 0.104t0.288 0.128 0.184 0.168q0.048 0.080 0.064 0.224-0.16 0.224-0.504 0.408t-0.84 0.36-0.656 0.256q-0.768 0.368-2.168 1.040t-1.96 0.944q-0.112 0.048-0.416 0.224t-0.464 0.24-0.52 0.16-0.568 0q-3.424-1.616-4.16-1.952-0.096-0.048-0.704-0.304t-1.112-0.48-0.984-0.472-0.544-0.36q-0.064-0.064-0.064-0.224zM0 3.888q0.16-0.24 0.504-0.424t0.84-0.36 0.656-0.256l5.568-2.592q0.48 0 0.856 0.112t0.904 0.416 0.64 0.352q0.624 0.288 1.872 0.872t1.872 0.872q0.064 0.032 0.584 0.24t0.872 0.384 0.432 0.32q0.048 0.064 0.064 0.208-0.144 0.208-0.416 0.36t-0.696 0.304-0.552 0.216q-0.752 0.352-2.24 1.064t-2.224 1.064q-0.096 0.048-0.32 0.176t-0.368 0.2-0.4 0.168-0.432 0.096-0.448-0.016q-3.92-1.824-4.096-1.904-0.064-0.032-1.008-0.44t-1.632-0.744-0.768-0.48q-0.064-0.064-0.064-0.208z"></path></svg>',
                "digg": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 18 16"><path d="M-0.016 12.032v-5.952q0-0.208 0.208-0.208h2.816v-2.064q0-0.224 0.208-0.224h1.568v8.224q0 0.224-0.192 0.224h-4.608zM1.728 10.704h1.072q0.208 0 0.208-0.224v-3.296h-1.072q-0.208 0-0.208 0.224v3.296zM5.328 12.032v-5.952q0-0.208 0.208-0.208h1.568v5.936q0 0.224-0.192 0.224h-1.584zM5.328 4.912v-1.104q0-0.224 0.192-0.224h1.584v1.104q0 0.224-0.192 0.224h-1.584zM7.712 12.032v-5.952q0-0.208 0.208-0.208h4.608v8.064q0 0.208-0.208 0.208h-4.608l0.016-1.104q0-0.224 0.192-0.224h2.816v-0.784h-3.024zM9.472 10.704h1.072q0.192 0 0.192-0.224v-3.296h-1.072q-0.192 0-0.192 0.224v3.296zM13.136 13.040q0-0.224 0.208-0.224h2.816v-0.784h-3.024v-5.952q0-0.208 0.208-0.208h4.592v8.064q0 0.208-0.192 0.208h-4.608v-1.104zM14.88 10.704h1.072q0.208 0 0.208-0.224v-3.296h-1.072q-0.208 0-0.208 0.224v3.296z"></path></svg>',
                "facebook": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="13" height="16" viewBox="0 0 13 16"><path d="M4.475 8.336v-3.056h2.464v-1.536q0-1.664 1.080-2.832t2.616-1.168h2.448v3.056h-2.448q-0.24 0-0.432 0.264t-0.192 0.648v1.568h3.072v3.056h-3.072v7.408h-3.072v-7.408h-2.464z"></path></svg>',
                "linkedin": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><path d="M-0.080 1.686q0-0.799 0.559-1.319t1.455-0.52q0.879 0 1.423 0.512 0.559 0.528 0.559 1.375 0 0.767-0.543 1.279-0.559 0.528-1.471 0.528h-0.016q-0.879 0-1.423-0.528t-0.543-1.327zM0.128 15.673v-10.678h3.549v10.678h-3.549zM5.643 15.673h3.549v-5.963q0-0.559 0.128-0.863 0.224-0.543 0.679-0.919t1.143-0.376q1.79 0 1.79 2.414v5.707h3.549v-6.122q0-2.366-1.119-3.589t-2.957-1.223q-2.062 0-3.213 1.774v0.032h-0.016l0.016-0.032v-1.519h-3.549q0.032 0.512 0.032 3.181t-0.032 7.497z"></path></svg>',
                "pinterest": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="13" height="16" viewBox="0 0 13 16"><path d="M0 5.792q0-1.008 0.424-2t1.216-1.84 2.080-1.376 2.856-0.528q2.544 0 4.176 1.568t1.632 3.696q0 2.736-1.384 4.52t-3.576 1.784q-0.72 0-1.352-0.336t-0.888-0.816l-0.64 2.528q-0.080 0.304-0.208 0.616t-0.28 0.6-0.304 0.544-0.304 0.48-0.264 0.376-0.208 0.264l-0.096 0.128q-0.048 0.064-0.128 0.048t-0.096-0.096q0-0.016-0.024-0.224t-0.048-0.44-0.048-0.6-0.016-0.712 0.048-0.76 0.12-0.768q0.176-0.752 1.184-5.008-0.128-0.256-0.208-0.616t-0.080-0.584l-0.016-0.24q0-1.024 0.52-1.704t1.256-0.68q0.592 0 0.92 0.392t0.328 0.984q0 0.368-0.136 0.904t-0.36 1.232-0.32 1.128q-0.16 0.72 0.28 1.248t1.176 0.528q1.264 0 2.088-1.432t0.824-3.464q0-1.552-1.008-2.536t-2.816-0.984q-2.016 0-3.272 1.296t-1.256 3.104q0 1.072 0.608 1.808 0.208 0.24 0.128 0.512-0.032 0.080-0.096 0.368t-0.096 0.368q-0.032 0.176-0.16 0.232t-0.288-0.008q-0.944-0.384-1.416-1.32t-0.472-2.184z"></path></svg>',
                "reddit": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="19" height="16" viewBox="0 0 19 16"><path d="M0 7.696q0-0.992 0.792-1.696t1.8-0.704q0.992 0 1.68 0.576h0.080q2.144-1.312 5.36-1.312l0.080-0.064 1.12-4.272 3.872 0.576q0 0.016 0.032 0.016 0.016 0 0-0.016 0.032-0.016 0.184-0.248t0.232-0.296q0.544-0.512 1.312-0.512 0.784 0 1.32 0.52t0.536 1.304-0.536 1.344-1.32 0.56q-0.912 0-1.392-0.616t-0.48-1.544q-0.032 0.016-1.56-0.208t-1.752-0.272h-0.048q-0.032 0.032-0.264 0.856t-0.48 1.744-0.28 1.016v0.048l0.080 0.064q1.152 0 2.36 0.328t2.088 0.92l0.048 0.032q0.016-0.016 0.032-0.016t0.032-0.016q0.4-0.288 0.568-0.4t0.472-0.208 0.688-0.096q1.088 0 1.856 0.768t0.768 1.872q0 0.752-0.424 1.376t-1.112 0.96q0 3.12-4.16 4.848-0.656 0.288-0.896 0.368-0.512 0.16-1.456 0.304t-1.488 0.144q-1.92 0-3.696-0.56-0.032-0.032-1.152-0.608-1.52-0.816-2.44-1.856t-0.92-2.496h-0.080q-0.528-0.272-0.944-0.8l-0.144-0.192q-0.24-0.432-0.304-0.728t-0.064-0.808zM0.544 7.888q0 1.056 0.912 1.792 0.080-0.944 0.768-1.856t1.616-1.616q-0.592-0.416-1.312-0.416-0.784 0-1.384 0.648t-0.6 1.448zM2.176 10.224q0 0.864 0.496 1.664t1.272 1.368 1.64 0.952 1.68 0.544q1.2 0.224 2.336 0.224 1.968 0 3.728-0.632t2.96-1.96h-0.048q0.48-0.608 0.688-1.096t0.208-1.16q0-0.864-0.424-1.648t-1.112-1.344-1.472-0.968-1.6-0.616q-1.36-0.368-2.896-0.368-1.184 0-2.424 0.288t-2.4 0.864-1.896 1.592-0.736 2.296zM5.152 9.056q0-0.592 0.36-0.952t0.952-0.36 0.992 0.36 0.4 0.952q0 0.576-0.408 0.96t-0.984 0.384-0.944-0.384-0.368-0.96zM5.728 11.68h0.704q0.4 0.72 1.272 1.096t1.784 0.376q0.928 0 1.76-0.376t1.344-1.096h0.656q-0.4 0.992-1.488 1.536t-2.272 0.544q-1.152 0-2.264-0.552t-1.496-1.528zM11.104 9.056q0-0.608 0.368-0.96t0.976-0.352q0.544 0 0.928 0.384t0.384 0.928-0.384 0.944-0.928 0.4q-0.592 0-0.968-0.376t-0.376-0.968zM15.184 1.568q0 0.56 0.4 0.96t0.96 0.4q0.576 0 0.944-0.392t0.368-0.968q0-0.544-0.384-0.928t-0.928-0.384-0.952 0.392-0.408 0.92zM15.408 6.064q0.88 0.576 1.52 1.48t0.896 1.912q0.944-0.688 0.944-1.536 0-0.928-0.568-1.6t-1.48-0.672q-0.72 0-1.312 0.416z"></path></svg>',
                "tumblr": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="10" height="16" viewBox="0 0 10 16"><path d="M0 6.304v-2.272q0.96-0.32 1.616-0.88 0.656-0.528 1.072-1.376 0.416-0.816 0.56-2.032h2.288v4.048h3.792v2.512h-3.792v4.096q0 1.376 0.144 1.792 0.144 0.384 0.544 0.624 0.544 0.32 1.216 0.32 1.232 0 2.416-0.784v2.512q-1.008 0.496-1.856 0.688-0.832 0.192-1.792 0.192-1.088 0-1.952-0.272-0.88-0.304-1.424-0.8-0.576-0.496-0.832-1.088-0.224-0.544-0.224-1.68v-5.6h-1.776z"></path></svg>',
                "twitter": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16"><path d="M0 14.185q0.479 0.048 0.959 0.048 2.813 0 5.018-1.726-1.31-0.016-2.349-0.799t-1.422-1.998q0.32 0.064 0.751 0.064 0.559 0 1.071-0.144-1.406-0.272-2.325-1.382t-0.919-2.565v-0.048q0.847 0.479 1.838 0.495-0.831-0.543-1.318-1.43t-0.487-1.926q0-1.087 0.559-2.030 1.502 1.87 3.668 2.98t4.642 1.239q-0.112-0.527-0.112-0.927 0-1.662 1.183-2.845t2.861-1.183q1.742 0 2.94 1.278 1.39-0.288 2.557-0.991-0.447 1.454-1.758 2.237 1.167-0.144 2.301-0.639-0.767 1.199-1.998 2.093v0.527q0 1.614-0.471 3.236t-1.438 3.1-2.309 2.629-3.22 1.83-4.035 0.679q-3.34 0-6.185-1.806z"></path></svg>',
                "email": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M15 2h-14c-0.55 0-1 0.45-1 1v10c0 0.55 0.45 1 1 1h14c0.55 0 1-0.45 1-1v-10c0-0.55-0.45-1-1-1zM14 4v0.719l-6 3.536-6-3.536v-0.719h12zM2 12v-5.54l6 3.536 6-3.536v5.54h-12z"></path></svg>',
                "download": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M8 9l4-4h-3v-4h-2v4h-3zM11.636 7.364l-1.121 1.121 4.064 1.515-6.579 2.453-6.579-2.453 4.064-1.515-1.121-1.121-4.364 1.636v4l8 3 8-3v-4z"></path></svg>'
            }
        };

        this.getGroup = function(groupNameOrObject){
            var name = "default", icons = $.extend(true, {}, this.registered.default);
            if (typeof groupNameOrObject === "string" && this.registered.hasOwnProperty(groupNameOrObject)){
                $.extend(true, icons, this.registered[groupNameOrObject]);
            }
            if (typeof groupNameOrObject === "object"){
                name = "custom";
                $.extend(true, icons, this.registered.default, groupNameOrObject);
            }
            return { name: name, icons: icons };
        };

        this.get = function(name, groupNameOrObject){
            var self = this,
                grp = self.getGroup(groupNameOrObject),
                icon = typeof name === "string" && grp.icons.hasOwnProperty(name) ? grp.icons[name].replace(/\[ICON_CLASS]/g, self.className).replace(/\[CURRENT_ICON_CLASS]/g, self.className + "-" + name) : null,
                classes = [false, name, grp.name].map(function(val){
                    return val === false ? self.className : self.className + "-" + val;
                }).join(" ");
            return $(icon).attr('class', classes);
        };

        return this;
    };

    _.icons = new _.Icons();

})( jQuery, window.FooBox );;(function($, FooBox){

    /**
     * The core FooBox logic.
     * @param {number} [id] - The optional id to create the instance with.
     * @constructor
     */
    FooBox.Instance = function (id) {
        /** @type {number} - The id of the current FooBox instance. */
        this.id = null;
        if (typeof id == 'number'){
            this.id = id;
            FooBox.instances[id - 1] = this;
        } else {
            this.id = FooBox.instances.push(this);
        }

        /** @type {jQuery} - The jQuery element the Foobox is bound to. */
        this.element = null;
        /** @type {Object} - The options for the current FooBox instance. */
        this.options = $.extend(true, {}, FooBox.defaults);
        /** @type {FooBox.Modal} - The modal object for this FooBox. */
        this.modal = new FooBox.Modal(this);
        /** @type {Object} - An object containing any registered object arrays. */
        this.objects = {};

        FooBox.addons.load(this);
        FooBox.handlers.load(this);

        /**
         * @type {FooBox.Instance} - Hold a reference to this instance of the object to avoid scoping issues.
         * @private
         */
        var _this = this;

        /**
         * Raises an event on this FooBox appending the args to the event.fb namespace.
         * @param {string} event - The name of the event to raise, this can include namespaces.
         * @param {Object} [args] - An object containing additional values to be merged into the event.fb namespace.
         * @returns {jQuery.Event}
         */
        this.raise = function (event, args) {
            return FooBox.raise(_this, event, args);
        };

        /**
         * Initializes this instance of the object using the supplied element and options.
         * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element the FooBox was initialized on.
         * @param {Object} options - The options supplied when the FooBox was initialized.
         */
        this.init = function (element, options) {
            _this.element = FooBox.isjQuery(element) ? element : $(element);
            _this.options = FooBox.options.merge(_this.options, options || {});

            FooBox.addons.call(_this, 'preinit', _this.element, _this.options);

            //init code
            _this.items.init();
            if (_this.items.array.length > 0){
                _this.element.data('fbx_instance', _this);

                if (_this.options.containerCssClass) {
                    _this.element.addClass(_this.options.containerCssClass);
                }

                _this.setup.bind();
                _this.modal.init(element, _this.options);

                FooBox.handlers.call(_this, 'init', _this.element, _this.options);

                _this.raise('foobox.initialized');

                if (FooBox.isFn(_this.options.initCallback)) {
                    _this.options.initCallback.call(_this);
                }
            } else { // self cleanup, no items, no FooBox
                _this.destroy();
            }
        };

        /**
         * Reinitializes this instance of the object using the supplied options.
         * @param {Object} options - The options supplied when the FooBox was reinitialized.
         */
        this.reinit = function (options) {
            _this.options = FooBox.options.merge(_this.options, options || {});

            //reinit code
            _this.items.init(true);
            if (_this.items.array.length > 0){
                _this.setup.bind();
                _this.modal.reinit(_this.options);

                FooBox.handlers.call(_this, 'reinit', _this.options);
                _this.raise('foobox.reinitialized');
            } else { // self cleanup, no items, no FooBox
                _this.destroy();
            }
        };

        /**
         * Destroys this instance of the object removing any changes made to the DOM.
         */
        this.destroy = function(){
            FooBox.addons.call(_this, 'destroy');
            FooBox.handlers.call(_this, 'destroy');

            _this.items.destroy();
            _this.modal.destroy();
            _this.element.removeClass('fbx-instance fbx-item').data({'fbx_instance': null, 'fbx_p_instance': null});

            if (_this.id == FooBox.instances.length){
                FooBox.instances.splice(_this.id - 1, 1);
            } else {
                FooBox.instances[_this.id - 1] = null;
            }
            _this.raise('foobox.destroy');
        };

        /** @namespace - Contains the logic used for initialization and reinitialization. */
        this.setup = {
            /**
             * Binds any the events required for FooBox to function.
             */
            bind: function () {
                // Bind any external element clicks to open FooBox.
                $(_this.options.externalSelector).off('click.fooboxExternal').on('click.fooboxExternal', function (e) {
                    e.preventDefault();
                    var selector = $(this).data('foobox'), target = $(selector);
                    var fbx = target.data('fbx_instance') || target.data('fbx_p_instance');
                    if (target.length > 0 && fbx instanceof FooBox.Instance && fbx.modal instanceof FooBox.Modal) {
                        fbx.modal.show(true);
                    }
                    return false;
                });
            }
        };

        /** @namespace - Contains the logic used for management of items. */
        this.items = {
            /** @type {Array} - An array containing all items for this instance of FooBox. */
            array: [],
            /** @namespace - Contains the logic used for the management of item indexes. */
            indexes: {
                /** @type {number} - The index of the previous item relative to the current. */
                prev: -1,
                /** @type {number} - The index of the current item being displayed. */
                current: 0,
                /** @type {number} - The index of the next item relative to the current. */
                next: 1,
                /** @type {string} - A directional indicator: * = unknown/no change, > = use next to proceed, < = use prev to proceed. */
                direction: '*',
                /**
                 * Sets the current, prev and next indexes given only the current by inspecting the items array to determine the min and max allowed indexes.
                 * @param {number} current - The new value that should be used for the current index.
                 */
                set: function (current) {
                    var now = _this.items.indexes.current;
                    current = current || 0;
                    current = (current > _this.items.array.length - 1 ? 0 : (current < 0 ? _this.items.array.length - 1 : current));
                    var prev = current - 1, next = current + 1;
                    _this.items.indexes.current = current;
                    _this.items.indexes.prev = (prev < 0) ? _this.items.array.length - 1 : prev;
                    _this.items.indexes.next = (next > _this.items.array.length - 1) ? 0 : next;
                    _this.items.indexes.direction = _this.items.indexes._direction(now, current, _this.items.array.length - 1);
                },
                /** @private */
                _direction: function (previous, current, max) {
                    if (current == 0 && previous == max) { return '>'; }
                    if (current == max && previous == 0) { return '<'; }
                    if (current > previous) { return '>'; }
                    if (current < previous) { return '<'; }
                    return '*';
                }
            },
            /**
             * Generates a new item array. If items are passed in as options they will be initialized here and added to the array.
             * @param {boolean} [reinit] - A boolean indicating whether or not this is a reinit of the array.
             */
            new_array: function (reinit) {
                reinit = reinit || false;
                var item, e, index = 0, base;

                if (reinit == true){
                    if (_this.items.array.length > 0){
                        // clean items that no longer exist
                        for(var k = 0; k < _this.items.array.length; k++){
                            item = _this.items.array[k];
                            if (FooBox.isjQuery(item.element)){
                                // if the item has an element
                                if (item.element.parents(':first').length == 0){
                                    // and the parent no longer exists (the item is not in the DOM) remove it.
                                    _this.items.array.splice(k, 1);
                                    k -= 1;
                                }
                            } else if (!item.option) {
                                // if the item was not created through options (option items have no element) remove it.
                                _this.items.array.splice(k, 1);
                                k -= 1;
                            }
                        }
                        // reset the item indexes
                        for(var l = 0; l < _this.items.array.length; l++){
                            item = _this.items.array[l];
                            if (FooBox.isjQuery(item.element)){
                                base = item.element.get(0);
                                base.index = item.index = l;
                            } else {
                                item.index = l;
                            }
                        }
                    }
                } else { _this.items.array = []; }

                if ($.isArray(_this.options.items)) {
                    for (var i = 0; i < _this.options.items.length; i++) {
                        item = _this.options.items[i];
                        if (_this.items.indexOf(item) != -1){ continue; }
                        for (var j = 0; j < _this.objects.handlers.length; j++) {
                            if (_this.objects.handlers[j].type == item.type) {
                                item.index = index;
                                item.option = true;
                                item.handler = _this.objects.handlers[j];
                                item.handler.defaults(item);
                                e = _this.raise('foobox.parseItem', { 'element': null, 'item': item });
                                _this.items.array.push(e.fb.item);
                                index++;
                                break;
                            }
                        }
                    }
                }
            },
            /**
             * Initializes this instances item collection by using either the options or querying the DOM.
             * @param {boolean} [reinit] - A boolean indicating whether or not this is a reinit of the items.
             */
            init: function (reinit) {
                reinit = reinit || false;
                _this.items.new_array(reinit);
                var fg, items, fgFilter = function(item){
                    return !item.noLightbox && !item.panelHide;
                };
                if (_this.element.is(_this.options.selector) && !_this.element.is(_this.options.excludes) && _this.items.add(_this.element)) {
                    _this.element.off('click.item').on('click.item', _this.items.clicked);
                } else if (_this.element.is(".foogallery") && (fg = _this.element.data("__FooGallery__")) && !!fg.items && (items = fg.items.available(fgFilter)).length > 0){
                    var anchors = $.map(items, function(item){ return item.$anchor ? item.$anchor.get(0) : null; });
                    $(anchors)
                        .not(_this.options.excludes)
                        .off('click.item')
                        .filter(function () {
                            return _this.items.add(this);
                        })
                        .on('click.item', _this.items.clicked);
                    _this.element.on('click.item', '.fbx-link', _this.items.clicked);
                } else {
                    _this.element.find(_this.options.selector)
                        .not(_this.options.excludes)
                        .off('click.item')
                        .filter(function () {
                            return _this.items.add(this);
                        })
                        .on('click.item', _this.items.clicked);
                    _this.element.on('click.item', '.fbx-link', _this.items.clicked);
                }
                _this.items.rel();
            },
            /**
             * Loops through all items and removes any modifications made by FooBox.
             */
            destroy: function(){
                _this.element.off('click.item', '.fbx-link', _this.items.clicked);
                var item, i;
                for (i = 0; i < _this.items.array.length; i++){
                    item = _this.items.array[i];
                    if (FooBox.isFn(item.destroy)){
                        item.destroy();
                    }
                    if (FooBox.isjQuery(item.element)){
                        item.element.off('click.item').removeClass('fbx-link').data('fbx_p_instance', null);
                    }
                }
                _this.items.array = [];
            },
            /**
             * Checks if a rel option or attribute has been set and if it does finds any associated elements and combines them into a single collection.
             */
            rel: function () {
                var rel = _this.options.rel || _this.element.attr('rel') || (_this.items.first(function(item){
                    return FooBox.isjQuery(item.element) ? typeof item.element.attr('rel') == 'string' : false;
                }) || { element: $() }).element.attr('rel');

                if (typeof rel == 'string'){
                    $('[rel="' + rel + '"]')
                        .not(_this.options.excludes)
                        .not(_this.element)
                        .off('click.item')
                        .filter(function () {
                            return _this.items.add(this);
                        })
                        .on('click.item', _this.items.clicked);
                }
            },
            /**
             * Checks if a item exists in the items array by comparing the item.url property by default.
             * If the second param 'prop' is supplied that property will be used for the comparison.
             * Returns
             * @param {FooBox.Item} item - The item to check if it exists.
             * @param {string} [prop=url] - The property to use for comparisons.
             * @returns {number} - (-1) if the item doesn't exist or the index of the item if it does.
             */
            indexOf: function (item, prop) {
                if (!item) { return -1; }
                prop = prop || 'url';
                var i;
                for (i = 0; i < _this.items.array.length; i++) {
                    if (_this.items.array[i][prop] != item[prop]) { continue; }
                    return i;
                }
                return -1;
            },
            is: {
                /**
                 * Check if the supplied item is the first item in the array.
                 * @param {FooBox.Item} item - The item to check.
                 * @returns {boolean}
                 */
                first: function(item){
                    if (!_this.items.array || _this.items.array.length == 0){ return false; }
                    return _this.items.array[0].index == item.index;
                },
                /**
                 * Check if the supplied item is the last item in the array.
                 * @param {FooBox.Item} item - The item to check.
                 * @returns {boolean}
                 */
                last: function(item){
                    if (!_this.items.array || _this.items.array.length == 0){ return false; }
                    return _this.items.array[_this.items.array.length - 1].index == item.index;
                }
            },
            /**
             * Attempts to parse an element and add an item to the array.
             * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element to inspect.
             * @returns {boolean} - True if an item is successfully added to the item array.
             */
            add: function (element) {
                element = FooBox.isjQuery(element) ? element : $(element);
                var item = _this.items.parse(element, _this.items.array.length);
                if (item === null) { return false; }
                var base = element.get(0), index = _this.items.indexOf(item);
                if (index != -1) {
                    // if the item exists dbl bind it so clicking on either element opens the image
                    item = _this.items.array[index];
                    base.index = item.index;
                } else {
                    base.index = item.index = _this.items.array.push(item) - 1;
                }
                var fbx = element.addClass('fbx-link').data('fbx_p_instance');
                if (!(fbx instanceof FooBox.Instance)) {
                    element.data('fbx_p_instance', _this);
                }
                return true;
            },
            /**
             * Attempts to retrieve an item from the array using only the element.
             * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element to inspect.
             * @returns {(Object|null)} - null if no item is found.
             */
            get: function (element) {
                element = FooBox.isjQuery(element) ? element : $(element);
                var item = null, index = element.get(0).index;
                if (index && index > 0 && index <= _this.items.array.length - 1) { item = _this.items.array[index]; }
                return item;
            },
            /**
             * Attempts to parse an item using all registered handlers being supplied just the element.
             * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element to parse.
             * @param {number} [index] - The index for the item.
             * @returns {(FooBox.Item|null)} - null if unable to parse an item.
             */
            parse: function (element, index) {
                element = FooBox.isjQuery(element) ? element : $(element);
                var item, e;
                for (var i = 0; i < _this.objects.handlers.length; i++) {
                    if (_this.objects.handlers[i].handles(element, _this.element)) {
                        item = _this.objects.handlers[i].parse(element);
                        if (!isNaN(index)) item.index = index;
                        e = _this.raise('foobox.parseItem', { 'element': element, 'item': item });
                        break;
                    }
                }
                return typeof e !== "undefined" && e.fb.item ? e.fb.item : null;
            },
            /**
             * Generates an error item and places it into the array at the supplied index.
             * @param {number} index - The index at which to create the error item.
             * @returns {FooBox.Item}
             */
            error: function (index) {
                if (_this.items.array.length > index && _this.items.array[index].error == true) { return _this.items.array[index]; }

                var handler = FooBox.handlers.get(_this, 'html'), $element, error, isSelector = false;
                if (handler == null) {
                    function ErrHandler(fbx){
                        var _h = this;
                        _h.FooBox = fbx;
                        _h.type = 'error';
                        _h.handles = function(){ return false; };
                        _h.hasChanged = function(){ return false; };
                        _h.defaults = function(){};
                        _h.parse = function(){};
                        _h.load = function (item, container, success, error) {
                            try {
                                container.empty().append(item.content);
                                if (FooBox.isFn(success)){ success(_h.getSize(item)); }
                            }
                            catch (err) {
                                if (FooBox.isFn(error)) { error(err); }
                            }
                        };
                        _h.getSize = function(item){
                            return new FooBox.Size(item.width, item.height);
                        };
                        return _h;
                    }
                    handler = new ErrHandler(_this);
                }
                if (_this.options.error.match(/^#/i) !== null && $(_this.options.error).length > 0) {
                    $element = $(_this.options.error);
                    isSelector = true;
                } else {
                    $element = $('<div class="fbx-error-msg" data-width="240" data-height="240"></div>')
                        .append(FooBox.icons.get('error'))
                        .append($('<p/>').text(_this.options.error));
                }
                error = new FooBox.Item(handler.type, $element.get(0), handler);
                error.selector = isSelector == true ? _this.options.error : null;
                error.index = index;
                error.error = true;
                error.title = $element.data('title') || null;
                error.description = $element.data('description') || null;
                error.width = $element.data('width') || 240 || null;
                error.height = $element.data('height') || 240 || null;
                error.content = isSelector == true ? null : $element;
                error.fullscreen = true;
                error.handler = handler;

                _this.items.array[index] = error;
                return error;
            },
            /**
             * Gets the first item from the array of items matching the specified where clause.
             * @param {function} where - A function that returns a boolean indicating a successful match. The first param is always the item.
             */
            first: function(where){
                var item = null;
                for (var i = 0; i < _this.items.array.length; i++){
                    if (where(_this.items.array[i])){
                        item = _this.items.array[i];
                        break;
                    }
                }
                return item;
            },
            /**
             * Gets the current item for FooBox.
             * @returns {FooBox.Item}
             */
            current: function () {
                return _this.items.array[_this.items.indexes.current];
            },
            /**
             * Gets the previous item for FooBox relative to the current.
             * @returns {FooBox.Item}
             */
            prev: function () {
                return _this.items.array[_this.items.indexes.prev];
            },
            /**
             * Gets the next item for FooBox relative to the current.
             * @returns {FooBox.Item}
             */
            next: function () {
                return _this.items.array[_this.items.indexes.next];
            },
            /**
             * Checks whether or not there are multiple items in this FooBox.
             * @returns {boolean} - True if there is more than 1 item.
             */
            multiple: function () {
                return _this.items.array.length > 1;
            },
            /**
             * Handles the click event for any items, setting the indexes and showing the modal.
             * @param {jQuery.Event} e - The jQuery clicked event argument.
             * @returns {boolean} - Always false to cancel default click behaviour.
             */
            clicked: function (e) {
                e.preventDefault();
                _this.items.indexes.set(this.index);
                _this.modal.show(true);
                return false;
            }
        };
        return this;
    };

})( jQuery, window.FooBox);;(function($, FooBox){

    /**
     * The core FooBox Modal logic.
     * @param {FooBox.Instance} instance - The parent FooBox instance for this modal.
     * @returns {FooBox.Modal}
     * @constructor
     */
    FooBox.Modal = function (instance) {
        /** @type {FooBox.Instance} - The parent FooBox instance for this modal. */
        this.FooBox = instance;
        /** @type {jQuery} - The jQuery object for the modal. */
        this.element = null;
        /** @type {FooBox.Timer} - A timer object used by the loader. */
        this.loaderTimeout = new FooBox.Timer();
        /** @type {boolean} - Whether or not the current item is the first item shown by this modal. This is reset on close. */
        this._first = false;

        this._busy = false;

        this._closed = false;

        /**
         * @type {FooBox.Modal} - Hold a reference to this instance of the object to avoid scoping issues.
         * @private
         */
        var _this = this;

        var _scroll;

        /**
         * Initializes this instance of the Modal object using the supplied element and options.
         * @param {(jQuery|HTMLElement)} element - The jQuery or DOM element the FooBox was initialized on.
         * @param {Object} options - The options supplied when the FooBox was initialized.
         */
        this.init = function (element, options) {
            _this.setup.html();
            _this.setup.options(options);
            _this.setup.bind();
        };

        /**
         * Reinitializes this instance of the Modal object using the supplied options.
         * @param {Object} options - The options supplied when the FooBox was reinitialized.
         */
        this.reinit = function (options) {
            _this.setup.options(options);
        };

        /**
         * Destroys this instance of the modal object.
         */
        this.destroy = function(){
            if (FooBox.isjQuery(_this.element)){
                _this.element.remove();
            }
        };

        /** @namespace - Contains the logic used for initialization and reinitialization. */
        this.setup = {
            /**
             * Generates the base HTML used to construct the FooBox modal if it doesn't already exist.
             */
            html: function () {
                if (FooBox.isjQuery(_this.element)){ return; }

                _this.element = $('<div class="fbx-modal"></div>');
                _this.element.append('<div class="fbx-inner-spacer"></div>');

                var $stage = $('<div class="fbx-stage"></div>');
                $stage.append('<div class="fbx-item-current"></div>');
                $stage.append('<div class="fbx-item-next"></div>');

                var $inner = $('<div class="fbx-inner fbx-inner-shadow"></div>');
                $inner.append($stage);

                $inner.append($('<a class="fbx-prev fbx-btn-transition fbx-btn-shadow"></a>').append(FooBox.icons.get('prev')));
                $inner.append('<div class="fbx-credit"><a target="_blank" href=""><em></em><span> FooBox</span></a></div>');
                $inner.append('<span class="fbx-count" />');
                $inner.append($('<a class="fbx-next fbx-btn-transition fbx-btn-shadow"></a>').append(FooBox.icons.get('next')));
                $inner.append($('<a class="fbx-close fbx-btn-transition fbx-btn-shadow"></a>').append(FooBox.icons.get('close')));

                _this.element.append($('<div class="fbx-loader"></div>').append(FooBox.icons.get('spinner')));
                _this.element.append($inner);

                _this.FooBox.raise('foobox.setupHtml');

                $('body').append(_this.element);
            },
            /**
             * Applies the initial state for FooBox using the supplied options.
             * @param {Object} options - The options supplied when FooBox was initialized.
             */
            options: function (options) {
                var display;
                _this.element.removeClass()
                    .addClass('fbx-modal')
                    .addClass('fbx-' + _this.FooBox.id)
                    .addClass(_this.FooBox.element.data('style') || options.style)
                    .addClass(_this.FooBox.element.data('theme') || options.theme)
                    .addClass(_this.FooBox.element.data('modal-class'))
                    .addClass(options.modalClass)
                    .data('fbx_instance', _this.FooBox);

                _this.element.addClass(FooBox.browser.css);

                display = options.affiliate.enabled ? '' : 'none';
                _this.element.find('.fbx-credit').css('display', display);
                if (options.affiliate.enabled) {
                    _this.element.find('.fbx-credit > a').attr('href', options.affiliate.url);
                    _this.element.find('.fbx-credit > a > em').text(options.affiliate.prefix);
                }

                display = options.showCount && _this.FooBox.items.multiple() ? '' : 'none';
                _this.element.find('.fbx-count').css('display', display);

                if (!options.showButtons || !_this.FooBox.items.multiple()){
                    _this.element.addClass('fbx-no-buttons');
                }

                _this.FooBox.raise('foobox.setupOptions');
            },
            /**
             * Binds any the events required for FooBox to function.
             */
            bind: function () {
                //overlay click
                if (_this.FooBox.options.closeOnOverlayClick == true){
                    _this.element.off('click.foobox')
                        .on('click.foobox', function (e) {
                            // the option is checked as well as it can be disabled during run time.
                            if (_this.FooBox.options.closeOnOverlayClick == true && $(e.target).is('.fbx-modal')) {
                                _this.close();
                            }
                        });
                }
                //bind action buttons
                _this.element.find('.fbx-close').off('click.foobox')
                    .on('click.foobox',function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        _this.close();
                    }).end()
                    .find('.fbx-prev').off('click.foobox')
                    .on('click.foobox',function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(this).hasClass('fbx-disabled')) return;
                        _this.prev();
                    }).end()
                    .find('.fbx-next').off('click.foobox')
                    .on('click.foobox', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(this).hasClass('fbx-disabled')) return;
                        _this.next();
                    });
            }
        };

        /**
         * Small function to make this instance have the highest visibility priority. i.e. it will show above all others.
         */
        this.prioritize = function () {
            if (FooBox.instances.length > 1) {
                _this.element.nextAll('.fbx-modal:last').after(_this.element);
            }
        };

        /**
         * Attempts to preload the previous and next items if required.
         */
        this.preload = function () {
            if (_this.FooBox.options.preload != true) { return; }
            var prev = _this.FooBox.items.prev();
            if (prev) prev.handler.preload(prev);
            var next = _this.FooBox.items.next();
            if (next) next.handler.preload(next);
        };

        /**
         * The core function to the modal. This contains all the logic for displaying an item.
         * @param {boolean} [first=false] - An optional parameter indicating whether or not this is the first item displayed by the modal.
         */
        this.show = function (first) {
            var item = _this.FooBox.items.current();
            if (!item) return;

            first = first || false;

            _this._first = first;
            _this._busy = true;
            _this._closed = false;

            $('body').addClass('fbx-active');
            if (_this.FooBox.options.hideScrollbars) {
                $('html').addClass('fbx-no-scroll');
            }

            if (item.error == true) { _this.element.addClass('fbx-error'); }
            else { _this.element.removeClass('fbx-error'); }

            if (!_this.element.hasClass('fbx-show')) {
                _this.prioritize();
                var max = _this.getMaxSize();
                _this.element.addClass('fbx-loading')
                    .find('.fbx-inner')
                    .css({ 'width': max.width, 'height': max.height, 'margin-top': max.marginTop, 'margin-left': max.marginLeft });
            }

            var current = _this.element.find('.fbx-item-current'),
                next = _this.element.find('.fbx-item-next');

            next.css({opacity: 0, visibility:"hidden"});

            function handleError(err) {
                _this.loaderTimeout.stop();
                _this.element.removeClass('fbx-loading');

                _this._busy = false;
                console.error(err);
                if (_this.FooBox.raise('foobox.onError', { 'error': err }).isDefaultPrevented()) { return; }
                var error = _this.FooBox.items.error(item.index);
                if (error != null) { _this.show(first); }
            }

            _this.element.find('.fbx-count').text(_this.FooBox.options.countMessage.replace('%index', '' + (_this.FooBox.items.indexes.current + 1)).replace('%total', '' + _this.FooBox.items.array.length));

            if (_this.FooBox.raise('foobox.beforeLoad', { 'item': item }).isDefaultPrevented()) { _this._busy = false; return; }

            if (item.handler.hasChanged(item)) {
                var i = item.index, base = item.element.get(0);
                item = item.handler.parse(item.element);
                var e = _this.FooBox.raise('foobox.parseItem', { 'element': item.element, 'item': item });
                base.index = e.fb.item.index = i;
                _this.FooBox.items.array[i] = e.fb.item;
            }

            _this.preload();

            //start timer
            _this.loaderTimeout.start(function () {
                if (!_this._closed) _this.element.addClass('fbx-loading');
            }, _this.FooBox.options.loaderTimeout);

            setTimeout(function () {
                _this.checkForLoop(item);
                //load the html into the next container
                item.handler.load(item, next, function (size) {
                    if (_this._closed == true){ _this._busy = false; return; }
                    _this.transitionOut(current, function () {
                        // fix for firefox continuing to play audio of videos after they have been removed from the DOM
                        current.find("video").each(function(){
                            this.pause();
                            this.removeAttribute("src");
                            this.load();
                        });
                        if (_this._closed == true){ _this._busy = false; return; }
                        _this.resize(size, next, function () {
                            if (_this._closed == true){ _this._busy = false; return; }
                            _this.loaderTimeout.stop();
                            next.css({opacity: '', visibility: ''});
                            _this.transitionIn(next, function () {
                                if (_this._closed == true){ _this._busy = false; return; }
                                //do some final stuff
                                next.add(current).toggleClass('fbx-item-next fbx-item-current');
                                current.empty();
                                if (!_this.element.hasClass('fbx-show')) {
                                    if (!_this.FooBox.raise('foobox.beforeShow', { 'item': item }).isDefaultPrevented()){
                                        _this.element.removeClass('fbx-loading').addClass('fbx-show');
                                        _this.FooBox.raise('foobox.afterShow', { 'item': item });
                                        _this.FooBox.raise('foobox.afterLoad', { 'item': item });
                                        _this.triggerFooGalleryLayout(next);
                                        _this._busy = false;
                                    }
                                } else {
                                    _this.element.removeClass('fbx-loading');
                                    _this.FooBox.raise('foobox.afterLoad', { 'item': item });
                                    _this.triggerFooGalleryLayout(next);
                                    _this._busy = false;
                                }
                            }, handleError);

                        }, handleError);

                    }, handleError);

                }, handleError);

            }, _this.FooBox.options.loadDelay);
        };

        this.triggerFooGalleryLayout = function($parent){
            if (!window.FooGallery) return;
            $parent.find('.foogallery').each(function(){
                var fg = $(this).data('__FooGallery__');
                if (fg && fg.initialized){
                    fg.layout();
                }
            });
        };

        /**
         * Checks the for the loop option and appends the fbx-disabled class to the previous or next buttons as required.
         * @param {FooBox.Item} item - The item to be shown.
         */
        this.checkForLoop = function(item){
            if (_this.FooBox.options.loop == true) return;
            _this.element.find('.fbx-prev, .fbx-next').removeClass('fbx-disabled');
            if (_this.FooBox.items.is.first(item)){
                _this.element.find('.fbx-prev').addClass('fbx-disabled');
            }
            if (_this.FooBox.items.is.last(item)){
                _this.element.find('.fbx-next').addClass('fbx-disabled');
            }
        };

        this.getMaxSize = function(){
            var $inner = _this.element.find('.fbx-inner'),
                $spacer = _this.element.find('.fbx-inner-spacer');

            // The variables used to determine the maximum size for the image.
            var mpt = parseInt($spacer.css('padding-top'), 0),
                mpb = parseInt($spacer.css('padding-bottom'), 0),
                mpl = parseInt($spacer.css('padding-left'), 0),
                mpr = parseInt($spacer.css('padding-right'), 0),
                ibt = parseInt($inner.css('border-top-width'), 0),
                ibb = parseInt($inner.css('border-bottom-width'), 0),
                ibl = parseInt($inner.css('border-left-width'), 0),
                ibr = parseInt($inner.css('border-right-width'), 0),
                padding = parseInt($inner.css('padding-left'), 0),
                vdiff = mpt + mpb + (padding * 2) + ibt + ibb,
                hdiff = mpl + mpr + (padding * 2) + ibl + ibr;

            var size = new FooBox.Size(_this.element.width() - hdiff, _this.element.height() - vdiff);
            size.marginTop = -((size.height / 2) + padding + ((ibb + ibt) / 2) + (mpb / 2) - (mpt / 2));
            size.marginLeft = -((size.width / 2) + padding + ((ibr + ibl) / 2) + (mpr / 2) - (mpl / 2));

            return size;
        };

        /**
         * Resizes the inner modal to the maximum size of the viewport while keeping it in proportion.
         * @param {FooBox.Size} size - The size to resize to.
         * @param {jQuery} container - The container for the item being resized (fbx-item-current or fbx-item-next).
         * @param {function} [success] - The function to execute once the new size has been determined. The first parameter is the new size.
         * @param {function} [error] - The function to execute if an error occurs while determining the new size. The first parameter is the error.
         */
        this.resize = function (size, container, success, error) {
            try {
                if (size.width === 0 || size.height === 0) {
                    if (FooBox.isFn(error)) {
                        error('Invalid size supplied. Width = ' + size.width + ', Height = ' + size.height);
                    }
                    return;
                }
                var item = _this.FooBox.items.current(),
                    oSize = new FooBox.Size(size.width, size.height),
                    $inner = _this.element.find('.fbx-inner'),
                    $spacer = _this.element.find('.fbx-inner-spacer');

                // The variables used to determine the maximum size for the image.
                var mpt = parseInt($spacer.css('padding-top'), 0),
                    mpb = parseInt($spacer.css('padding-bottom'), 0),
                    mpl = parseInt($spacer.css('padding-left'), 0),
                    mpr = parseInt($spacer.css('padding-right'), 0),
                    ibt = parseInt($inner.css('border-top-width'), 0),
                    ibb = parseInt($inner.css('border-bottom-width'), 0),
                    ibl = parseInt($inner.css('border-left-width'), 0),
                    ibr = parseInt($inner.css('border-right-width'), 0),
                    padding = parseInt($inner.css('padding-left'), 0),
                    vdiff = mpt + mpb + (padding * 2) + ibt + ibb,
                    hdiff = mpl + mpr + (padding * 2) + ibl + ibr,
                    cs = new FooBox.Size(parseInt($inner.css('width'), 0), parseInt($inner.css('height'), 0)),
                    ms = new FooBox.Size(_this.element.width() - hdiff, _this.element.height() - vdiff),
                    ratio = ms.width / size.width;

                // If this is a portrait calculate the ratio using the height variables instead of the width.
                if ((size.height * ratio) > ms.height) { ratio = ms.height / size.height; }

                // Calculate the size to scale the modal to if fitToScreen is set to true.
                if (_this.FooBox.options.fitToScreen === true || (item.proportion == true && (size.height > ms.height || size.width > ms.width))) {
                    size.height = Math.floor(size.height * ratio);
                    size.width = Math.floor(size.width * ratio);
                } else if (item.proportion == false){
                    if (size.height > ms.height){ size.height = ms.height; }
                    if (size.width > ms.width){ size.width = ms.width; }
                }

                // enforce min size requirements (this is so buttons etc don't look arb if the modal is sized to like 20x20)
                if (size.height < 100) { size.height = 100; }
                if (size.width < 100) { size.width = 100; }

                var mt = -((size.height / 2) + padding + ((ibb + ibt) / 2) + (mpb / 2) - (mpt / 2)),
                    ml = -((size.width / 2) + padding + ((ibr + ibl) / 2) + (mpr / 2) - (mpl / 2));

                if (!cs.equalTo(size)) { // If the size has changed resize it.
                    if (!_this.FooBox.raise('foobox.beforeResize', { 'item': item, 'size': size, 'offset': { 'top': mt, 'left': ml }, 'error': error, 'success': success }).isDefaultPrevented()){
                        mt = -((size.height / 2) + padding + ((ibb + ibt) / 2) + (mpb / 2) - (mpt / 2));
                        ml = -((size.width / 2) + padding + ((ibr + ibl) / 2) + (mpr / 2) - (mpl / 2));

                        if (!_this.element.hasClass('fbx-show')) {
                            $inner.css({ 'height': size.height, 'width': size.width, 'margin-top': mt, 'margin-left': ml });
                            _this.overflow(item, oSize, size, container);
                            _this.FooBox.raise('foobox.afterResize', { 'item': item, 'size': size });
                            if (FooBox.isFn(success)) { success(); }
                        } else if (FooBox.browser.supportsTransitions()) {
                            var speed = _this.FooBox.options.resizeSpeed / 1000, trans = 'all ' + speed + 's ease-in-out';
                            $inner.css({
                                WebkitTransition : trans,
                                MozTransition    : trans,
                                MsTransition     : trans,
                                OTransition      : trans,
                                transition       : trans
                            });

                            $inner.css({ 'height': size.height, 'width': size.width, 'margin-top': mt, 'margin-left': ml });
                            setTimeout(function() {
                                $inner.css({
                                    WebkitTransition : '',
                                    MozTransition    : '',
                                    MsTransition     : '',
                                    OTransition      : '',
                                    transition       : ''
                                });
                                _this.overflow(item, oSize, size, container);
                                _this.FooBox.raise('foobox.afterResize', { 'item': item, 'size': size });
                                if (FooBox.isFn(success)) { success(size); }
                            }, _this.FooBox.options.resizeSpeed);
                        } else {
                            $inner.animate({ 'height': size.height, 'width': size.width, 'margin-top': mt, 'margin-left': ml }, _this.FooBox.options.resizeSpeed, function () {
                                _this.overflow(item, oSize, size, container);
                                _this.FooBox.raise('foobox.afterResize', { 'item': item, 'size': size });
                                if (FooBox.isFn(success)) { success(size); }
                            });
                        }
                    }
                } else {
                    $inner.css({ 'height': size.height, 'width': size.width, 'margin-top': mt, 'margin-left': ml });
                    _this.overflow(item, oSize, size, container);
                    _this.FooBox.raise('foobox.afterResize', { 'item': item, 'size': size });
                    if (FooBox.isFn(success)) { success(); }
                }
            } catch (err) {
                if (FooBox.isFn(error)) { error(err); }
            }
        };

        /**
         * Handles overflow for an item given the item and the current size.
         * @param {FooBox.Item} item - The item being displayed.
         * @param {FooBox.Size} oSize - The original size for the item before resizing occurred.
         * @param {FooBox.Size} size - The size for the item.
         * @param {jQuery} container - The container for the item being resized (fbx-item-current or fbx-item-next).
         */
        this.overflow = function(item, oSize, size, container){
            var $item = container.find('.fbx-item');
            if ((item.overflow && (size.width < (oSize.width * 0.8) || size.height < (oSize.height * 0.8)))
                || (item.overflow && !item.proportion && (size.width < oSize.width || size.height < oSize.height))
                || (item.overflow && item.type === 'html')) {
                if (item.type === 'html'){
                    $item.css({ 'width': '100%', 'height': 'auto', 'overflow': '' });
                    container.css('overflow', 'auto');
                } else {
                    $item.css({ 'width': oSize.width, 'height': oSize.height, 'overflow': '' });
                    container.css({
                        'overflow-x': (oSize.width <= size.width ? 'hidden' : ''),
                        'overflow-y': (oSize.height <= size.height ? 'hidden' : '')
                    });
                }
                _this.FooBox.raise('foobox.showOverflow', { 'item': item, 'container': container });
            } else {
                $item.css({ 'width': '', 'height': '', 'overflow': $item.is("iframe") ? '' : 'hidden' });
                container.css({ 'overflow-x': '', 'overflow-y': '', 'overflow': 'hidden' });
                _this.FooBox.raise('foobox.hideOverflow', { 'item': item, 'container': container });
            }
        };

        /**
         * Transitions out the current item.
         * @param {jQuery} current - The current jQuery object to transition out.
         * @param {function} [success] - The function to execute once the transition is complete.
         * @param {function} [error] - The function to execute if an error occurs while transitioning. The first parameter is the error.
         */
        this.transitionOut = function (current, success, error) {
            try {
                if (!_this.element.hasClass('fbx-show')) {
                    if (FooBox.isFn(success)) { success(); }
                    return;
                }
                current.animate({ 'opacity': 0 }, current.is(':visible') ? _this.FooBox.options.transitionOutSpeed : 0, function () {
                    if (FooBox.isFn(success)) { success(); }
                });
            } catch (err) {
                if (FooBox.isFn(error)) { error(err); }
            }
        };

        /**
         * Transitions in the next item.
         * @param {jQuery} next - The next jQuery object to transition in.
         * @param {function} [success] - The function to execute once the transition is complete.
         * @param {function} [error] - The function to execute if an error occurs while transitioning. The first parameter is the error.
         */
        this.transitionIn = function (next, success, error) {
            try {
                if (!_this.element.hasClass('fbx-show')) {
                    next.css({ 'opacity': 1 });
                    if (FooBox.isFn(success)) { success(); }
                    return;
                }
                next.animate({ 'opacity': 1 }, _this.FooBox.options.transitionInSpeed, function () {
                    if (FooBox.isFn(success)) { success(); }
                });
            } catch (err) {
                if (FooBox.isFn(error)) { error(err); }
            }
        };

        /**
         * Closes the current instance of the modal.
         */
        this.close = function () {
            if (!_this.FooBox.raise('foobox.beforeClose').isDefaultPrevented()){
                _this._closed = true;
                _this._busy = false;
                _this.loaderTimeout.stop();
                _this.element.removeClass('fbx-loading fbx-show');
                $('body').removeClass('fbx-active');
                _this.FooBox.raise('foobox.close');
                // fix for firefox continuing to play audio of videos after they have been removed from the DOM
                _this.element.find('.fbx-item-current video, .fbx-item-next video').each(function(){
                    this.pause();
                    this.removeAttribute("src");
                    this.load();
                });
                _this.element.find('.fbx-item-current, .fbx-item-next').empty();
                $('html').removeClass('fbx-no-scroll');
                _this.FooBox.raise('foobox.afterClose');
            }
        };

        /**
         * Shows the previous item in the collection, if the current item is the first this will loop back round to the last.
         * @param {string} [type] - An optional item type to use when going to the previous item.
         */
        this.prev = function (type) {
            if (_this._busy == true) return;
            _this.FooBox.items.indexes.set(_this.FooBox.items.indexes.prev);

            if (typeof type === 'string') {
                var item = _this.FooBox.items.current();
                while (item.type != type) {
                    _this.FooBox.items.indexes.set(_this.FooBox.items.indexes.prev);
                    item = _this.FooBox.items.current();
                }
            }

            _this.show(false);
            _this.FooBox.raise('foobox.previous');
        };

        /**
         * Shows the next item in the collection, if the current item is the last this will loop back round to the first.
         * @param {string} [type] - An optional item type to use when going to the next item.
         */
        this.next = function (type) {
            if (_this._busy == true) return;
            _this.FooBox.items.indexes.set(_this.FooBox.items.indexes.next);

            if (typeof type === 'string') {
                var item = _this.FooBox.items.current();
                while (item.type != type) {
                    _this.FooBox.items.indexes.set(_this.FooBox.items.indexes.next);
                    item = _this.FooBox.items.current();
                }
            }

            _this.show(false);
            _this.FooBox.raise('foobox.next');
        };

        return this;
    };

})( jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox HtmlHandler - A handler providing support for HTML items for the FooBox plugin.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the HTML handler. */
	var defaults = {
		/** @type {Object} - An object containing HTML handler related options. */
		html: {
			/** @type {string} - A String determining the attribute to check on the element. */
			attr: 'href',
			/** @type {boolean} - A Boolean indicating whether or not to allow overflow by default on HTML items. */
			overflow: true,
			/** @type {boolean} - A Boolean indicating whether or not to allow fullscreen by default on HTML items. */
			fullscreen: true,
			/** @type {number} - A number indicating the extra padding to apply to the size of the HTML element. */
			sizePadding: 20,
			/** @type {boolean} - A Boolean indicating whether or not to display captions on a HTML item. */
			showCaptions: false,
			/** @type {RegExp} - A regular expression used to check whether or not this handler handles an item. */
			regex: /^#/i,
			/**
			 * @type {function} - A Function to use to 'find' the selector on an element.
			 * @param {FooBox.Instance} foobox - The parent instance of FooBox.
			 * @param {jQuery} element - The jQuery element to find the selector on.
			 * @returns {string}
			 */
			findSelector: function (foobox, element) {
				if (!element) { return ''; }
				var attr = element.attr(foobox.options.html.attr);
				return (typeof attr == 'string') ? element.attr(foobox.options.html.attr) : '';
			}
		}
	};

	/**
	 * Extend the FooBox.Item with additional properties used by this handler.
	 */
	/** @type {jQuery} - The jQuery object containing the content to display. */
	FooBox.Item.prototype.content = null;
	/** @type {string} - The selector for the element to display. */
	FooBox.Item.prototype.selector = null;
	/** @type {number} - The amount of padding to apply to the item when displayed. */
	FooBox.Item.prototype.padding = null;

	/**
	 * The core logic for the FooBox.HtmlHandler addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this handler.
	 * @constructor
	 */
	FooBox.HtmlHandler = function(instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this handler. */
		this.FooBox = instance;
		/** @type {string} - A String representing the type this handler parses. */
		this.type = 'html';
		/** @type {RegExp} - The regular expression used to check whether or not this handler handles an item. */
		this.regex = /^#/i;

		/**
		 * @type {FooBox.HtmlHandler} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called after the core FooBox plugin and addons are initialized.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.init = function(element){
			_this.handlers.unbind();
			element.on('foobox.close', _this.handlers.onClose);
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind();
		};

		/** @namespace - Contains all the event handlers used by this handler. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 */
			unbind: function(){
				_this.FooBox.element.off('foobox.close', _this.handlers.onClose);
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onClose: function(){
				var i, item;
				for(i = 0; i < _this.FooBox.items.array.length; i++){
					item = _this.FooBox.items.array[i];
					if (item.type == _this.type && FooBox.isjQuery(item.content) && item.error == false && $(item.selector).length > 0){
						$(item.selector).append(item.content.children());
						item.content = null;
					}
				}
			}
		};

		/**
		 * Determines whether or not this handler handles the element supplied.
		 * @param {jQuery} element - The jQuery element to check.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @returns {boolean} - True if this handler handles the supplied element.
		 */
		this.handles = function (element) {
			var selector = _this.FooBox.options.html.findSelector(_this.FooBox, element),
				handle = $(element).attr('target') === 'foobox' && typeof selector === 'string' && selector.match(_this.FooBox.options.html.regex) != null && $(element).data('type') != 'embed' && ($(selector).length > 0 || $(element).data('ajax') == true);

			var e = _this.FooBox.raise('foobox.handlesHtml', { 'element': element, 'handle': handle });
			return e.fb.handle;
		};

		/**
		 * Sets the default values for an item parsed by this handler.
		 * @param {FooBox.Item} item - The new item to set default values on.
		 */
		this.defaults = function(item){
			item.fullscreen = item.fullscreen || _this.FooBox.options.html.fullscreen;
			item.overflow = item.overflow || _this.FooBox.options.html.overflow;
			item.social = item.social || true;
			item.proportion = item.proportion || false;
			item.captions = item.captions || _this.FooBox.options.html.showCaptions;
		};

		/**
		 * Parses the supplied element into an item for use by FooBox.
		 * @param {jQuery} element - The jQuery element to parse into an item.
		 * @returns {FooBox.Item}
		 */
		this.parse = function (element) {
			var item = new FooBox.Item(_this.type, element, this);

			_this.defaults(item);

			item.url = item.selector = _this.FooBox.options.html.findSelector(_this.FooBox, element) || null;
			item.padding = element.data('padding') || _this.FooBox.options.html.sizePadding || 0;
			var $target = item.selector != null ? $(item.selector) : null;
			if ($target != null && $target.length > 0) {
				item.width = FooBox.Size.check($target.data('width') || element.data('width') || item.width || null);
				item.height = FooBox.Size.check($target.data('height') || element.data('height') || item.height || null);
			} else {
				item.width = FooBox.Size.check(element.data('width') || item.width || null);
				item.height = FooBox.Size.check(element.data('height') || item.height || null);
			}
			item.overflow = typeof element.data('overflow') == 'boolean' ? element.data('overflow') : item.overflow;
			item.fullscreen = typeof element.data('fullscreen') == 'boolean' ? element.data('fullscreen') : item.fullscreen;
			item.proportion = typeof element.data('proportion') == 'boolean' ? element.data('proportion') : item.proportion;
			item.image_url = element.data('image') || '';
			return item;
		};

		/**
		 * Attempts to load the supplied items content executing the supplied callbacks as required.
		 * @param {FooBox.Item} item - The item to load.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @param {function} success - The function to execute if the loading succeeds. This will be supplied the size of the item as the first parameter.
		 * @param {function} error - The function to execute if the loading fails.
		 */
		this.load = function (item, container, success, error) {
			try {
				var $html = $('<div/>').addClass('fbx-item');

				// seeing as I made this handle the errors I've added this small if else to append the correct class when needed.
				if (item.error == true){ $html.addClass('fbx-item-error'); }
				else { $html.addClass('fbx-item-html'); }

				if (item.content == null && typeof item.selector == 'string'){
					if ($(item.selector).length == 0){
						var e = _this.FooBox.raise('foobox.loadHtml', {
							container: $html,
							selector: item.selector,
							success: function(){
								item.content = e.fb.container;
								container.empty().append(item.content);
								if (FooBox.isFn(success)){ success(_this.getSize(item)); }
							},
							error: function(err){
								err = err || 'Unable to load HTML.';
								if (FooBox.isFn(error)){ error(err); }
							}
						});
						return;
					} else {
						var $content = $(item.selector);
						if ($content.length > 0){
							item.content = $html.append($content.children());
						}
					}
				}
				if (FooBox.isjQuery(item.content)){
					container.empty().append(item.content);
					if (FooBox.isFn(success)){ success(_this.getSize(item)); }
				} else {
					if (FooBox.isFn(error)){ error('No valid HTML found to display.'); }
				}
			} catch (err) {
				if (FooBox.isFn(error)){ error(err); }
			}
		};

		/**
		 * If required preloads the item to reduce load time changing between previous and next items.
		 * @param {FooBox.Item} item - The item to preload if required.
		 */
		this.preload = function(item){

		};

		/**
		 * Small function to retrieve CSS prop values for sizing.
		 * @param {jQuery} element - The element to retrieve the CSS prop values from.
		 * @returns {Object}
		 */
		this.getCSS = function(element){
			return { 'font': element.css('font'), 'padding': element.css('padding'), 'margin': element.css('margin'), 'border': element.css('border') };
		};

		/**
		 * Retrieves the size for the supplied item. If no width and height is set this should attempt to find the size.
		 * @param {FooBox.Item} item - The item to retrieve the size for.
		 * @returns {FooBox.Size}
		 */
		this.getSize = function (item) {
			if ((item.auto || item.width == null || item.width == 0 || item.height == null || item.height == 0) && typeof item.selector == 'string'){
				item.auto = item.auto || { width: false, height: false };
				var $clone, styles, $item = $(item.selector);
				if ($item.length > 0 && $item.children().length > 0){
					styles = _this.getCSS($item);
					$clone = $item.clone().css(styles);
				} else if (FooBox.isjQuery(item.content)) {
					styles = _this.getCSS(item.content);
					$clone = item.content.clone().css(styles);
				}
				if (FooBox.isjQuery($clone)){
					var $spacer = _this.FooBox.modal.element.find(".fbx-inner-spacer"),
							paddingLeft = parseInt($spacer.css('padding-left')), paddingRight = parseInt($spacer.css('padding-right')),
							padding = (isNaN(paddingLeft) ? 0 : paddingLeft) + (isNaN(paddingRight) ? 0 : paddingRight),
							$inner = _this.FooBox.modal.element.find(".fbx-inner"),
							borderLeft = parseInt($inner.css('border-left')), borderRight = parseInt($inner.css('border-right')),
							border = (isNaN(borderLeft) ? 0 : borderLeft) + (isNaN(borderRight) ? 0 : borderRight),
							maxWidth = Math.max(document.documentElement.clientWidth, window.innerWidth, 0) - padding - border;
					$clone.css({ position: 'absolute', visibility: 'hidden', display: 'block', top: -10000, left: -10000, maxWidth: maxWidth }).appendTo('body');
					if (item.auto.width == true || item.width == null || item.width == 0){
						item.auto.width = true;
						item.width = $clone.outerWidth(true);
					} else if (!!item.width){
						$clone.width(item.width);
					}
					if (item.auto.height == true || item.height == null || item.height == 0){
						item.auto.height = true;
						item.height = $clone.outerHeight(true);
					}
					$clone.remove();
				}
			}

			if (item.width != null && item.height != null) {
				return new FooBox.Size(item.width, item.height);
			}
			// If all else fails... return (0, 0)
			return new FooBox.Size(0, 0);
		};

		/**
		 * This is called prior to displaying the item. You can perform various checks here to determine whether or not the item has changed since it's last display.
		 * @param {FooBox.Item} item - The item to check for changes.
		 * @returns {boolean} - True if the item has changed otherwise false.
		 */
		this.hasChanged = function () {
			return false;
		};
	};

	FooBox.handlers.register(FooBox.HtmlHandler, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox EmbedHandler - A handler providing support for embed items for the FooBox plugin.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the embed handler. */
	var defaults = {
		/** @type {Object} - An object containing embed handler related options. */
		embed: {
			/** @type {string} - A String determining the attribute to check on the element. */
			attr: 'href',
			/** @type {boolean} - A Boolean indicating whether or not to allow overflow by default on embed items. */
			overflow: false,
			/** @type {boolean} - A Boolean indicating whether or not to allow fullscreen by default on embed items. */
			fullscreen: true,
			/** @type {boolean} - A Boolean indicating whether or not to display captions on a embed item. */
			showCaptions: false,
			/** @type {RegExp} - A regular expression used to check whether or not this handler handles an item. */
			regex: /^#/i,
			/**
			 * @type {function} - A Function to use to 'find' the selector on an element.
			 * @param {FooBox.Instance} foobox - The parent instance of FooBox.
			 * @param {jQuery} element - The jQuery element to find the selector on.
			 * @returns {string}
			 */
			findSelector: function (foobox, element) {
				if (!element) { return ''; }
				var attr = element.attr(foobox.options.embed.attr);
				return (typeof attr == 'string') ? element.attr(foobox.options.embed.attr) : '';
			}
		}
	};

	/**
	 * The core logic for the FooBox.EmbedHandler addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this handler.
	 * @constructor
	 */
	FooBox.EmbedHandler = function(instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this handler. */
		this.FooBox = instance;
		/** @type {string} - A String representing the type this handler parses. */
		this.type = 'embed';
		/** @type {RegExp} - The regular expression used to check whether or not this handler handles an item. */
		this.regex = /^#/i;

		/**
		 * @type {FooBox.EmbedHandler} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called after the core FooBox plugin and addons are initialized.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.init = function(element){
			_this.handlers.unbind();
			element.on('foobox.close', _this.handlers.onClose);
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind();
		};

		/** @namespace - Contains all the event handlers used by this handler. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 */
			unbind: function(){
				_this.FooBox.element.off('foobox.close', _this.handlers.onClose);
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onClose: function(){
				var i, item;
				for(i = 0; i < _this.FooBox.items.array.length; i++){
					item = _this.FooBox.items.array[i];
					if (item.type == _this.type && FooBox.isjQuery(item.content) && item.error == false && $(item.selector).length > 0){
						var $content = item.content.children().detach();
						$(item.selector).append($content);
						item.content = null;
					}
				}
			}
		};

		/**
		 * Determines whether or not this handler handles the element supplied.
		 * @param {jQuery} element - The jQuery element to check.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @returns {boolean} - True if this handler handles the supplied element.
		 */
		this.handles = function (element) {
			var selector = _this.FooBox.options.embed.findSelector(_this.FooBox, element),
				handle = $(element).attr('target') === 'foobox' && typeof selector === 'string' && selector.match(_this.FooBox.options.embed.regex) != null && $(selector).length > 0 && $(element).data('type') == 'embed';

			var e = _this.FooBox.raise('foobox.handlesEmbed', { 'element': element, 'handle': handle });
			return e.fb.handle;
		};

		/**
		 * Sets the default values for an item parsed by this handler.
		 * @param {FooBox.Item} item - The new item to set default values on.
		 */
		this.defaults = function(item){
			item.fullscreen = item.fullscreen || _this.FooBox.options.embed.fullscreen;
			item.overflow = item.overflow || _this.FooBox.options.embed.overflow;
			item.social = item.social || true;
			item.proportion = item.proportion || true;
			item.captions = item.captions || _this.FooBox.options.embed.showCaptions;
		};

		/**
		 * Parses the supplied element into an item for use by FooBox.
		 * @param {jQuery} element - The jQuery element to parse into an item.
		 * @returns {FooBox.Item}
		 */
		this.parse = function (element) {
			var item = new FooBox.Item(_this.type, element, this);

			_this.defaults(item);

			item.url = item.selector = _this.FooBox.options.embed.findSelector(_this.FooBox, element) || null;
			var $target = item.selector != null ? $(item.selector) : null;
			if ($target != null && $target.length > 0) {
				item.width = FooBox.Size.check($target.data('width') || element.data('width') || item.width || null);
				item.height = FooBox.Size.check($target.data('height') || element.data('height') || item.height || null);
			} else {
				item.width = FooBox.Size.check(element.data('width') || item.width || null);
				item.height = FooBox.Size.check(element.data('height') || item.height || null);
			}
			item.overflow = typeof element.data('overflow') == 'boolean' ? element.data('overflow') : item.overflow;
			item.fullscreen = typeof element.data('fullscreen') == 'boolean' ? element.data('fullscreen') : item.fullscreen;
			item.proportion = typeof element.data('proportion') == 'boolean' ? element.data('proportion') : item.proportion;
			item.image_url = element.data('image') || '';
			return item;
		};

		/**
		 * Attempts to load the supplied items content executing the supplied callbacks as required.
		 * @param {FooBox.Item} item - The item to load.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @param {function} success - The function to execute if the loading succeeds. This will be supplied the size of the item as the first parameter.
		 * @param {function} error - The function to execute if the loading fails.
		 */
		this.load = function (item, container, success, error) {
			try {
				var $html = $('<div/>').addClass('fbx-item fbx-item-embed');
				if (item.content == null && typeof item.selector == 'string'){
					if ($(item.selector).length == 0){
						var e = _this.FooBox.raise('foobox.loadEmbed', {
							container: $html,
							selector: item.selector,
							success: function(){
								item.content = e.fb.container;
								container.empty().append(item.content);
								if (FooBox.isFn(success)){ success(_this.getSize(item)); }
							},
							error: function(err){
								err = err || 'Unable to load embed.';
								if (FooBox.isFn(error)){ error(err); }
							}
						});
						return;
					} else {
						var $content = $(item.selector);
						if ($content.length > 0){
							item.content = $html.append($content.children());
						}
					}
				}
				if (FooBox.isjQuery(item.content)){
					container.empty().append(item.content);
					if (FooBox.isFn(success)){ success(_this.getSize(item)); }
				} else {
					if (FooBox.isFn(error)){ error('No valid HTML found to display.'); }
				}
			} catch (err) {
				if (FooBox.isFn(error)){ error(err); }
			}
		};

		/**
		 * If required preloads the item to reduce load time changing between previous and next items.
		 * @param {FooBox.Item} item - The item to preload if required.
		 */
		this.preload = function(item){

		};

		/**
		 * Small function to retrieve CSS prop values for sizing.
		 * @param {jQuery} element - The element to retrieve the CSS prop values from.
		 * @returns {Object}
		 */
		this.getCSS = function(element){
			return { 'font': element.css('font'), 'padding': element.css('padding'), 'margin': element.css('margin'), 'border': element.css('border') };
		};

		/**
		 * Retrieves the size for the supplied item. If no width and height is set this should attempt to find the size.
		 * @param {FooBox.Item} item - The item to retrieve the size for.
		 * @returns {FooBox.Size}
		 */
		this.getSize = function (item) {
			if ((item.auto || item.width == null || item.width == 0 || item.height == null || item.height == 0) && typeof item.selector == 'string'){
				item.auto = item.auto || { width: false, height: false };
				var $clone, styles, $item = $(item.selector);
				if ($item.length > 0 && $item.children().length > 0){
					styles = _this.getCSS($item);
					$clone = $item.clone().css(styles);
				} else if (FooBox.isjQuery(item.content)) {
					styles = _this.getCSS(item.content);
					$clone = item.content.clone().css(styles);
				}
				if (FooBox.isjQuery($clone)){
					var $spacer = _this.FooBox.modal.element.find(".fbx-inner-spacer"),
							paddingLeft = parseInt($spacer.css('padding-left')), paddingRight = parseInt($spacer.css('padding-right')),
							padding = (isNaN(paddingLeft) ? 0 : paddingLeft) + (isNaN(paddingRight) ? 0 : paddingRight),
							$inner = _this.FooBox.modal.element.find(".fbx-inner"),
							borderLeft = parseInt($inner.css('border-left')), borderRight = parseInt($inner.css('border-right')),
							border = (isNaN(borderLeft) ? 0 : borderLeft) + (isNaN(borderRight) ? 0 : borderRight),
							maxWidth = Math.max(document.documentElement.clientWidth, window.innerWidth, 0) - padding - border;
					$clone.css({ position: 'absolute', visibility: 'hidden', display: 'block', top: -10000, left: -10000, maxWidth: maxWidth }).appendTo('body');
					if (item.auto.width == true || item.width == null || item.width == 0){
						item.auto.width = true;
						item.width = $clone.outerWidth(true);
					} else if (!!item.width){
						$clone.width(item.width);
					}
					if (item.auto.height == true || item.height == null || item.height == 0){
						item.auto.height = true;
						item.height = $clone.outerHeight(true);
					}
					$clone.remove();
				}
			}

			if (item.width != null && item.height != null) {
				return new FooBox.Size(item.width, item.height);
			}
			// If all else fails... return (0, 0)
			return new FooBox.Size(0, 0);
		};

		/**
		 * This is called prior to displaying the item. You can perform various checks here to determine whether or not the item has changed since it's last display.
		 * @param {FooBox.Item} item - The item to check for changes.
		 * @returns {boolean} - True if the item has changed otherwise false.
		 */
		this.hasChanged = function () {
			return false;
		};
	};

	FooBox.handlers.register(FooBox.EmbedHandler, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox ImageHandler - A handler providing support for image items for the FooBox plugin.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the images handler. */
	var defaults = {
		/** @type {Object} - An object containing image handler related options. */
		images: {
			/** @type {boolean} - A Boolean indicating whether or not to allow users to right click on an image. */
			noRightClick: false,
			/** @type {string} - A String determining the attribute to check on the element. */
			attr: 'href',
			/** @type {boolean} - A Boolean indicating whether or not to allow overflow by default on image items. */
			overflow: false,
			/** @type {boolean} - A Boolean indicating whether or not to allow fullscreen by default on image items. */
			fullscreen: true,
			/** @type {boolean} - A Boolean indicating whether or not to display captions on a IMAGE item. */
			showCaptions: true,
			/** @type {RegExp} - A regular expression used to check whether or not this handler handles an item. */
			regex: /https?:\/\/.*?\/.*?\.(jpg|jpeg|png|gif|bmp|webp|svg)/i,
			/** @type {number} - A Number specifying the maximum allowed width for images. Any images exceeding this size will be resized down while maintaining it's proportions. */
			maxWidth: -1,
			/** @type {number} - A Number specifying the maximum allowed height for images. Any images exceeding this size will be resized down while maintaining it's proportions. */
			maxHeight: -1,
			/**
			 * @type {function} - A Function to use to 'find' the url on an element.
			 * @param {FooBox.Instance} foobox - The parent instance of FooBox.
			 * @param {jQuery} element - The jQuery element to find the selector on.
			 * @returns {string}
			 */
			findUrl: function (foobox, element) {
				if (!element) { return ''; }
				if (element.data('image')) { return element.data('image'); }
				var attr = element.attr(foobox.options.images.attr);
				return (typeof attr == 'string') ? FooBox.qualifiedURL(element.attr(foobox.options.images.attr)) : '';
			}
		}
	};

	/**
	 * Extend the FooBox.Item with additional properties used by this addon.
	 */
	/** @type {Image} - The image for this item. */
	FooBox.Item.prototype.image = null;
	/** @type {number} - The max width for this items image. */
	FooBox.Item.prototype.maxWidth = null;
	/** @type {number} - The max height for this items image. */
	FooBox.Item.prototype.maxHeight = null;

	/**
	 * The core logic for the FooBox.ImageHandler addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this handler.
	 * @constructor
	 */
	FooBox.ImageHandler = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this handler. */
		this.FooBox = instance;
		/** @type {string} - A String representing the type this handler parses. */
		this.type = 'image';

		/**
		 * @type {FooBox.ImageHandler} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * Determines whether or not this handler handles the element supplied.
		 * @param {jQuery} element - The jQuery element to check.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @returns {boolean} - True if this handler handles the supplied element.
		 */
		this.handles = function (element) {
			var url = _this.FooBox.options.images.findUrl(_this.FooBox, element);
			var handle = typeof url === 'string' && url.match(_this.FooBox.options.images.regex) != null;
			var e = _this.FooBox.raise('foobox.handlesImage', { 'element': element, 'handle': handle });
			return e.fb.handle;
		};

		/**
		 * Sets the default values for an item parsed by this handler.
		 * @param {FooBox.Item} item - The new item to set default values on.
		 */
		this.defaults = function (item) {
			item.fullscreen = item.fullscreen || _this.FooBox.options.images.fullscreen;
			item.captions = item.captions || _this.FooBox.options.images.showCaptions;
			item.social = item.social || true;
			item.overflow = item.overflow || _this.FooBox.options.images.overflow;
			item.proportion = item.proportion || true;
			item.maxWidth = item.maxWidth || _this.FooBox.options.images.maxWidth;
			item.maxHeight = item.maxHeight || _this.FooBox.options.images.maxHeight;
		};

		/**
		 * Parses the supplied element into an item for use by FooBox.
		 * @param {jQuery} element - The jQuery element to parse into an item.
		 * @returns {FooBox.Item}
		 */
		this.parse = function (element) {
			var item = new FooBox.Item(_this.type, element, this);

			_this.defaults(item);

			item.url = _this.FooBox.options.images.findUrl(_this.FooBox, element) || null;
			item.width = FooBox.Size.check(element.data('width') || item.width || null);
			item.height = FooBox.Size.check(element.data('height') || item.height || null);
			item.fullscreen = typeof element.data('fullscreen') == 'boolean' ? element.data('fullscreen') : item.fullscreen;
			item.overflow = typeof element.data('overflow') == 'boolean' ? element.data('overflow') : item.overflow;
			item.proportion = typeof element.data('proportion') == 'boolean' ? element.data('proportion') : item.proportion;
			item.maxWidth = typeof element.data('maxWidth') == 'number' ? element.data('maxWidth') : item.maxWidth;
			item.maxHeight = typeof element.data('maxHeight') == 'number' ? element.data('maxHeight') : item.maxHeight;
			item.image = null;
			item.image_url = item.url;

			return item;
		};

		/**
		 * Attempts to load the supplied items content executing the supplied callbacks as required.
		 * @param {FooBox.Item} item - The item to load.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @param {function} success - The function to execute if the loading succeeds. This will be supplied the size of the item as the first parameter.
		 * @param {function} error - The function to execute if the loading fails.
		 */
		this.load = function (item, container, success, error) {
			try {
				var _load = function () {
					var $img = $(item.image).addClass('fbx-item fbx-item-image');
					if (_this.FooBox.options.images.noRightClick) {
						$img.on('contextmenu', function (e) {
							e.preventDefault();
							return false;
						});
					}
					container.empty().append($img);
					if (FooBox.isFn(success)) { success(_this.getSize(item)); }
				};
				if (!item.image || item.image === null) {
					item.image = new Image();
					item.image.onload = function () {
						item.image.onload = item.image.onerror = null;
						if (!item.width || !item.height){
							item.height = item.image.height;
							item.width = item.image.width;
						}
						_load();
					};
					item.image.onerror = function () {
						item.image.onload = item.image.onerror = null;
						item.image = null;
						if (FooBox.isFn(error)) { error('An error occurred attempting to load the image.'); }
					};
					item.image.src = item.url;
				} else {
					_load();
				}
			} catch (err) {
				if (FooBox.isFn(error)) { error(err); }
			}
		};

		/**
		 * If required preloads the item to reduce load time changing between previous and next items.
		 * @param {FooBox.Item} item - The item to preload if required.
		 */
		this.preload = function (item) {
			if (item.preloaded != true) {
				var image = new Image();
				image.src = item.url;
				item.preloaded = true;
			}
		};

		/**
		 * Retrieves the size for the supplied item. If no width and height is set this should attempt to find the size.
		 * @param {FooBox.Item} item - The item to retrieve the size for.
		 * @returns {FooBox.Size}
		 */
		this.getSize = function (item) {
			if (item.width != null && item.height != null) {
				if (item.maxWidth > 0 && item.maxHeight > 0 && (item.width > item.maxWidth || item.height > item.maxHeight)){
					var ratio = item.width > item.height ? item.maxWidth / item.width : item.maxHeight / item.height;
					item.width *= ratio;
					item.height *= ratio;
				}
				return new FooBox.Size(item.width, item.height);
			}
			// If all else fails... return (0, 0)
			return new FooBox.Size(0, 0);
		};

		/**
		 * This is called prior to displaying the item. You can perform various checks here to determine whether or not the item has changed since it's last display.
		 * @param {FooBox.Item} item - The item to check for changes.
		 * @returns {boolean} - True if the item has changed otherwise false.
		 */
		this.hasChanged = function (item) {
			if (FooBox.isjQuery(item.element)) {
				var actual = _this.FooBox.options.images.findUrl(_this.FooBox, item.element);
				return item.url != actual;
			}
			return false;
		};
	};

	FooBox.handlers.register(FooBox.ImageHandler, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox VideoHandler - A handler providing support for video items for the FooBox plugin.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the video handler. */
	var defaults = {
		/** @type {Object} - An object containing video handler related options. */
		videos: {
			/** @type {string} - A String determining the attribute to check on the element. */
			attr: 'href',
			/** @type {boolean} - A Boolean indicating whether or not to allow fullscreen by default on video items. */
			fullscreen: true,
			/** @type {RegExp} - A regular expression used to check whether or not this handler handles an item. */
			regex: /(youtube(-nocookie)?\.com\/(watch|v|embed)|youtu\.be|vimeo\.com(?!(\/user))|\.mp4|\.ogv|\.wmv|\.webm|(.+)?(wistia\.(com|net)|wi\.st)\/.*|(www.)?dailymotion\.com|dai\.ly)/i,
			/**
			 * @type {function} - A Function to use to 'find' the url on an element.
			 * @param {FooBox.Instance} foobox - The parent instance of FooBox.
			 * @param {jQuery} element - The jQuery element to find the selector on.
			 * @returns {string}
			 */
			findUrl: function (foobox, element) {
				if (!element) { return ''; }
				var attr = element.attr(foobox.options.videos.attr);
				return (typeof attr == 'string') ? FooBox.qualifiedURL(element.attr(foobox.options.videos.attr)) : '';
			},
			/** @type {boolean} - A Boolean indicating whether or not to autoplay a video item. */
			autoPlay: false,
			/** @type {number} - The default width to use for video items if none is supplied. */
			defaultWidth: 640,
			/** @type {number} - The default height to use for video items if none is supplied. */
			defaultHeight: 385,
			/** @type {boolean} - A Boolean indicating whether or not to display captions on a VIDEO item. */
			showCaptions: false,
			/** @type {boolean} - A Boolean indicating whether or not to display the modal immediately or to wait for the IFRAME to load. */
			showImmediate: false,
			/** @type {number} - A double indicating the audio volume, from 0.0 (silent) to 1.0 (loudest). */
			volume: 0.2
		}
	};

	/** @type {Array.<FooBox.VideoUrl>} - The url for the video. */
	FooBox.Item.prototype.video_url = null;
	/** @type {string} - The url for the video's image. */
	FooBox.Item.prototype.image_url = null;

	FooBox.Item.prototype.mimeType = null;
	FooBox.Item.prototype.player = null;
	FooBox.Item.prototype.$player = null;


	FooBox.VideoUrl = function(url, autoPlay){
		if (!(this instanceof FooBox.VideoUrl)) return new FooBox.VideoUrl(url);
		this.autoPlay = autoPlay;
		var parts = url.split('#');
		this.hash = parts.length == 2 ? '#'+parts[1] : '';
		parts = parts[0].split('?');
		this.url = parts[0];
		var match = this.url.match(/.*\/(.*)$/);
		this.id = match && match.length >= 2 ? match[1] : null;
		this.protocol = window.location.protocol === 'https:' ? 'https:' : (url.substring(0,5) == 'https' ? 'https:' : 'http:');
		this.params = [];
		var params = (parts.length == 2 ? parts[1] : '').split(/[&;]/g);
		for (var i = 0, len = params.length, pair; i < len; i++){
			pair = params[i].split('=');
			if (pair.length != 2) continue;
			this.params.push({key: decodeURIComponent(pair[0]), value: decodeURIComponent(pair[1])});
		}

		this.mimeTypes = { // list of supported mimeTypes and the regex used to test a url
			'video/youtube': /(www.)?youtube|youtu\.be/i,
			'video/vimeo': /(player.)?vimeo\.com/i,
			'video/wistia': /(.+)?(wistia\.(com|net)|wi\.st)\/.*/i,
			'video/daily': /(www.)?dailymotion\.com|dai\.ly/i,
			'video/mp4': /\.mp4/i,
			'video/webm': /\.webm/i,
			'video/wmv': /\.wmv/i,
			'video/ogg': /\.ogv/i
		};
		this.mimeType = null;
		for (var name in this.mimeTypes){
			if (this.mimeTypes.hasOwnProperty(name) && this.mimeTypes[name].test(url))
				this.mimeType = name;
		}

		var ua = navigator.userAgent.toLowerCase(), ie = ua.indexOf('msie ') > -1 || ua.indexOf('trident/') > -1 || ua.indexOf('edge/') > -1, ie8orless = !document.addEventListener;
		this.isDirectLink = FooBox.inArray(this.mimeType, ['video/mp4','video/wmv','video/ogg','video/webm']) !== -1;
		this.isBrowserSupported = FooBox.inArray(this.mimeType, ie ? ie8orless ? [] : ['video/mp4','video/wmv'] : ['video/mp4','video/ogg','video/webm']) !== -1;

		if (this.mimeType === 'video/youtube'){
			this.id = /embed\//i.test(this.url)
				? this.url.split(/embed\//i)[1].split(/[?&]/)[0]
				: url.split(/v\/|v=|youtu\.be\//i)[1].split(/[?&]/)[0];
			this.url = this.protocol + '//www.youtube-nocookie.com/embed/' + this.id;
			if (this.autoPlay) this.param('autoplay', '1');
			this.param('modestbranding', '1');
			this.param('rel', '0');
			this.param('wmode', 'transparent');
			this.param('showinfo', '0');
		} else if (this.mimeType === 'video/vimeo'){
			var showcaseRegex = /\/showcase\/(\d*)?\/?/, showcase = showcaseRegex.test(this.url);
			this.id = showcase ? showcaseRegex.exec(this.url)[1] : this.url.substr(this.url.lastIndexOf('/')+1);
			this.url = this.protocol + (showcase ? '//vimeo.com/showcase/' : '//player.vimeo.com/video/') + this.id + (showcase ? '/embed' : '');
			if (this.autoPlay) this.param('autoplay', '1');
			this.param('badge', '0');
			this.param('portrait', '0');
		} else if (this.mimeType === 'video/wistia'){
			this.id = /embed\//i.test(this.url)
				? this.url.split(/embed\/.*?\//i)[1].split(/[?&]/)[0]
				: this.url.split(/medias\//)[1].split(/[?&]/)[0];
			var playlist = /playlists\//i.test(this.url);
			this.url = this.protocol + '//fast.wistia.net/embed/'+(playlist ? 'playlists' : 'iframe')+'/'+this.id;
			if (this.autoPlay){
				if (playlist) this.param('media_0_0[autoPlay]', '1');
				else this.param('autoPlay', '1');
			}
			this.param('theme', '');
		} else if (this.mimeType === 'video/daily'){
			this.id = /\/video\//i.test(this.url)
				? this.url.split(/\/video\//i)[1].split(/[?&]/)[0].split(/[_]/)[0]
				: url.split(/dai\.ly/i)[1].split(/[?&]/)[0];
			this.url = this.protocol + '//www.dailymotion.com/embed/video/' + this.id;
			if (this.autoPlay) this.param('autoplay', '1');
			this.param('wmode', 'opaque');
			this.param('info', '0');
			this.param('logo', '0');
			this.param('related', '0');
		}
		this.autoPlay = /[?&](autoplay|autoPlay|media_0_0(\[|%5B)autoPlay(]|%5D))=(1|true)([&]|$)/.test(this.toString());
	};

	FooBox.VideoUrl.prototype.param = function(key, value){
		var GET = typeof value === 'undefined', DELETE = typeof value === 'string' && value === '';
		for (var i = this.params.length; i-- > 0;) {
			if (this.params[i].key === key) {
				if (GET) return this.params[i].value;
				if (DELETE) this.params.splice(i, 1);
				else this.params[i].value = value;
				return;
			}
		}
		if (!GET && !DELETE) this.params.push({key: key, value: value});
	};

	FooBox.VideoUrl.prototype.toString = function(){
		var params = this.params.length > 0 ? '?' : '';
		for (var i = 0, len = this.params.length; i < len; i++){
			if (i !== 0) params += '&';
			params += encodeURIComponent(this.params[i].key) + '=' + encodeURIComponent(this.params[i].value);
		}
		return this.url + params + this.hash;
	};

	/**
	 * The core logic for the FooBox.VideoHandler addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this handler.
	 * @constructor
	 */
	FooBox.VideoHandler = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this handler. */
		this.FooBox = instance;
		/** @type {string} - A String representing the type this handler parses. */
		this.type = 'video';

		this.volume = !!instance ? instance.options.videos.volume : 0.2;

		/**
		 * @type {FooBox.VideoHandler} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * Determines whether or not this handler handles the element supplied.
		 * @param {jQuery} element - The jQuery element to check.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @returns {boolean} - True if this handler handles the supplied element.
		 */
		this.handles = function (element) {
			var url = _this.FooBox.options.videos.findUrl(_this.FooBox, element);
			var handle = typeof url === 'string' && url.match(_this.FooBox.options.videos.regex) != null;
			var e = _this.FooBox.raise('foobox.handlesVideo', { 'element': element, 'handle': handle });
			return e.fb.handle;
		};

		/**
		 * Sets the default values for an item parsed by this handler.
		 * @param {FooBox.Item} item - The new item to set default values on.
		 */
		this.defaults = function (item) {
			item.width = item.width || _this.FooBox.options.videos.defaultWidth;
			item.height = item.height || _this.FooBox.options.videos.defaultHeight;
			item.fullscreen = item.fullscreen || _this.FooBox.options.videos.fullscreen;
			item.social = item.social || true;
			item.captions = item.captions || _this.FooBox.options.videos.showCaptions;
			item.proportion = item.proportion || true;
		};

		/**
		 * Parses the supplied element into an item for use by FooBox.
		 * @param {jQuery} element - The jQuery element to parse into an item.
		 * @returns {FooBox.Item}
		 */
		this.parse = function (element) {
			var item = new FooBox.Item(_this.type, element, this);

			_this.defaults(item);

			item.url = _this.FooBox.options.videos.findUrl(_this.FooBox, element) || null;
			item.width = FooBox.Size.check(element.data('width') || item.width || null);
			item.height = FooBox.Size.check(element.data('height') || item.height || null);
			item.image_url = element.data('image') || element.data('cover') || element.find('img').attr('src') || null;
			item.fullscreen = typeof element.data('fullscreen') == 'boolean' ? element.data('fullscreen') : item.fullscreen;
			item.proportion = typeof element.data('proportion') == 'boolean' ? element.data('proportion') : item.proportion;
			item.autoPlay = _this.FooBox.options.videos.autoPlay;

			item.video_url = [];
			if (item.url !== null){
				item.video_url = decodeURI(item.url).split(',');
				for (var i = 0, len = item.video_url.length; i < len; i++){
					item.video_url[i] = new FooBox.VideoUrl(FooBox.trim(item.video_url[i]), _this.FooBox.options.videos.autoPlay);
					if (item.video_url[i].autoPlay) item.autoPlay = true;
				}
			}

			return item;
		};

		this.isDirectLink = function(item){
			if (!document.addEventListener) return false;
			for (var i = 0, len = item.video_url.length; i < len; i++){
				if (item.video_url[i].isDirectLink && item.video_url[i].isBrowserSupported) return true;
			}
			return false;
		};

		/**
		 * Attempts to load the supplied items content executing the supplied callbacks as required.
		 * @param {FooBox.Item} item - The item to load.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @param {function} success - The function to execute if the loading succeeds. This will be supplied the size of the item as the first parameter.
		 * @param {function} error - The function to execute if the loading fails.
		 */
		this.load = function (item, container, success, error) {
			try {
				var $wrapper, $player, i, len;
				if (this.isDirectLink(item)){
					$wrapper = $('<div/>', {'class': 'fbx-item fbx-item-video'});
					$player = $('<video/>', { 'class': 'fbx-video', width: item.width, height: item.height, controls: true, controlsList: "nodownload", preload: false })
							.css({ width: '100%', height: '100%' });
					if (item.image_url != null){
						$player.attr('poster', item.image_url);
					}
					for (i = 0, len = item.video_url.length; i < len; i++){
						$player.append($('<source/>', { type: item.video_url[i].mimeType, src: item.video_url[i].toString() }));
					}
					if (!!document.addEventListener){
						var player = $player[0];
						function onerror(){
							if (FooBox.isFn(error)) { error('The video [' + item.url + '] is not supported'); }
						}
						function onloadeddata(){
							player.removeEventListener('loadeddata', onloadeddata, false);
							player.removeEventListener('error', onerror, false);
							if (item.autoPlay){
								var p = player.play();
								if (typeof p !== 'undefined'){
									p.catch(function(){
										console.log("Unable to autoplay video due to policy changes: https://developers.google.com/web/updates/2017/09/autoplay-policy-changes");
									});
								}
							}
							if (FooBox.isFn(success)) { success(_this.getSize(item)); }
						}
						function onvolumechange(){
							_this.volume = player.volume;
						}
						player.volume = _this.volume;
						player.addEventListener('error', onerror, false);
						player.addEventListener('loadeddata', onloadeddata, false);
						player.addEventListener('volumechange', onvolumechange, false);
						container.empty().append($wrapper.append($player));
						if (player.readyState < 4) player.load();
						else onloadeddata();
					}
				} else if (item.video_url.length === 1 && !item.video_url[0].isDirectLink) {
					var $html = $('<iframe style="width:100%; height:100%" frameborder="no" allow="autoplay; fullscreen" allowfullscreen />')
						.addClass('fbx-item fbx-item-video'),
						size = _this.getSize(item);
					container.empty().append($html);
					if (_this.FooBox.options.videos.showImmediate){
						$html.attr('src', item.video_url[0].toString());
						if (FooBox.isFn(success)) { success(size); }
					} else {
						$html.one('load', function(){
							if (FooBox.isFn(success)) { success(size); }
						}).attr('src', item.video_url[0].toString());
					}
				} else {
					if (FooBox.isFn(error)) { error('The video [' + item.url + '] is not supported'); }
				}
			} catch (err) {
				if (FooBox.isFn(error)) { error(err); }
			}
		};

		/**
		 * If required preloads the item to reduce load time changing between previous and next items.
		 * @param {FooBox.Item} item - The item to preload if required.
		 */
		this.preload = function(item){

		};

		/**
		 * Retrieves the size for the supplied item. If no width and height is set this should attempt to find the size.
		 * @param {FooBox.Item} item - The item to retrieve the size for.
		 * @returns {FooBox.Size}
		 */
		this.getSize = function (item) {
			if (item.width == null || item.height == null) {
				item.width = _this.FooBox.options.videos.defaultWidth;
				item.height = _this.FooBox.options.videos.defaultHeight;
			}
			if (item.width != null && item.height != null) {
				return new FooBox.Size(item.width, item.height);
			}
			// If all else fails... return (0, 0)
			return new FooBox.Size(0, 0);
		};

		/**
		 * This is called prior to displaying the item. You can perform various checks here to determine whether or not the item has changed since it's last display.
		 * @param {FooBox.Item} item - The item to check for changes.
		 * @returns {boolean} - True if the item has changed otherwise false.
		 */
		this.hasChanged = function () {
			return false;
		};

		/**
		 * Retrieves a url parameter value from the supplied url.
		 * @param {string} name - The name of the url parameter.
		 * @param {string} url - The url to retrieve the value from.
		 * @returns {string}
		 */
		this.getUrlVar = function (name, url) {
			name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
				results = regex.exec(url);
			return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};

		/**
		 * Safely appends a url parameter value onto the end of the supplied url.
		 * @param {string} append - The key-value to append.
		 * @param {string} url - The url you want to append to.
		 * @returns {string}
		 */
		this.appendUrlVar = function (append, url) {
			var separator = url.indexOf('?') !== -1 ? '&' : '?';
			return url + separator + append;
		};
	};

	FooBox.handlers.register(FooBox.VideoHandler, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox IframeHandler - A handler providing support for iframe items for the FooBox plugin.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the iframe handler. */
	var defaults = {
		/** @type {Object} - An object containing iframe handler related options. */
		iframe: {
			/** @type {string} - A String determining the attribute to check on the element. */
			attr: 'href',
			/** @type {boolean} - A Boolean indicating whether or not to allow fullscreen by default on IFRAME items. */
			fullscreen: true,
			/** @type {boolean} - A Boolean indicating whether or not to display captions on a IFRAME item. */
			showCaptions: false,
			/** @type {boolean} - A Boolean indicating whether or not to display the modal immediately or to wait for the IFRAME to load. */
			showImmediate: false,
			/** @type {boolean} - A Boolean indicating whether or not to add the allowfullscreen attribute onto the IFRAME. */
			allowFullscreen: false,
			/** @type {RegExp} - A regular expression used to check whether or not this handler handles an item. */
			regex: /^https?/i,
			/** @type {RegExp} - A regular expression used to exclude an item. */
			exclude: /(youtube(-nocookie)?\.com\/(watch|v|embed)|youtu\.be|vimeo\.com(?!(\/user))|\.mp4|\.ogv|\.wmv|\.webm|(.+)?(wistia\.(com|net)|wi\.st)\/.*|(www.)?dailymotion\.com|dai\.ly|\.(jpg|jpeg|png|gif|bmp|webp|svg))/i,
			/**
			 * @type {function} - A Function to use to 'find' the url on an element.
			 * @param {FooBox.Instance} foobox - The parent instance of FooBox.
			 * @param {jQuery} element - The jQuery element to find the selector on.
			 * @returns {string}
			 */
			findUrl: function (foobox, element) {
				if (!element) { return ''; }
				var attr = element.attr(foobox.options.iframe.attr);
				return (typeof attr == 'string') ? FooBox.qualifiedURL(element.attr(foobox.options.iframe.attr)) : '';
			}
		}
	};

	/**
	 * The core logic for the FooBox.IframeHandler addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this handler.
	 * @constructor
	 */
	FooBox.IframeHandler = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this handler. */
		this.FooBox = instance;
		/** @type {string} - A String representing the type this handler parses. */
		this.type = 'iframe';

		/**
		 * @type {FooBox.IframeHandler} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * Determines whether or not this handler handles the element supplied.
		 * @param {jQuery} element - The jQuery element to check.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @returns {boolean} - True if this handler handles the supplied element.
		 */
		this.handles = function (element) {
			var href = _this.FooBox.options.iframe.findUrl(_this.FooBox, element);
			var handle = $(element).attr('target') === 'foobox' && typeof href === 'string' && href.match(_this.FooBox.options.iframe.regex) != null && !href.match(_this.FooBox.options.iframe.exclude);
			var e = _this.FooBox.raise('foobox.handlesIframe', { 'element': element, 'handle': handle });
			return e.fb.handle;
		};

		/**
		 * Sets the default values for an item parsed by this handler.
		 * @param {FooBox.Item} item - The new item to set default values on.
		 */
		this.defaults = function (item) {
			item.fullscreen = item.fullscreen || _this.FooBox.options.iframe.fullscreen;
			item.overflow = item.overflow || false;
			item.social = item.social || true;
			item.proportion = item.proportion || false;
			item.captions = item.captions || _this.FooBox.options.iframe.showCaptions;
		};

		/**
		 * Parses the supplied element into an item for use by FooBox.
		 * @param {jQuery} element - The jQuery element to parse into an item.
		 * @returns {FooBox.Item}
		 */
		this.parse = function (element) {
			var item = new FooBox.Item(_this.type, element, this);

			_this.defaults(item);

			item.url = _this.FooBox.options.iframe.findUrl(_this.FooBox, element) || null;
			item.width = FooBox.Size.check(element.data('width') || item.width || null);
			item.height = FooBox.Size.check(element.data('height') || item.height || null);
			item.overflow = typeof element.data('overflow') == 'boolean' ? element.data('overflow') : item.overflow;
			item.fullscreen = typeof element.data('fullscreen') == 'boolean' ? element.data('fullscreen') : item.fullscreen;
			item.proportion = typeof element.data('proportion') == 'boolean' ? element.data('proportion') : item.proportion;
			item.image_url = element.data('image') || '';
			return item;
		};

		/**
		 * Attempts to load the supplied items content executing the supplied callbacks as required.
		 * @param {FooBox.Item} item - The item to load.
		 * @param {jQuery} container - The jQuery element the core FooBox plugin was initialized on.
		 * @param {function} success - The function to execute if the loading succeeds. This will be supplied the size of the item as the first parameter.
		 * @param {function} error - The function to execute if the loading fails.
		 */
		this.load = function (item, container, success, error) {
			try {
				var size = _this.getSize(item);
				var $html = $(FooBox.browser.iOS12OrBelow
					? '<iframe framborder="0" width="100%" scrolling="no" allow="autoplay; fullscreen" allowfullscreen />'
					: '<iframe framborder="0" allow="autoplay; fullscreen" allowfullscreen />').addClass('fbx-item fbx-item-iframe').css({
					width: size.width,
					height: size.height
				});
				if (_this.FooBox.options.iframe.allowFullscreen && item.fullscreen) {
					$html.attr('allowfullscreen', '').attr("allow", "autoplay");
				}
				container.empty().append($html);
				if (_this.FooBox.options.iframe.showImmediate){
					$html.attr('src', item.url);
					if (FooBox.isFn(success)) { success(size); }
				} else {
					$html.one('load', function(){
						if (FooBox.isFn(success)) { success(size); }
					}).attr('src', item.url);
				}
			} catch (err) {
				if (FooBox.isFn(error)) { error(err); }
			}
		};

		/**
		 * If required preloads the item to reduce load time changing between previous and next items.
		 * @param {FooBox.Item} item - The item to preload if required.
		 */
		this.preload = function(item){

		};

		/**
		 * Retrieves the size for the supplied item. If no width and height is set this should attempt to find the size.
		 * @param {FooBox.Item} item - The item to retrieve the size for.
		 * @returns {FooBox.Size}
		 */
		this.getSize = function (item) {
			if (item.auto || item.width == null || item.width == 0 || item.height == null || item.height == 0){
				item.auto = item.auto || { width: false, height: false };
				if (item.auto.width == true || item.width == null || item.width == 0){
					item.auto.width = true;
					item.width = (window.innerWidth || document.documentElement.clientWidth || (document.body ? document.body.offsetWidth : 0)) * 0.8;
				}
				if (item.auto.height == true || item.height == null || item.height == 0){
					item.auto.height = true;
					item.height = (window.innerHeight || document.documentElement.clientHeight || (document.body ? document.body.offsetHeight : 0)) * 0.8;
				}
			}

			if (item.width != null && item.height != null) {
				return new FooBox.Size(item.width, item.height);
			}
			// If all else fails... return (0, 0)
			return new FooBox.Size(0, 0);
		};

		/**
		 * This is called prior to displaying the item. You can perform various checks here to determine whether or not the item has changed since it's last display.
		 * @param {FooBox.Item} item - The item to check for changes.
		 * @returns {boolean} - True if the item has changed otherwise false.
		 */
		this.hasChanged = function () {
			return false;
		};
	};

	FooBox.handlers.register(FooBox.IframeHandler, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Captions - An addon providing captions for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the caption addon. */
	var defaults = {
		/** @type {Object} - An object containing caption related options. */
		captions: {
			/** @type {string} - A String used to determine the type of animation to use when showing/hiding the caption. Available animations are 'slide', 'fade' and 'show'. */
			animation: 'slide',
			/** @type {boolean} - A Boolean indicating whether or not the captions feature is enabled. */
			enabled: true,
			/** @type {string} - A String used to determine how to find the description used for captions, the title attribute is interrogated for the value. Available options are 'find', 'image_find', 'image', 'image_alt', 'anchor' and 'none'. */
			descSource: 'find',
			/** @type {number} - A Number determining the amount of milliseconds to wait before showing the caption when the onlyShowOnHover option is set to true. If the mouse cursor exits the FooBox before this value the caption will not be shown at all. */
			hoverDelay: 300,
			/** @type {number} - A Number determining the maximum percentage the caption can occupy before the description is auto hidden. */
			maxHeight: 0.4,
			/** @type {boolean} - A Boolean indicating whether or not to only show the captions when hovering over the FooBox. */
			onlyShowOnHover: false,
			/** @type {boolean} - A Boolean indicating whether or not to only show the captions when clicking the FooBox. */
			onlyShowOnClick: false,
			/** @type {boolean} -  */
			overrideDesc: false,
			/** @type {boolean} -  */
			overrideTitle: false,
			/** @type {boolean} - A Boolean indicating whether or not to try 'prettify' the caption (removes dashes and trailing numbers and converts to sentence case). */
			prettify: false,
			/** @type {string} - A String used to determine how to find the title used for captions, the title attribute is interrogated for the value. Available options are 'find', 'image_find', 'image', 'image_alt', 'anchor' and 'none'. */
			titleSource: 'image_find',
			/** @type {string[]} - A string array containing the data- attributes to check for title values */
			dataTitle: ['captionTitle', 'title'],
			/** @type {string[]} - A string array containing the data- attributes to check for description values */
			dataDesc: ['captionDesc', 'description']
		}
	};

	/**
	 * Extend the FooBox.Item with additional properties used by this addon.
	 */
	/** @type {boolean} - Whether or not this item allows captions to be displayed. */
	FooBox.Item.prototype.captions = false;
	/** @type {string} - The title of item. */
	FooBox.Item.prototype.title = null;
	/** @type {string} - The description of item. */
	FooBox.Item.prototype.description = null;
	/** @type {string} - The caption for the item. */
	FooBox.Item.prototype.caption = null;

	/**
	 * The core logic for the FooBox.Captions addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Captions = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {Object} - An object containing any timers required by this addon. */
		this.timers = {
			/** @type {FooBox.Timer} - A timer used to create a hover delay. */
			hover: new FooBox.Timer()
		};

		/**
		 * @type {FooBox.Captions} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupHtml': _this.handlers.setupHtml,
				'foobox.setupOptions': _this.handlers.setupOptions,
				'foobox.parseItem': _this.handlers.parseItem,
				'foobox.onError': _this.handlers.onError,
				'foobox.beforeClose': _this.handlers.beforeClose
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function() {
			_this.handlers.unbind(true);
		};

		/**
		 * Gets the first found value for an element given an array of data- attributes to check.
		 * @param {jQuery} $element - The element to pull the data from.
		 * @param {string[]} names - An array containing the data- attribute names to check for a value.
		 * @param {string} src - The "source" to use to find a value if all else fails.
		 * @returns {string|null}
		 */
		this.getDataValue = function($element, names, src){
			if (src === 'none') return '';
			var found = null, i = 0, l = names.length;
			for (; i < l; i++){
				var tmp = $element.data(names[i]);
				if (typeof tmp === 'string'){
					found = tmp;
					break;
				}
			}
			if (found === null){
				found = _this.text($element, src);
			}
			return found;
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all) {
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupHtml': _this.handlers.setupHtml,
						'foobox.setupOptions': _this.handlers.setupOptions,
						'foobox.parseItem': _this.handlers.parseItem,
						'foobox.onError': _this.handlers.onError,
						'foobox.beforeClose': _this.handlers.beforeClose
					});
				}
				_this.FooBox.element.off({
					'foobox.beforeLoad': _this.handlers.beforeLoad,
					'foobox.afterLoad': _this.handlers.afterLoad
				});
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.off('mouseenter.captions mouseleave.captions')
						.find('.fbx-item-current, .fbx-item-next')
						.off('click.captions');
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (e.fb.options.captions.enabled === true) {
					e.fb.instance.element.on({
						'foobox.beforeLoad': _this.handlers.beforeLoad,
						'foobox.afterLoad': _this.handlers.afterLoad,
						'foobox.beforeClose': _this.handlers.beforeClose
					});
					if (e.fb.options.modalClass.indexOf("fbx-caption-toggle-only") === -1){
						e.fb.modal.find('.fbx-item-current, .fbx-item-next').on('click.captions', _this.handlers.toggleCaptions);
					}
					if (e.fb.options.captions.onlyShowOnHover === true) {
						e.fb.modal.on('mouseenter.captions', '.fbx-inner:not(:has(.fbx-item-error))', _this.handlers.mouseenter)
							.on('mouseleave.captions', '.fbx-inner:not(:has(.fbx-item-error))', _this.handlers.mouseleave);
					}
				}
			},
			/**
			 * Handles the standard jQuery click event on the current item to toggle the captions.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			toggleCaptions: function (e) {
				if ($(e.target).is("img") || $(e.target).is(".fbx-close-caption") || $(e.target).is(".fbx-open-caption")){
					var item = _this.FooBox.items.current(),
						$modal = _this.FooBox.modal.element;
					if (item.captions && !$modal.hasClass('fbx-error')){
						e.preventDefault();
						if ($modal.find('.fbx-caption').is('.fbx-fade-caption, .fbx-slide-caption, .fbx-hide-caption')) {
							$modal.removeClass('fbx-captions-hidden');
							_this.show();
						} else {
							$modal.addClass('fbx-captions-hidden');
							_this.hide();
						}
					}
				}
			},
			/**
			 * Handles the standard jQuery mouseenter event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseenter: function () {
				_this.timers.hover.start(function () {
					_this.show();
				}, _this.FooBox.options.captions.hoverDelay);
			},
			/**
			 * Handles the standard jQuery mouseleave event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseleave: function () {
				_this.timers.hover.start(function () {
					_this.hide();
				}, _this.FooBox.options.captions.hoverDelay);
			},
			/**
			 * Handles the foobox.setupHtml event allowing this addon to create any required HTML.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupHtml: function (e) {
				var $caption = $('<div/>', {'class': 'fbx-caption'}),
					$open_caption = $('<a/>', {'class': 'fbx-open-caption', html: '&hellip;'}).on('click.captions',_this.handlers.toggleCaptions);
				if (typeof e.fb.options.modalClass == 'string' && e.fb.options.modalClass.indexOf('fbx-sticky-caption') != -1){
					e.fb.modal.append($caption, $open_caption);
				} else {
					e.fb.modal.find('.fbx-stage').append($caption);
					e.fb.modal.find('.fbx-inner').append($open_caption);
				}
			},
			/**
			 * Handles the foobox.setupOptions event allowing this addon to set its initial starting state.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupOptions: function (e) {
				e.fb.modal.find('.fbx-caption').addClass(_this.animation());
			},
			/**
			 * Handles the foobox.beforeClose event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeClose: function (e) {
				if (e.fb.options.captions.onlyShowOnHover == true){	return;	}
				e.fb.modal.find('.fbx-caption').addClass("fbx-hide-caption");
			},
			/**
			 * Handles the foobox.beforeLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeLoad: function (e) {
				if (e.fb.options.captions.onlyShowOnHover == true){	return;	}
				e.fb.modal.find('.fbx-caption').addClass(_this.animation());
			},
			/**
			 * Handles the foobox.afterLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterLoad: function (e) {
				var item = e.fb.item;
				
				if (item && !item.error && !item.captionLoaded) {
						e.fb.instance.raise('foobox.alterCaption', { 'element': e.fb.element, 'item': item });
						item.captionLoaded = true;
				}
				
				if (e.fb.options.captions.onlyShowOnHover == true){
					_this.update();
					return;
				}
				
				if (e.fb.options.captions.onlyShowOnClick == false){
					_this.show();
				}
			},
			/**
			 * Handles the foobox.onError event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onError: function (e) {
				e.fb.modal.find('.fbx-caption').addClass(_this.animation());
			},
			/**
			 * Handles the foobox.parseItem event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			parseItem: function (e) {
				var opts = e.fb.options.captions;
				if (!e.fb.item.captions || opts.enabled == false) return;
				var caption = '', title, desc;
				if (e.fb.element != null) {
					var tSrc = $(e.fb.element).data('titleSource') || $(e.fb.instance.element).data('titleSource') || opts.titleSource,
						dSrc = $(e.fb.element).data('descSource') || $(e.fb.instance.element).data('descSource') || opts.descSource;

					title = _this.getDataValue(e.fb.element, opts.dataTitle, tSrc);
					desc = _this.getDataValue(e.fb.element, opts.dataDesc, dSrc);
				} else {
					title = e.fb.item.title;
					desc = e.fb.item.description;
				}

				// ensure working with strings
				title = (title == null || typeof title === 'undefined' ? '' : title + '');
				desc = (desc == null || typeof desc === 'undefined' ? '' : desc + '');

				if (title && title === desc) desc = null;

				caption = typeof title === 'string' && title.length > 0 ? '<div class="fbx-caption-title">' + title + '</div>' : caption;
				caption = typeof desc === 'string' && desc.length > 0 ? caption + '<div class="fbx-caption-desc">' + desc + '</div>' : caption;

				e.fb.item.title = title;
				e.fb.item.description = desc;
				e.fb.item.caption = caption;

				e.fb.instance.raise('foobox.createCaption', { 'element': e.fb.element, 'item': e.fb.item });
			}
		};

		/**
		 * Attempts to retrieve text from the supplied element using the method defined by source.
		 * @param {jQuery} element - The jQuery element to retrieve text from.
		 * @param {string} source - The source which defines how to retrieve the text from the element. Available options are 'find', 'image_find', 'image', 'image_alt', 'anchor' and 'none'.
		 * @returns {(string|null)} - null if no valid source is supplied.
		 */
		this.text = function (element, source) {
			var result;
			switch (source) {
				case 'find':
					result = FooBox.trim(element.data('title') || element.attr('title') || element.find('img:first').data('title') || element.find('img:first').data('alt') || element.find('img:first').attr('title') || element.find('img:first').attr('alt'));
					break;
				case 'image_find':
					result = FooBox.trim(element.find('img:first').data('title') || element.find('img:first').data('alt') || element.find('img:first').attr('title') || element.find('img:first').attr('alt'));
					break;
				case 'image':
					result = FooBox.trim(element.find('img:first').data('title') || element.find('img:first').attr('title'));
					break;
				case 'image_alt':
					result = FooBox.trim(element.find('img:first').data('alt') || element.find('img:first').attr('alt'));
					break;
				case 'anchor':
					result = FooBox.trim(element.data('title') || element.attr('title'));
					break;
				default:
					result = null;
					break;
			}
			if (_this.FooBox.options.captions.prettify) {
				result = _this.prettifier(result);
			}
			return result;
		};

		this.animation = function(){
			switch (_this.FooBox.options.captions.animation) {
				case 'fade':
					return 'fbx-fade-caption';
				case 'slide':
					return 'fbx-slide-caption';
				default:
					return 'fbx-hide-caption';
			}
		};

		/**
		 * Hides the caption taking into account the various options for animation.
		 */
		this.hide = function () {
			var item = _this.FooBox.items.current(),
				$caption = _this.FooBox.modal.element.find('.fbx-caption');

			if (!_this.FooBox.options.captions.enabled || !item.captions || typeof item.caption != 'string' || item.caption.length == 0) {
				$caption.addClass('fbx-hide-caption');
				return;
			}
			$caption.addClass(_this.animation());
			_this.FooBox.raise('foobox.captionsHide', { item: item });
		};

		/**
		 * Shows the caption taking into account the various options for animation as well as parsing the caption for foobox enabled links.
		 */
		this.show = function () {
			var _fb = _this.FooBox,
				item = _fb.items.current(),
				$caption = _fb.modal.element.find('.fbx-caption');

			if (!_fb.options.captions.enabled || !item.captions || typeof item.caption != 'string' || item.caption.length == 0 || _fb.modal.element.hasClass('fbx-captions-hidden') || (FooBox.isjQuery(item.element) && item.element.hasClass('no-captions'))) {
				$caption.addClass('fbx-hide-caption');
				return;
			}

			_this.update();
			$caption.removeClass('fbx-fade-caption fbx-slide-caption fbx-hide-caption');
			_this.FooBox.raise('foobox.captionsShow', { item: item });
		};

		this.update = function() {
			var item = _this.FooBox.items.current(),
				$caption = _this.FooBox.modal.element.find('.fbx-caption');

			$caption.html(item.caption)
				.find('a[href^="#"]')
				.filter(function () {
					var identifier = $(this).attr('href'), target = $(identifier), fbx = target.data('fbx_instance') || target.data('fbx_p_instance');
					if (target.length > 0 && fbx instanceof FooBox.Instance) {
						$(this).data('hrefTarget', target.get(0));
						return true;
					}
					return false;
				})
				.off('click.captions')
				.on('click.captions', function (e) {
					e.preventDefault();
					var target = $(this).data('hrefTarget');
					var fbx = $(target).data('fbx_instance') || $(target).data('fbx_p_instance');
					if (fbx instanceof FooBox.Instance) {
						_this.FooBox.modal.close();
						fbx.items.indexes.set(target.index);
						fbx.modal.show();
					}
					return false;
				});

			$('<a/>', {'class': 'fbx-close-caption', html: '&times;'}).on('click.captions',_this.handlers.toggleCaptions).prependTo($caption);
		};

		/**
		 * Attempts to 'prettify' the caption, removes dashes and trailing numbers and converts to sentence case.
		 * @param {string} text - The text to 'prettify'.
		 * @returns {string} - The 'prettified' version of the string.
		 */
		this.prettifier = function (text) {
			if (typeof text !== 'string') return null;
			text = text.replace(/\s*-\d+/g, '').replace(/\s*_\d+/g, '').replace(/-/g, ' ').replace(/_/g, ' ');
			text = text.replace(/\w\S*/g, function (txt) {
				if (txt.indexOf('#') != -1) {
					return txt; // fix to leave an id selector href as it was (id's are case sensitive so changing anything breaks the selector)
				}
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			});
			return text;
		};
	};

	FooBox.addons.register(FooBox.Captions, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Deeplinking - An addon providing deeplinking URL support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the deeplinking addon. */
	var defaults = {
		/** @type {Object} - An object containing deeplinking related options. */
		deeplinking: {
			/** @type {boolean} - A Boolean indicating whether or not the deeplinking feature is enabled. */
			enabled: true,
			/** @type {string} - A String prefix used for the creating of hash tags. */
			prefix: 'foobox'
		}
	};

	/**
	 * Extend the FooBox.Item with additional properties used by this addon.
	 */
	/** @type {string} - The deeplink appended to the url for the item. */
	FooBox.Item.prototype.deeplink = null;

	/** @type {boolean} - A Boolean value indicating whether or not a deeplink has been set. */
	FooBox.Instance.prototype.isDeepLink = false;
	
	FooBox.hashBang = '#';

	/**
	 * The core logic for the FooBox.DeepLinking addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.DeepLinking = function (instance) {
		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {Object} - An object containing any timers required by this addon. */
		this.timers = {
			/** @type {FooBox.Timer} - A timer used to create a hover delay. */
			display: new FooBox.Timer()
		};

		/**
		 * @type {FooBox.DeepLinking} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.parseItem': _this.handlers.parseItem
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.parseItem': _this.handlers.parseItem
					});
				}
				_this.FooBox.element.off({
					'foobox.afterLoad': _this.handlers.afterLoad,
					'foobox.close': _this.handlers.close
				});
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (e.fb.options.deeplinking.enabled === true) {
					e.fb.instance.element.on({
						'foobox.afterLoad': _this.handlers.afterLoad,
						'foobox.close': _this.handlers.close
					});
					var hash = _this.hash.get();
					if (hash && _this.FooBox.id == hash.id) {
						var item = _this.FooBox.items.array[hash.index];
						if (!item) return;
						_this.FooBox.isDeepLink = true;
						_this.FooBox.raise('foobox.hasHash', { 'item': item });
						_this.timers.display.start(function () {
							_this.FooBox.items.indexes.set(item.index);
							_this.FooBox.modal.show(true);
						}, 100);
					}
				}
			},
			/**
			 * Handles the foobox.parseItem event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			parseItem: function(e){
				if (e.fb.options.deeplinking.enabled != true) return;
				switch(e.fb.item.type){
					case 'image':
						if (e.fb.item.url.match(/.*\/(.*)$/)) {
							e.fb.item.deeplink = e.fb.item.url.match(/.*\/(.*)$/)[1];
						}
						break;
					case 'html':
						e.fb.item.deeplink = e.fb.item.selector.replace(FooBox.hashBang, '');
						break;
					case 'video':
						if (e.fb.item.video_url.length > 0){
							e.fb.item.deeplink = e.fb.item.video_url[0].id;
						}
						break;
				}
				e.fb.item.hash = e.fb.options.deeplinking.prefix + '-' + e.fb.instance.id + '/' + e.fb.item.index;
				if (e.fb.item.deeplink) {
					e.fb.item.hash += '/' + e.fb.item.deeplink;
				}
				e.fb.item.deeplink_url = ('' + window.location).split(FooBox.hashBang)[0] + FooBox.hashBang + e.fb.item.hash;
			},
			/**
			 * Handles the foobox.afterLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterLoad: function () {
				_this.hash.set();
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			close: function () {
				_this.hash.clear();
			}
		};

		/** @namespace - Contains all the getter/setter logic for the hash value. */
		this.hash = {
			/**
			 * Retrieves the current hash values from the url if one exists.
			 * @returns {Object}
			 */
			get: function () {
				if (location.hash.indexOf(FooBox.hashBang + _this.FooBox.options.deeplinking.prefix) === -1) { return null; }

				var hash = location.hash;
				if (hash.substr(-1) == '/') { hash = hash.substr(0, hash.length - 1); }

				if (!(/\/([^\/]+)\/?([^\/]+)?$/.test(hash))) return null;
				var regex = hash.match(/\/([^\/]+)\/?([^\/]+)?$/);
				var index = regex[1],
					name = regex[2],
					actual = -1;

				//first, try to get the item index based on the item name
				if (typeof name == 'string') {
					$.each(_this.FooBox.items.array, function (i, item) {
						if (item.deeplink && item.deeplink === name) {
							actual = i;
							return false;
						}
						return true;
					});
				}

				if (actual == -1) { actual = index; }

				var id = hash.substring(0, hash.indexOf('/'));
				id = id.replace(FooBox.hashBang + _this.FooBox.options.deeplinking.prefix + '-', '');

				return { id: id, index: actual };
			},
			/**
			 * Sets a new hash value on the URL using the current item.
			 */
			set: function () {
				var item = _this.FooBox.items.current(),
					url = ('' + window.location).split(FooBox.hashBang)[0] + FooBox.hashBang + item.hash;
				if (window.history && window.history.replaceState){
					window.history.replaceState(null, document.title, url);
				} else {
					window.location.replace(url);
				}
			},
			/**
			 * Clears the hash value from the URL.
			 */
			clear: function () {
				if (window.history && window.history.replaceState){
					var url = window.location.pathname;
					url += (typeof window.location.search === 'string' && window.location.search !== '') ? (window.location.search.substring(0,1) === '?'
						? window.location.search
						: '?'+window.location.search) : '';
					window.history.replaceState(null, document.title, url);
				} else {
					window.location.replace(('' + window.location).split(FooBox.hashBang)[0] + FooBox.hashBang + '/');
				}
			}
		};
	};

	FooBox.addons.register(FooBox.DeepLinking, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Events - An addon providing providing backwards compatibility support for the FooBox V2 plugin by raising V1 events
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/**
	 * The core logic for the FooBox.Events addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Events = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.Events} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind();
			element.on({
				'foobox.afterLoad': _this.handlers.foobox_image_onload,
				'foobox.beforeLoad': _this.handlers.foobox_image_custom_caption
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind();
		};

		this.raise = function (eventName, args) {
			args = args || {};
			var e = $.Event(eventName);
			//pre jQuery 1.6 did not allow data to be passed to the event object constructor so extend instead
			$.extend(true, e, args);
			_this.FooBox.element.trigger(e);
			return e;
		};

		this.handlers = {
			unbind: function(){
				_this.FooBox.element.off({
					'foobox.afterLoad': _this.handlers.foobox_image_onload,
					'foobox.beforeLoad': _this.handlers.foobox_image_custom_caption
				});
			},
			foobox_image_onload: function (e) {
				if (e.fb.item.type == 'image'){
					_this.raise('foobox_image_onload', { thumb: { jq: e.fb.item.element, target: e.fb.item.url } });
				}
			},
			foobox_image_custom_caption: function (e) {
				var ne = _this.raise('foobox_image_custom_caption', { thumb: e.fb.item.element, title: e.fb.item.title, desc: e.fb.item.description, handled: false });
				if (ne.handled == true) {
					e.fb.item.title = ne.title;
					e.fb.item.description = ne.desc;
					var caption = typeof e.fb.item.title === 'string' && e.fb.item.title.length > 0 ? '<div class="fbx-caption-title">' + e.fb.item.title + '</div>' : '';
					caption = typeof e.fb.item.description === 'string' && e.fb.item.description.length > 0 ? caption + '<div class="fbx-caption-desc">' + e.fb.item.description + '</div>' : caption;
					e.fb.item.caption = caption;
				}
			}
		};
	};

	FooBox.addons.register(FooBox.Events);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Fullscreen - An addon providing fullscreen support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the fullscreen addon. */
	var defaults = {
		/** @type {Object} - An object containing fullscreen related options. */
		fullscreen: {
			/** @type {boolean} - A Boolean indicating whether or not the fullscreen feature is enabled. */
			enabled: false,
			/** @type {boolean} -  */
			force: false,
			/** @type {boolean} - A Boolean indicating whether or not to attempt to use the browser specific API to control fullscreen functionality. If set to false FooBox will simply fill the available browser window. */
			useAPI: true
		}
	};

	/**
	 * Extend the FooBox.Item with additional properties used by this addon.
	 */
	/** @type {boolean} - Whether or not this item is allowed in fullscreen mode. */
	FooBox.Item.prototype.fullscreen = false;

	/**
	 * The core logic for the FooBox.Fullscreen addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Fullscreen = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.Fullscreen} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupHtml': _this.handlers.setupHtml,
				'foobox.setupOptions': _this.handlers.setupOptions
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupHtml': _this.handlers.setupHtml,
						'foobox.setupOptions': _this.handlers.setupOptions
					});
				}
				_this.FooBox.element.off({
					'foobox.afterShow': _this.handlers.afterShow,
					'foobox.beforeLoad': _this.handlers.beforeLoad,
					'foobox.keydown': _this.handlers.onKeydown,
					'foobox.close': _this.handlers.onClose
				});
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.find('.fbx-fullscreen-toggle').off('click.fullscreen', _this.handlers.onClick);
				}
				$(document).off('webkitfullscreenchange mozfullscreenchange fullscreenchange', _this.handlers.onFullscreenChange)
					.on('webkitfullscreenchange mozfullscreenchange fullscreenchange', _this.handlers.onFullscreenChange);
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();

				if (e.fb.options.fullscreen.enabled === true) {
					e.fb.instance.element.on({
						'foobox.beforeLoad': _this.handlers.beforeLoad,
						'foobox.afterShow': _this.handlers.afterShow,
						'foobox.afterResize': _this.handlers.afterResize,
						'foobox.keydown': _this.handlers.onKeydown,
						'foobox.close': _this.handlers.onClose
					});
					e.fb.modal.find('.fbx-fullscreen-toggle').on('click.fullscreen', _this.handlers.onClick);
				}
			},
			/**
			 * Handles the foobox.setupHtml event allowing this addon to create any required HTML.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupHtml: function (e) {
				e.fb.modal.find('.fbx-inner').append($('<a class="fbx-fullscreen-toggle fbx-maximize fbx-btn-transition fbx-btn-shadow"></a>').append(FooBox.icons.get('fullscreen')));
			},
			/**
			 * Handles the foobox.setupOptions event allowing this addon to set its initial starting state.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupOptions: function (e) {
				e.fb.options.fullscreen.enabled = typeof e.fb.instance.element.data('fullscreenEnabled') == 'boolean'
					? e.fb.instance.element.data('fullscreenEnabled') : e.fb.options.fullscreen.enabled;

				e.fb.options.fullscreen.force = typeof e.fb.instance.element.data('fullscreenForce') == 'boolean'
					? e.fb.instance.element.data('fullscreenForce') : e.fb.options.fullscreen.force;

				e.fb.options.fullscreen.useAPI = typeof e.fb.instance.element.data('fullscreenAPI') == 'boolean'
					? e.fb.instance.element.data('fullscreenAPI') : e.fb.options.fullscreen.useAPI;

				if (e.fb.options.fullscreen.enabled == true) { e.fb.modal.addClass('fbx-fullscreen'); }
				else { e.fb.modal.removeClass('fbx-fullscreen'); }
				if (e.fb.options.fullscreen.force == true) {
					e.fb.modal.find('.fbx-fullscreen-toggle').hide();
				}
			},
			/**
			 * Handles the foobox.afterShow event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterShow: function (e) {
				if (e.fb.options.fullscreen.force === true && !e.fb.modal.hasClass('fbx-phone')) {
					_this.FooBox.modal.element.addClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').hide();
					if (!e.fb.modal.hasClass('fbx-fullscreen-mode')){
						_this.fullscreen.launch();
					}
				}
			},
			/**
			 * Handles the foobox.afterResize event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterResize: function (e) {
				if (e.fb.modal.hasClass('fbx-phone')) {
					e.fb.modal.removeClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').hide();
					if (e.fb.modal.hasClass('fbx-fullscreen-mode')){
						_this.fullscreen.cancel();
					}
				} else if (e.fb.options.fullscreen.force === true) {
					_this.FooBox.modal.element.addClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').hide();
					if (!e.fb.modal.hasClass('fbx-fullscreen-mode')){
						_this.fullscreen.launch();
					}
				} else {
					e.fb.modal.addClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').show();
				}
			},
			/**
			 * Handles the foobox.beforeLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeLoad: function (e) {
				if (e.fb.modal.hasClass('fbx-fullscreen-mode') && e.fb.item.fullscreen != true) {
					e.preventDefault();
					switch (e.fb.instance.items.indexes.direction) {
						case '<':
							e.fb.instance.modal.prev();
							break;
						default:
							e.fb.instance.modal.next();
							break;
					}
				} else if (e.fb.item.fullscreen != true || e.fb.modal.hasClass('fbx-phone')) {
					e.fb.modal.removeClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').hide();
				} else if (e.fb.options.fullscreen.force === true) {
					e.fb.modal.find('.fbx-fullscreen-toggle').hide();
				} else {
					e.fb.modal.addClass('fbx-fullscreen').find('.fbx-fullscreen-toggle').show();
				}
			},
			/**
			 * Handles the standard jQuery click event toggle the between fullscreen and normal modes.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			onClick: function (e) {
				e.preventDefault();
				if (_this.FooBox.modal.element.hasClass('fbx-fullscreen-mode')) { _this.fullscreen.cancel(); }
				else { _this.fullscreen.launch(); }
				return false;
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onClose: function () {
				_this.fullscreen.cancel();
			},
			/**
			 * Handles the foobox.keydown event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onKeydown: function (e) {
				if (e.fb.modal.hasClass('fbx-fullscreen-mode') && e.fb.keyCode == 27) {
					_this.fullscreen.cancel();
					if (e.fb.options.fullscreen.force == true){
						e.fb.instance.modal.close();
					}
				}
			},
			/**
			 * Handles the fullscreenchange event to toggle between fullscreen and normal modes.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			onFullscreenChange: function () {
				if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen) {
					_this.fullscreen.apply();
				} else {
					_this.fullscreen.remove();
				}
			}
		};

		/** @namespace - Contains the launch/cancel logic for the fullscreen feature. */
		this.fullscreen = {
			/**
			 * Launches the fullscreen mode for FooBox using the built in browser API's if required.
			 */
			launch: function () {
				if (_this.FooBox.options.fullscreen.useAPI == true && _this.FooBox.options.fullscreen.force == false && !(FooBox.browser.isLtSafari10 && FooBox.browser.Mac)) {
					var modal = _this.FooBox.modal.element.get(0);
					if (modal.requestFullScreen) { modal.requestFullScreen(); }
					else if (modal.mozRequestFullScreen) { modal.mozRequestFullScreen(); }
					else if (modal.webkitRequestFullScreen) { modal.webkitRequestFullScreen(); }
					else { _this.fullscreen.apply(); }
				} else {
					_this.fullscreen.apply();
				}
			},
			/**
			 * Cancels the current fullscreen mode for FooBox using the built in browser API's if required.
			 */
			cancel: function () {
				if (!_this.FooBox.modal.element.hasClass('fbx-fullscreen-mode')) { return; }
				if (_this.FooBox.options.fullscreen.useAPI == true && _this.FooBox.options.fullscreen.force == false && !(FooBox.browser.isLtSafari10 && FooBox.browser.Mac)) {
					if (document.cancelFullScreen) { document.cancelFullScreen(); }
					else if (document.mozCancelFullScreen) { document.mozCancelFullScreen(); }
					else if (document.webkitCancelFullScreen) { document.webkitCancelFullScreen(); }
					else { _this.fullscreen.remove(); }
				} else {
					_this.fullscreen.remove();
				}
			},
			/**
			 * Applies the CSS styles and sets the state for fullscreen mode.
			 */
			apply: function () {
				var item = _this.FooBox.items.current();
				if (item){
					_this.FooBox.modal.element.addClass('fbx-fullscreen-mode').find('.fbx-fullscreen-toggle').removeClass('fbx-maximize').addClass('fbx-minimize');
					_this.FooBox.modal.resize(item.handler.getSize(item), _this.FooBox.modal.element.find('.fbx-item-current'));
				}
				_this.FooBox.raise('foobox.fullscreenEnabled');
			},
			/**
			 * Removes the CSS styles and sets the state for normal mode.
			 */
			remove: function () {
				var item = _this.FooBox.items.current();
				if (item){
					_this.FooBox.modal.element.removeClass('fbx-fullscreen-mode').find('.fbx-fullscreen-toggle').removeClass('fbx-minimize').addClass('fbx-maximize');
					_this.FooBox.modal.resize(item.handler.getSize(item), _this.FooBox.modal.element.find('.fbx-item-current'));
				}
				_this.FooBox.raise('foobox.fullscreenDisabled');
			}
		};
	};

	FooBox.addons.register(FooBox.Fullscreen, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox FX - An addon providing extra effects for the FooBox plugin
 * @version 0.0.1
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the addon. */
	var defaults = {
		effect: null
	};

	/**
	 * The core logic for the FooBox.FX addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.FX = function(instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.FX} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupOptions': _this.handlers.setupOptions
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function() {
			_this.handlers.unbind(true);
		};

		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all) {
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupOptions': _this.handlers.setupOptions
					});
				}
				_this.FooBox.element.off({
					'foobox.afterResize': _this.handlers.afterResize,
					'foobox.beforeShow': _this.handlers.beforeShow,
					'foobox.beforeClose': _this.handlers.beforeClose
				});
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (FooBox.browser.supportsTransitions() && typeof _this.FooBox.options.effect == 'string' && _this.FooBox.options.effect != ""){
					e.fb.instance.element.on({
						'foobox.afterResize': _this.handlers.afterResize,
						'foobox.beforeShow': _this.handlers.beforeShow,
						'foobox.beforeClose': _this.handlers.beforeClose
					});
				}
			},
			/**
			 * Handles the foobox.setupOptions event allowing this addon to set its initial starting state.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupOptions: function (e) {
				if ( _this.FooBox.element.data('effect') ) {
					_this.FooBox.options.effect = _this.FooBox.element.data('effect');
				}
				if (FooBox.browser.supportsTransitions() && typeof _this.FooBox.options.effect == 'string' && _this.FooBox.options.effect != ""){
					e.fb.modal.addClass('fbx-effects').addClass(e.fb.options.effect);
				}
			},
			afterResize: function(e){
				// handle moving the buttons around if required.
				var $inner = e.fb.modal.find('.fbx-inner'),
					$stage = e.fb.modal.find('.fbx-stage'),
					isStickyButtons = e.fb.modal.hasClass('fbx-sticky-buttons'),
					isStickyCaptions = e.fb.modal.hasClass('fbx-sticky-caption'),
					isPhone = e.fb.modal.hasClass('fbx-phone'),
					isFullscreen = e.fb.modal.hasClass('fbx-fullscreen-mode'),
					isFlat = e.fb.modal.hasClass('fbx-flat');
				if (isStickyButtons || isPhone || isFullscreen){
					if (isPhone || isFullscreen || isFlat){
						e.fb.modal.find('.fbx-close').insertAfter($inner);
						e.fb.modal.find('.fbx-play,.fbx-pause').insertAfter($inner);
						e.fb.modal.find('.fbx-fullscreen-toggle').insertBefore($inner);
						if (isPhone){
							e.fb.modal.find('.fbx-social-toggle,.fbx-social').insertAfter($inner);
						} else {
							e.fb.modal.find('.fbx-social-toggle,.fbx-social').appendTo($inner);
						}
					} else {
						e.fb.modal.find('.fbx-close').appendTo($inner);
						e.fb.modal.find('.fbx-play,.fbx-pause').appendTo($inner);
						e.fb.modal.find('.fbx-fullscreen-toggle').prependTo($inner);
						e.fb.modal.find('.fbx-social-toggle,.fbx-social').appendTo($inner);
					}
					e.fb.modal.find('.fbx-prev').insertBefore($inner);
					e.fb.modal.find('.fbx-next').insertAfter($inner);
				} else {
					e.fb.modal.find('.fbx-close').appendTo($inner);
					e.fb.modal.find('.fbx-play,.fbx-pause').appendTo($inner);
					e.fb.modal.find('.fbx-fullscreen-toggle').prependTo($inner);
					e.fb.modal.find('.fbx-prev').prependTo($inner);
					e.fb.modal.find('.fbx-next').appendTo($inner);
					e.fb.modal.find('.fbx-social-toggle,.fbx-social').appendTo($inner);
				}
				// handle moving the caption around if required.
				if (isStickyCaptions || isPhone || isFullscreen){
					e.fb.modal.find('.fbx-caption').appendTo(e.fb.modal);
					e.fb.modal.find('.fbx-open-caption').appendTo(e.fb.modal);
				} else {
					e.fb.modal.find('.fbx-caption').appendTo($stage);
					e.fb.modal.find('.fbx-open-caption').appendTo($inner);
				}
			},
			/**
			 * Handles the foobox.beforeShow event allowing this addon to override the default function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeShow: function(e){
				e.preventDefault();
				e.fb.modal.removeClass('fbx-loading').addClass('fbx-show').one(FooBox.browser.transitionEnd(), function(){
					var fbx = e.fb.instance,
							item = e.fb.item,
							size = item.handler.getSize(item),
							$current = fbx.modal.element.find('.fbx-item-current'),
							cHeight = $current.height();

					fbx.modal.triggerFooGalleryLayout($current);
					if (cHeight != size.height){
						fbx.modal.resize(size, $current, function(){
							fbx.modal.triggerFooGalleryLayout($current);
							fbx.raise('foobox.afterShow', { 'item': item });
							fbx.raise('foobox.afterLoad', { 'item': item });
							fbx.modal._busy = false;
						});
					} else {
						fbx.raise('foobox.afterShow', { 'item': item });
						fbx.raise('foobox.afterLoad', { 'item': item });
						fbx.modal._busy = false;
					}
				}).addClass('fbx-animate');
			},
			/**
			 * Handles the foobox.beforeClose event allowing this addon to override the default function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeClose: function(e){
				e.preventDefault();
				function close(){
					e.fb.instance.modal._closed = true;
					e.fb.instance.modal._busy = false;
					$('body').removeClass('fbx-active');
					e.fb.instance.raise('foobox.close');
					e.fb.modal.removeClass('fbx-loading fbx-show').find('.fbx-item-current, .fbx-item-next').empty();
					$('html').removeClass('fbx-no-scroll');
					e.fb.instance.raise('foobox.afterClose');
				}
				if (e.fb.modal.hasClass('fbx-animate')) e.fb.modal.one(FooBox.browser.transitionEnd(), close).removeClass('fbx-animate');
				else close();
			}
		};
	};

	FooBox.addons.register(FooBox.FX, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Keyboard - An addon providing simple keyboard support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	var defaults = {
		keyboard: {
			enabled: true,
			preventDefault: false,
			stopPropagation: false
		}
	};

	/**
	 * The core logic for the FooBox.Keyboard addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Keyboard = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.Keyboard} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupHtml': _this.handlers.setupHtml
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupHtml': _this.handlers.setupHtml
					});
				}
				_this.FooBox.element.off('foobox.afterLoad', _this.handlers.afterLoad);
				if (FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.off('keydown.foobox', _this.handlers.onKeydown);
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function () {
				_this.handlers.unbind();
				if (_this.FooBox.options.keyboard.enabled == true){
					_this.FooBox.element.on('foobox.afterLoad', _this.handlers.afterLoad);
					if (FooBox.isjQuery(_this.FooBox.modal.element)){
						_this.FooBox.modal.element.on('keydown.foobox', _this.handlers.onKeydown);
					}
				}
			},
			/**
			 * Handles the foobox.setupHtml event allowing this addon to create any required HTML.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupHtml: function (e) {
				e.fb.modal.attr('tabindex', 0);
			},
			/**
			 * Handles the foobox.afterLoad event allowing this addon to focus the modal to enable capturing of key events.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterLoad: function (e) {
				e.fb.modal.trigger('focus');
			},
			/**
			 * Handles the standard jQuery keydown event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			onKeydown: function (e) {
				var modal = _this.FooBox.modal, options = _this.FooBox.options.keyboard;
				if (modal.element.hasClass('fbx-show')) {
					if (!modal.element.hasClass('fbx-fullscreen-mode') && e.which === 27) {
						modal.close();
					} else if (_this.FooBox.items.multiple() && e.which === 37) {
						modal.prev();
					} else if (_this.FooBox.items.multiple() && e.which === 39) {
						modal.next();
					}
					if (options.preventDefault == true){ e.preventDefault(); }
					if (options.stopPropagation == true){ e.stopPropagation(); }
					_this.FooBox.raise('foobox.keydown', { 'keyCode': e.which });
				}
			}
		};
	};

	FooBox.addons.register(FooBox.Keyboard, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Responsive (REQUIRED) - An addon providing responsive support for the FooBox plugin.
 * This addon is required for FooBox to operate correctly and should always be loaded.
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the responsive addon. */
	var defaults = {
		/** @type {boolean} - A Boolean indicating whether or not to completely hide the previous/next buttons when on mobile devices. */
		hideNavOnMobile: false,
		/** @type {number} - The Number of milliseconds to wait before triggering the FooBox resize event. The timer is reset if another resize is triggered ensuring only a single event is raised. */
		resizeTimeout: 300,
		/** @type {Object} - An object containing breakpoint specific values. */
		breakpoints: {
			/** @type {number} - The Number used to determine the width for phones. Anything with a width smaller than or equal to this number will be treated as being a phone. */
			phone: 812,
			/** @type {number} - The Number used to determine the width for tablets. Anything with a width smaller than or equal to this number will be treated as being a tablet until the phone breakpoint is reached. */
			tablet: 1366
		},
		/** @type {number} - The time in milliseconds between ios7 & Safari's periodic checks for the address and bookmarks bars. Smaller values give better responsiveness but may reduce performance. */
		iosInterval: 200
	};

	/**
	 * Supplied the breakpoints array this object returns all the information required for responsiveness.
	 * @param {Array} breakpoints - An array of breakpoint key value pairs.
	 * @constructor
	 */
	FooBox.BPInfo = function (breakpoints) {
		var $window = $(window);
		this.width = $window.width();
		this.height = $window.height();
		this.orientation = this.width > this.height ? 'fbx-landscape' : 'fbx-portrait';
		var current = null, breakpoint;
		if ($.isArray(breakpoints)) {
			for (var i = 0; i < breakpoints.length; i++) {
				breakpoint = breakpoints[i];
				if (breakpoint && breakpoint.width && this.width <= breakpoint.width) {
					current = breakpoint;
					break;
				}
			}
		}
		this.breakpoint = current == null ? 'fbx-desktop' : current.name;
	};

	/**
	 * The core logic for the FooBox.Responsive addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Responsive = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {Object} - An object containing parsed option values for this addon. */
		this.breakpoint = {
			/** @type {Array} - An array containing key value pairs using the properties "name" and "width". */
			values: [],
			/** @type {string} - A string containing space separated breakpoint class names. */
			names: ''
		};
		/** @type {Object} - An object containing any timers required by this addon. */
		this.timers = {
			/** @type {FooBox.Timer} - A timer used to create a resize delay. */
			resize: new FooBox.Timer(),
			/** @type {FooBox.Timer} - A timer used to create a period check on ios7's bookmark and address bars visibility. */
			ios: new FooBox.Timer()
		};

		/**
		 * @type {FooBox.Responsive} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * @type {?boolean} - Whether or not the ios7 bookmark and address bar is visible. Null is indeterminate.
		 * This variable prevents excessive firing of resizes so only a single event is fired per change.
		 * @private
		 */
		var _iosBars = null;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on('foobox.initialized foobox.reinitialized', _this.handlers.initialized);
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off('foobox.initialized foobox.reinitialized', _this.handlers.initialized);
				}
				$(window).off('resize.foobox', _this.handlers.resize);
				_this.FooBox.element.off({
					'foobox.beforeShow': _this.handlers.iosBeforeShow,
					'foobox.close': _this.handlers.iosClose
				});
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function () {
				_this.handlers.unbind();
				_this.setup.breakpoints();
				_this.style();
				$(window).on('resize.foobox', _this.handlers.resize);
				// This is a hack to get around ios7 & Safari's terrible handling of the address and bookmarks bar.
				if (FooBox.browser.iOS12OrBelow && (FooBox.browser.isSafari || FooBox.browser.isChrome)){
					_this.FooBox.element.on({
						'foobox.beforeShow': _this.handlers.iosBeforeShow,
						'foobox.close': _this.handlers.iosClose
					});
				}
				//var $modal = _this.FooBox.modal.element;
				//if ($modal.hasClass('fbx-ios') && ($modal.hasClass('fbx-safari') || $modal.hasClass('fbx-chrome'))){
				//}
			},
			/**
			 * Handles the standard jQuery resize event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			resize: function () {
				_this.timers.resize.start(function () {
					_this.style();
					var fbx = _this.FooBox, item = _this.FooBox.items.current();
					if (!fbx.modal.element.has('fbx-show') || item == null) { return; }
					var size = item.handler.getSize(item), $current = fbx.modal.element.find('.fbx-item-current');
					fbx.modal.resize(size, $current, function(){
						fbx.modal.triggerFooGalleryLayout($current);
						var size = item.handler.getSize(item),
								cHeight = $current.height();
						if (cHeight != size.height){
							fbx.modal.resize(size, $current, function(){
								fbx.modal.triggerFooGalleryLayout($current);
							});
						}
					});
				}, _this.FooBox.options.resizeTimeout);
			},
			/**
			 * For ios7 & Safari kick off a periodic check so see if the address and bookmarks bar are visible while the modal is open.
			 */
			iosBeforeShow: function(){
				_this.timers.ios.start(_this.handlers.ios, _this.FooBox.options.iosInterval);
			},
			/**
			 * For ios7 & Safari stop the periodic checks for the address and bookmarks bar when the modal is closed.
			 */
			iosClose: function(){
				_this.timers.ios.stop();
			},
			/**
			 * Handles the period check of the window orientation and size to determine if the IOS7 & Safari address and bookmark bars are visible.
			 * If the bars are visible this tricks the browser into correctly redrawing itself and triggers the resize event for FooBox.
			 */
			ios: function(){
				if (!_this.FooBox.modal.element.hasClass('fbx-show')){
					_this.timers.ios.start(_this.handlers.ios, _this.FooBox.options.iosInterval);
					return;
				}

				// as this is only executed on iOS12 or below we can simply use the now deprecated
				// window.orientation property instead of the full ScreenOrientation API
				var o = window.orientation || 0,
					h = window.innerHeight,
					landscape = o === 90 || o === -90,
					open = 0, closed = 0,
					sh = landscape ? screen.availWidth : screen.availHeight;

				if (FooBox.browser.isChrome){
					open = sh - (landscape ? 76 : 56);
					closed = sh - (landscape ? 0 : 20);
				} else if (FooBox.browser.isSafari){
					open = sh - 88;
					closed = sh - (landscape ? 0 : 19);
				}

				if ((_iosBars == null || _iosBars === false) && h <= open){
					_iosBars = true;
					_this.iosRedraw();
				} else if ((_iosBars == null || _iosBars === true) && h >= closed) {
					_iosBars = false;
					_this.iosRedraw();
				}
				_this.timers.ios.start(_this.handlers.ios, _this.FooBox.options.iosInterval);
			}
		};

		/** @namespace - Contains the logic used for initialization and reinitialization. */
		this.setup = {
			/**
			 * Parses the 'breakpoints' option to break it down into more friendly objects to work with.
			 */
			breakpoints: function () {
				_this.breakpoint.values = [];
				_this.breakpoint.names = '';
				// Create an array and a space separated string of names to work with.
				for (var name in _this.FooBox.options.breakpoints) {
					if (_this.FooBox.options.breakpoints.hasOwnProperty(name)) {
						_this.breakpoint.values.push({ 'name': _this.fixName(name), 'width': _this.FooBox.options.breakpoints[name] });
						_this.breakpoint.names += _this.fixName(name) + ' ';
					}
				}
				// Sort it so the smallest breakpoint is checked first
				_this.breakpoint.values.sort(function (a, b) {
					return a.width - b.width;
				});
			}
		};

		/**
		 * This is the core fix of the ios7 redraw issue when the address and bookmark bars appear.
		 * This first line triggers a layout by simply setting the scroll position to itself.
		 * The second line triggers a resize event like pre ios7.
		 */
		this.iosRedraw = function(){
			window.scrollTo(0, document.body.scrollTop);
			$(window).trigger('resize');
		};

		/**
		 * Takes a breakpoint name and generates a CSS class name out of it.
		 * @param {string} name - The breakpoint name to be converted.
		 * @returns {string}
		 */
		this.fixName = function (name) {
			if (!(/^fbx-[a-zA-Z0-9]/).test(name)) { return 'fbx-' + name; }
			return name;
		};

		/**
		 * Takes into account the options and display size and adds or removes CCS classes as required.
		 */
		this.style = function () {
			var info = new FooBox.BPInfo(_this.breakpoint.values),
				modal = _this.FooBox.modal.element;

			modal.removeClass(_this.breakpoint.names)
				.removeClass('fbx-desktop fbx-landscape fbx-portrait')
				.addClass(info.breakpoint)
				.addClass(info.orientation);

			if (_this.FooBox.options.hideNavOnMobile === true) {
				modal.addClass('fbx-no-nav');
			} else {
				modal.removeClass('fbx-no-nav');
			}
		};
	};

	FooBox.Responsive.metaCheck = function(){
		/* The below is performed only once on script load and is designed to append the correct META tags required for mobile viewing. */
		var $meta = $('meta[name=viewport]'), content = $meta.attr('content');
		if ($meta.length > 0 && typeof content == 'string'){
			var parts = content.split(','), width = false, initial_scale = false;
			for (var i = 0; i < parts.length; i++){
				var part = FooBox.trim(parts[i]);
				if (part.substring(0, 5) == 'width' && part.indexOf("device-width") !== -1){
					width = true;
				} else if (part.substring(0, 13) == 'initial-scale'){
					initial_scale = true;
				}
			}
			if (width && !initial_scale){
				parts.push('initial-scale=1');
				content = parts.join(',');
				$meta.attr('content', content);
			}
		}
	};

	FooBox.addons.register(FooBox.Responsive, defaults);

	FooBox.Responsive.metaCheck();

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Slideshow - An addon providing slideshow support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the slideshow addon. */
	var defaults = {
		/** @type {Object} - An object containing slideshow related options. */
		slideshow: {
			/** @type {boolean} - A Boolean indicating whether or not to automatically start the slideshow. This will only trigger if there is more than one item in the collection. */
			autostart: false,
			/** @type {boolean} - A Boolean indicating whether or not to automatically stop the slideshow when the last image is displayed. */
			autostop: false,
			/** @type {boolean} - A Boolean indicating whether or not the slideshow feature is enabled. */
			enabled: true,
			/** @type {boolean} - A Boolean indicating whether or not the slideshow only displays image items or all items. */
			imagesOnly: false,
			/** @type {number} - A Number determining the amount of time it takes to hide the play/pause button once the mouse stops moving in fullscreen. */
			mousestopTimeout: 300,
			/** @type {number} - A Number determining the amount of time between switching items in the slideshow. */
			timeout: 6000,
			/** @type {number} - A Number determining the minimum pixels the mouse must move before the play/pause button is shown when in fullscreen mode. */
			sensitivity: 130,
			/** @type {boolean} - A Boolean indicating whether or not to skip over item loading errors and proceed to the next item. */
			skipErrors: false
		}
	};

	/**
	 * The core logic for the FooBox.Slideshow addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Slideshow = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {boolean} - A Boolean indicating whether or not the slideshow is running. */
		this.autostart = false;
		/** @type {boolean} - A Boolean indicating whether or not the slideshow is running. */
		this.running = false;
		/** @type {number} - A Number indicating the percentage the slideshow progress was paused at if it was paused. */
		this.paused = 0;
		/** @type {number} - A Number indicating the amount of milliseconds required to display the remaining percentage of the paused value. */
		this.remaining = 0;
		/** @type {Object} - An object containing any timers required by this addon. */
		this.timers = {
			/** @type {FooBox.Timer} - A timer used to detect when the mouse stops moving. */
			mousestop: new FooBox.Timer()
		};

		/**
		 * @type {FooBox.Slideshow} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this, start = null;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element, options) {
			_this.handlers.unbind(true);
			_this.autostart = options.slideshow.autostart;
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupHtml': _this.handlers.setupHtml,
				'foobox.setupOptions': _this.handlers.setupOptions,
				'foobox.onError': _this.handlers.onError
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupHtml': _this.handlers.setupHtml,
						'foobox.setupOptions': _this.handlers.setupOptions,
						'foobox.onError': _this.handlers.onError
					});
				}
				_this.FooBox.element.off({
					'foobox.beforeLoad': _this.handlers.beforeLoad,
					'foobox.afterLoad': _this.handlers.afterLoad,
					'foobox.close': _this.handlers.onClose,
					'foobox.previous foobox.next': _this.handlers.onChange,
					'foobox.keydown': _this.handlers.onKeydown,
					'foobox.fullscreenEnabled': _this.handlers.fullscreenEnabled,
					'foobox.fullscreenDisabled': _this.handlers.fullscreenDisabled
				});
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.off('click.slideshow').off('mousemove.foobox')
						.find('.fbx-play, .fbx-pause').off('mouseenter.foobox mouseleave.foobox');
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				// Clear any pre-existing bindings before we do anything
				_this.handlers.unbind();
				// Only bind if the addon is enabled.
				if (e.fb.options.slideshow.enabled === true && e.fb.instance.items.multiple()) {
					e.fb.instance.element.on({
						'foobox.beforeLoad': _this.handlers.beforeLoad,
						'foobox.afterLoad': _this.handlers.afterLoad,
						'foobox.close': _this.handlers.onClose,
						'foobox.previous foobox.next': _this.handlers.onChange,
						'foobox.keydown': _this.handlers.onKeydown,
						'foobox.fullscreenEnabled': _this.handlers.fullscreenEnabled,
						'foobox.fullscreenDisabled': _this.handlers.fullscreenDisabled
					});

					e.fb.modal
						.on('click.slideshow', '.fbx-play', _this.handlers.playClicked)
						.on('click.slideshow', '.fbx-pause', _this.handlers.pauseClicked);

					if (e.fb.modal.hasClass('fbx-playpause-center')){
						e.fb.modal
							.on('mousemove.foobox', _this.handlers.mousemove)
							.find('.fbx-play, .fbx-pause')
							.on('mouseenter.foobox', _this.handlers.mouseenter)
							.on('mouseleave.foobox', _this.handlers.mouseleave);
					}
				}
			},
			/**
			 * Handles the foobox.setupHtml event allowing this addon to create any required HTML.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupHtml: function (e) {
				if (e.fb.options.slideshow.enabled !== true) { return; }
				e.fb.modal.find('.fbx-inner')
					.append('<div class="fbx-progress"></div>')
					.append($('<a class="fbx-play fbx-btn-transition fbx-btn-shadow"></a>').append(FooBox.icons.get('slideshow')));
			},
			/**
			 * Handles the foobox.setupOptions event allowing this addon to set its initial starting state.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupOptions: function (e) {
				if (e.fb.options.slideshow.enabled !== true) { return; }
				e.fb.modal.addClass('fbx-slideshow');
			},
			/**
			 * Handles the foobox.beforeLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeLoad: function (e) {
				if (e.fb.modal.hasClass('fbx-fullscreen-mode') && e.fb.modal.hasClass('fbx-playpause-center')) {
					start = null;
					_this.timers.mousestop.stop();
				}
			},
			/**
			 * Handles the foobox.afterLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterLoad: function (e) {
				if ((e.fb.options.slideshow.autostop == true && e.fb.instance.items.is.last(e.fb.item)) || (e.fb.item.type === 'video' && e.fb.item.autoPlay)) {
					_this.stop(false);
					return;
				}
				if (_this.autostart != true) { return; }
				_this.start();
			},
			/**
			 * Handles the foobox.onError event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onError: function (e) {
				if (e.fb.options.slideshow.skipErrors === true && e.fb.instance.modal._first === false) {
					e.preventDefault();
					switch (e.fb.instance.items.indexes.direction) {
						case '<':
							e.fb.instance.modal.prev(e.fb.options.slideshow.imagesOnly === true ? 'image' : null);
							break;
						default:
							e.fb.instance.modal.next(e.fb.options.slideshow.imagesOnly === true ? 'image' : null);
							break;
					}
				} else if (_this.autostart == true) {
					_this.start();
				}
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onClose: function () {
				_this.stop(_this.running);
			},
			/**
			 * Handles the foobox.prev and foobox.next events.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onChange: function () {
				_this.stop((_this.autostart == true || _this.running == true));
			},
			/**
			 * Handles the foobox.keydown event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onKeydown: function (e) {
				if (e.fb.keyCode == 32){
					if (_this.running == true) _this.pause();
					else _this.start();
				}
			},
			/**
			 * Handles the foobox.fullscreenEnabled event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			fullscreenEnabled: function (e) {
				if (e.fb.modal.hasClass('fbx-fullscreen-mode') && e.fb.modal.hasClass('fbx-playpause-center')) {
					_this.timers.mousestop.start(function () {
						start = null;
						e.fb.modal.find('.fbx-play, .fbx-pause').fadeOut('fast');
					}, _this.FooBox.options.slideshow.mousestopTimeout);
				}
			},
			/**
			 * Handles the foobox.fullscreenDisabled event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			fullscreenDisabled: function (e) {
				if (!e.fb.modal.hasClass('fbx-fullscreen-mode') && e.fb.modal.hasClass('fbx-playpause-center')) {
					e.fb.modal.find('.fbx-play, .fbx-pause').removeClass('fbx-active');
					start = null;
					_this.timers.mousestop.stop();
				}
			},
			/**
			 * Handles the standard jQuery click event on the play button.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			playClicked: function (e) {
				e.preventDefault();
				e.stopPropagation();
				_this.start();
			},
			/**
			 * Handles the standard jQuery mouseleave event on the pause button.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			pauseClicked: function (e) {
				e.preventDefault();
				e.stopPropagation();
				_this.pause();
			},
			/**
			 * Handles the standard jQuery mousemove event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mousemove: function (e) {
				var s = _this.FooBox.options.slideshow.sensitivity,
					m = _this.FooBox.modal.element;

				if (m.hasClass('fbx-fullscreen-mode') && !m.hasClass('fbx-loading') && !m.hasClass('fbx-error') && !m.hasClass('fbx-playpause-active') && m.hasClass('fbx-playpause-center')) {
					if (start == null) {
						start = {};
						start.X = e.pageX;
						start.Y = e.pageY;
					} else if ((start.X - e.pageX) >= s || (start.Y - e.pageY) >= s || (start.X - e.pageX) <= -s || (start.Y - e.pageY) <= -s) {
						var playpause = m.find('.fbx-play, .fbx-pause');
						if (!playpause.is(':visible')) {
							playpause.fadeIn('fast');
						}
						_this.timers.mousestop.start(function () {
							start = null;
							playpause.fadeOut('fast');
						}, _this.FooBox.options.slideshow.mousestopTimeout);
					}
				}
			},
			/**
			 * Handles the standard jQuery mouseenter event on the play/pause button.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseenter: function () {
				var m = _this.FooBox.modal.element;
				if (m.hasClass('fbx-fullscreen-mode') && !m.hasClass('fbx-error') && m.hasClass('fbx-playpause-center')) {
					m.addClass('fbx-playpause-active');
					start = null;
					_this.timers.mousestop.stop();
				}
			},
			/**
			 * Handles the standard jQuery mouseleave event on the play/pause button.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseleave: function () {
				var m = _this.FooBox.modal.element;
				if (m.hasClass('fbx-fullscreen-mode') && !m.hasClass('fbx-error') && m.hasClass('fbx-playpause-center')) {
					m.removeClass('fbx-playpause-active');
					_this.timers.mousestop.start(function () {
						start = null;
						m.find('.fbx-play, .fbx-pause').fadeOut('fast');
					}, _this.FooBox.options.slideshow.mousestopTimeout);
				}
			}
		};

		/**
		 * Starts the slideshow taking into account the various options as well as the current state.
		 */
		this.start = function () {
			_this.remaining = (_this.remaining < 1) ? _this.FooBox.options.slideshow.timeout : _this.remaining;
			_this.autostart = false;
			_this.running = true;
			_this.FooBox.modal.element.find('.fbx-progress')
				.css('width', _this.paused + '%')
				.show()
				.animate({ 'width': '100%' }, _this.remaining, 'linear', function () {
					_this.paused = 0;
					_this.remaining = _this.FooBox.options.slideshow.timeout;
					_this.autostart = true;
					_this.FooBox.modal.next(_this.FooBox.options.slideshow.imagesOnly === true ? 'image' : null);
				});
			_this.FooBox.modal.element.find('.fbx-play').toggleClass('fbx-play fbx-pause');
			_this.FooBox.raise('foobox.slideshowStart');
		};

		/**
		 * Stops the slideshow taking into account the various options as well as the current state.
		 * @param {boolean} autostart - If set to true the slideshow will start again after the next item is loaded.
		 */
		this.stop = function (autostart) {
			_this.paused = 0;
			_this.FooBox.modal.element.find('.fbx-progress').stop().hide().css('width', '0%');
			_this.running = false;
			_this.autostart = autostart;
			_this.remaining = _this.FooBox.options.slideshow.timeout;
			if (!autostart) {
				_this.FooBox.modal.element.find('.fbx-pause').toggleClass('fbx-play fbx-pause');
			}
			_this.FooBox.raise('foobox.slideshowStop');
		};

		/**
		 * Pauses the slideshow taking into account the various options as well as the current state.
		 */
		this.pause = function () {
			var p = _this.FooBox.modal.element.find('.fbx-progress');
			var pw = p.stop().css('width'), pos = p.css('position');
			var cw = (pos == 'fixed')
				? _this.FooBox.modal.element.css('width')
				: _this.FooBox.modal.element.find('.fbx-inner').css('width');

			_this.running = false;
			_this.paused = (parseInt(pw, 0) / parseInt(cw, 0)) * 100;
			_this.remaining = _this.FooBox.options.slideshow.timeout - (_this.FooBox.options.slideshow.timeout * (_this.paused / 100));
			_this.FooBox.modal.element.find('.fbx-pause').toggleClass('fbx-play fbx-pause');
			_this.FooBox.raise('foobox.slideshowPause');
		};
	};

	FooBox.addons.register(FooBox.Slideshow, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Social - An addon providing social sharing support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the caption addon. */
	var defaults = {
		/** @type {Object} - An object containing social related options. */
		social: {
			/** @type {boolean} - A Boolean indicating whether or not the captions feature is enabled. */
			enabled: false,
			/** @type {string} - A String used to determine where to place the social links bar. Available options are 'fbx-top', 'fbx-topleft', 'fbx-topright', 'fbx-bottom', 'fbx-bottomleft' and 'fbx-bottomright'. */
			position: 'fbx-top',
			/** @type {boolean} - A Boolean indicating whether or not to only show the captions when hovering over the FooBox. */
			onlyShowOnHover: false,
			/** @type {boolean} - A Boolean indicating whether or not to only show the captions when clicking the FooBox. */
			onlyShowOnClick: false,			
			/** @type {number} - A Number determining the amount of milliseconds to wait before showing the caption when the onlyShowOnHover option is set to true. If the mouse cursor exits the FooBox before this value the caption will not be shown at all. */
			hoverDelay: 300,
			/** @type {Object} - An Array of global excludes used to override the individual excludes set in each link. */
			excludes: ['iframe', 'html'],
			nonce: '',
			networks: [],
			popup: true,
			popupSize: {
				"facebook": {width: 600, height: 600},
				"google-plus": {width: 415, height: 505},
				"twitter": {width: 600, height: 350},
				"pinterest": {width: 770, height: 660},
				"linkedin": {width: 600, height: 500},
				"buffer": {width: 780, height: 580},
				"reddit": {width: 600, height: 600},
				"digg": {width: 600, height: 600},
				"tumblr": {width: 545, height: 370},
				"stumble-upon": {width: 600, height: 600}
			},
			download: false,
			mailto: false
		}
	};

	/** @type {boolean} - Whether or not this item allows social icons to be displayed. */
	FooBox.Item.prototype.social = false;

	FooBox.URL = function(url){
		if (!(this instanceof FooBox.URL)) return new FooBox.URL(url);
		if (!url || (typeof url === "string" && url.length === 0)) url = window.location.href;
		this.original = url;
		var parts = url.split('#');
		this.__hash = parts.length === 2 ? '#' + parts[1] : '';
		parts = parts[0].split('?');
		this.__base = parts[0];
		this.__queryString = parts.length === 2 ? parts[1] : '';
		this.__params = this.__parseParams();
	};

	FooBox.URL.prototype.__parseParams = function(){
		var result = [], params = this.__queryString.split(/[&;]/g);
		for (var i = 0, len = params.length, pair; i < len; i++){
			pair = params[i].split('=');
			if (pair.length !== 2) continue;
			result.push({
				key: decodeURIComponent(pair[0]),
				value: decodeURIComponent(pair[1])
			});
		}
		return result;
	};

	FooBox.URL.prototype.param = function(key, value){
		var GET = typeof value === 'undefined', DELETE = typeof value === 'string' && (value === '' || value === null);
		for (var i = this.__params.length; i-- > 0;) {
			if (this.__params[i].key === key) {
				if (GET) return this.__params[i].value;
				if (DELETE) this.__params.splice(i, 1);
				else this.__params[i].value = value;
				return this;
			}
		}
		if (!GET && !DELETE) this.__params.push({key: key, value: value});
		return this;
	};

	FooBox.URL.prototype.params = function(values){
		if (typeof values === "undefined") return this.__params;
		var keys = Object.keys(values);
		if (keys.length === 0){
			this.__params = [];
		} else {
			var self = this;
			keys.forEach(function(key){
				self.param(key, values[key]);
			});
		}
		return this;
	};

	FooBox.URL.prototype.hash = function(value){
		if (typeof value === "undefined") return this.__hash;
		this.__hash = typeof value !== "string" ? '' : value;
		return this;
	};

	FooBox.URL.prototype.toString = function(){
		var params = this.__params.length > 0 ? '?' : '';
		for (var i = 0, len = this.__params.length; i < len; i++){
			if (i !== 0) params += '&';
			params += encodeURIComponent(this.__params[i].key) + '=' + encodeURIComponent(this.__params[i].value);
		}
		return this.__base + params + this.__hash;
	};

	/**
	 * The core logic for the FooBox.Social addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Social = function (instance) {
		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {Object} - An object containing any timers required by this addon. */
		this.timers = {
			/** @type {FooBox.Timer} - A timer used to create a hover delay. */
			hover: new FooBox.Timer()
		};
		this.post_id = false;

		/**
		 * @type {FooBox.Social} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		var _forceClose = false;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
				'foobox.setupHtml': _this.handlers.setupHtml,
				'foobox.setupOptions': _this.handlers.setupOptions,
				'foobox.onError': _this.handlers.onError
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off({
						'foobox.initialized foobox.reinitialized': _this.handlers.initialized,
						'foobox.setupHtml': _this.handlers.setupHtml,
						'foobox.setupOptions': _this.handlers.setupOptions,
						'foobox.onError': _this.handlers.onError,
						'foobox.captionsShow': _this.handlers.captionsShow,
						'foobox.captionsHide': _this.handlers.captionsHide
					});
				}
				_this.FooBox.element.off({
					'foobox.beforeLoad': _this.handlers.beforeLoad,
					'foobox.afterLoad': _this.handlers.afterLoad,
					'foobox.afterResize': _this.handlers.afterResize
				});
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.off('mouseenter.social mouseleave.social')
						.find('.fbx-item-current, .fbx-item-next')
						.off('click.social');
					_this.FooBox.modal.element.find('.fbx-social').off('click.social');
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (e.fb.options.social.enabled === true) {
					e.fb.instance.element.on({
						'foobox.beforeLoad': _this.handlers.beforeLoad,
						'foobox.afterLoad': _this.handlers.afterLoad,
						'foobox.afterResize': _this.handlers.afterResize,
						'foobox.captionsShow': _this.handlers.captionsShow,
						'foobox.captionsHide': _this.handlers.captionsHide
					});
					_this.post_id = -1;
					e.fb.instance.element.parents().each(function(){
						var $post_id = $(this).find('.fooboxshare_post_id');
						if ($post_id.length === 1){
							_this.post_id = $post_id.val();
							return false;
						}
					});
					e.fb.modal.find('.fbx-social-toggle').off('click.social').on('click.social', function(e){
						e.preventDefault();
						e.stopPropagation();
						_this.handlers.toggleSocial();
					});
					e.fb.modal.find('.fbx-social').off('click.social').on('click.social', 'a', function(e){
						_this.FooBox.raise('foobox.socialClicked', {network: $(this).data("network"), item: _this.FooBox.items.current()});
					});

					if (e.fb.options.social.onlyShowOnHover === true) {
						e.fb.modal.on('mouseenter.social', '.fbx-inner:not(:has(.fbx-item-error))', _this.handlers.mouseenter)
							.on('mouseleave.social', '.fbx-inner:not(:has(.fbx-item-error))', _this.handlers.mouseleave);
					}
				}
			},
			/**
			 * Handles the foobox.afterResize event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterResize: function(e){
				if (e.fb.modal.hasClass('fbx-phone')){
					e.fb.modal.removeClass('fbx-social-showing').addClass('fbx-social-hidden');
					_this.hide();
				} else if (!_forceClose && e.fb.options.social.onlyShowOnHover === false) {
					e.fb.modal.removeClass('fbx-social-hidden').addClass('fbx-social-showing');
					_this.show();
				}
			},
			captionsShow: function(e){
				if (!e.fb.modal.hasClass('fbx-phone') && e.fb.options.social.onlyShowOnHover === false && FooBox.inArray(e.fb.item.type, e.fb.options.social.excludes) === -1) {
					e.fb.modal.removeClass('fbx-social-hidden').addClass('fbx-social-showing');
					_this.show();
					_forceClose = false;
				}
			},
			captionsHide: function(e){
				if (!e.fb.modal.hasClass('fbx-phone') && e.fb.options.social.onlyShowOnHover === false && FooBox.inArray(e.fb.item.type, e.fb.options.social.excludes) === -1) {
					e.fb.modal.removeClass('fbx-social-showing').addClass('fbx-social-hidden');
					_this.hide();
					_forceClose = true;
				}
			},
			/**
			 * Handles the standard jQuery click event on the current item to toggle the captions.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			toggleSocial: function () {
				if (_this.FooBox.modal.element.hasClass('fbx-error') || _forceClose) return;
				if (_this.FooBox.modal.element.find('.fbx-social').is('.fbx-fade-social')) {
					_this.FooBox.modal.element.removeClass('fbx-social-hidden');
					_this.show();
				} else {
					_this.FooBox.modal.element.addClass('fbx-social-hidden');
					_this.hide();
				}
			},
			/**
			 * Handles the standard jQuery mouseenter event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseenter: function () {
				var item = _this.FooBox.items.current();
				if (item.element.hasClass('no-social') || _forceClose) return;
				_this.timers.hover.start(function () {
					_this.FooBox.modal.element.removeClass('fbx-social-hidden');
					_this.show();
				}, _this.FooBox.options.social.hoverDelay);
			},
			/**
			 * Handles the standard jQuery mouseleave event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			mouseleave: function () {
				var item = _this.FooBox.items.current();
				if (item.element.hasClass('no-social') || _forceClose) return;
				_this.timers.hover.start(function () {
					_this.FooBox.modal.element.addClass('fbx-social-hidden');
					_this.hide();
				}, _this.FooBox.options.social.hoverDelay);
			},
			/**
			 * Handles the foobox.setupHtml event allowing this addon to create any required HTML.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupHtml: function (e) {
				if (!e.fb.options.social.enabled) return;
				var $social = $('<div class="fbx-social"></div>');
				e.fb.modal.find('.fbx-inner').append($social)
					.append($('<a class="fbx-social-toggle fbx-btn-transition fbx-btn-shadow"></a>')
						.append(FooBox.icons.get('social')));
			},
			/**
			 * Handles the foobox.setupOptions event allowing this addon to set its initial starting state.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			setupOptions: function (e) {
				if (!e.fb.options.social.enabled) return;
				e.fb.modal.find('.fbx-social').addClass(e.fb.options.social.position).addClass("fbx-fade-social");
			},
			/**
			 * Handles the foobox.close event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			close: function (e) {
				if (e.fb.options.social.onlyShowOnHover == true){	return;	}
				e.fb.modal.find('.fbx-social').addClass("fbx-fade-social");
			},
			/**
			 * Handles the foobox.beforeLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			beforeLoad: function (e) {
				if ($(e.fb.item.element).hasClass('no-social')) {
					e.fb.modal.removeClass('fbx-social-share');
					_forceClose = false;
					return;
				}
				if (e.fb.options.captions.onlyShowOnHover == true){	return;	}
				e.fb.modal.find('.fbx-social').addClass("fbx-fade-social");
			},
			/**
			 * Handles the foobox.afterLoad event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			afterLoad: function (e) {
				if ($(e.fb.item.element).hasClass('no-social')) {
					e.fb.modal.removeClass('fbx-social-share');
					_forceClose = false;
					return;
				}
				e.fb.modal.addClass('fbx-social-share');
				if (e.fb.modal.hasClass('fbx-phone') || e.fb.options.social.onlyShowOnHover == true){
					return;
				}

				if (e.fb.options.social.onlyShowOnClick == false){
					_this.show();
				}
			},
			/**
			 * Handles the foobox.onError event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onError: function (e) {
				e.fb.modal.find('.fbx-social').addClass("fbx-fade-social");
			}
		};

		this.generateShareUrl = function(item, network){
			var content = {
				foobox_share: _this.FooBox.options.social.nonce,
				network: network,
				post_id: _this.post_id,
				hash: location.hash,
				content_url: item.url,
				content_type: item.type,
				title: item.title != null ? item.title : '',
				description: item.description != null ? item.description : ''
			};
			if (content.post_id === -1){
				delete content.post_id;
			}
			return new FooBox.URL().params(content).hash('').toString();
		};

		/**
		 * Hides the social links.
		 */
		this.hide = function () {
			var item = _this.FooBox.items.current(),
				$social = _this.FooBox.modal.element.find('.fbx-social');

			if (!_this.FooBox.options.social.enabled || !item.social || (FooBox.isjQuery(item.element) && item.element.hasClass('no-social'))) {
				$social.addClass('fbx-hide-social');
				return;
			}
			$social.addClass("fbx-fade-social");
		};

		/**
		 * Shows the social links.
		 */
		this.show = function () {
			var item = _this.FooBox.items.current(),
				$social = _this.FooBox.modal.element.find('.fbx-social');

			if (!_this.FooBox.options.social.enabled || !item.social || _this.FooBox.modal.element.hasClass('fbx-social-hidden') || (FooBox.isjQuery(item.element) && item.element.hasClass('no-social'))) {
				$social.addClass('fbx-hide-social');
				return;
			}

			if (_this.FooBox.modal.element.hasClass('fbx-phone')){
				$social.one('click', _this.hide);
			}

			_this.update();
			$social.removeClass('fbx-fade-social');
		};

		this.update = function(){
			var o = _this.FooBox.options,
				item = _this.FooBox.items.current(),
				modal = _this.FooBox.modal.element;

			if (FooBox.inArray(item.type, o.social.excludes) === -1){
				var $social = modal.find('.fbx-social').empty();
				for (var i = 0; i < o.social.networks.length; i++) {
					var network = o.social.networks[i],
						url = _this.generateShareUrl(item, network);
					var $a = $('<a/>', {href: url, 'class': 'fbx-'+network, rel: 'nofollow', target: '_blank'})
						.data('network', network)
						.append(FooBox.icons.get(network));
					if (document.addEventListener && o.social.popup){
						$a.on('click', function(e){
							e.preventDefault();
							_this.popup(this.href, $.data(this, 'network'));
						});
					}
					$social.append($a);
				}
				if (o.social.download == true && item.type === 'image'){
					var $download = $('<a/>', {href: item.url, 'class': 'fbx-download', rel: 'nofollow', target: '_blank', download: ''})
						.data('network', 'download')
						.append(FooBox.icons.get('download'));
					$social.append($download);
				}
				if (o.social.mailto == true){
					var title = item.title != null && item.title != '' ? 'subject=' + encodeURIComponent(item.title) : null;
					var description = item.description != null && item.description != ''
							? 'body=' + encodeURIComponent(item.description + ' ' + (item.deeplink_url || item.url))
							: 'body=' + encodeURIComponent(item.deeplink_url || item.url);
					var href = 'mailto:?' + (title != null ? title + '&' : '') + description;
					var $mailto = $('<a/>', {href: href, 'class': 'fbx-email', rel: 'nofollow', target: '_blank'})
						.data('network', 'mailto')
						.append(FooBox.icons.get('email'));
					$social.append($mailto);
				}
			}
		};

		this.popup = function (url, network) {
			if (typeof url !== 'string') return;

			var o = _this.FooBox.options,
				size = o.social.popupSize[network] || {};

			var winHandle;
			var options = {
				toolbar: 'no',
				location: 'no',
				directories: 'no',
				status: 'no',
				menubar: 'no',
				scrollbars: 'no',
				resizable: 'yes',
				copyhistory: 'no',
				width: 600,
				height: 600
			};
			$.extend(options, size);
			options.left = window.screenX + (screen.width / 2) - (options.width / 2);
			options.top = (screen.height / 2) - (options.height / 2);

			var features = $.map(options, function(value, key){
				return key + '=' + value;
			}).join(',');

			winHandle = window.open(url, 'FooShare Popup', features);

			return $.Deferred(function(d){
				winHandle.addEventListener('DOMContentLoaded', function(){
					var timer = setInterval(function(){
						if (winHandle.closed){
							clearInterval(timer);
							d.resolve();
						}
					}, 500);
				});
			});
		}

	};

	FooBox.addons.register(FooBox.Social, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Swipe - An addon providing simple swipe support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the swipe addon. */
	var defaults = {
		swipe: {
			/** @type {boolean} - A Boolean indicating whether or not the swipe feature is enabled. */
			enabled: true,
			/** @type {number} - A Number indicating the minimum amount of pixels to travel before detecting a swipe. */
			min: 80
		}
	};

	/**
	 * The core logic for the FooBox.Swipe addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Swipe = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;
		/** @type {boolean} - A Boolean indicating whether or not the touchmove event is active. */
		this.isMoving = false;

		/**
		 * @type {FooBox.Swipe} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * @type {number} - Holds the starting page X position for the swipe.
		 * @private
		 */
		var startX;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on('foobox.initialized foobox.reinitialized', _this.handlers.initialized);
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off('foobox.initialized foobox.reinitialized', _this.handlers.initialized);
				}
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)){
					_this.FooBox.modal.element.off({
						'touchstart': _this.handlers.onTouchStart,
						'touchmove': _this.handlers.onTouchMove
					});
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (e.fb.options.swipe.enabled === true) {
					e.fb.modal.on('touchstart', _this.handlers.onTouchStart);
				}
			},
			/**
			 * Handles the standard jQuery touchstart event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			onTouchStart: function (e) {
				var touches = e.originalEvent.touches || e.touches;
				if (touches.length != 1 || !_this.FooBox.items.multiple()) { return; }
				startX = touches[0].pageX;
				_this.isMoving = true;
				_this.FooBox.modal.element.on('touchmove', _this.handlers.onTouchMove);
			},
			/**
			 * Handles the standard jQuery touchstart event.
			 * @param {jQuery.Event} e - A standard jQuery.Event object.
			 */
			onTouchMove: function (e) {
				if (!_this.isMoving) { return; }
				var touches = e.originalEvent.touches || e.touches;
				var x = touches[0].pageX;
				var dx = startX - x;
				if (Math.abs(dx) >= _this.FooBox.options.swipe.min) {
					_this.cancelTouch();
					if (dx > 0) {
						_this.FooBox.raise('foobox.swipeRight');
						_this.FooBox.modal.next();
					} else {
						_this.FooBox.raise('foobox.swipeLeft');
						_this.FooBox.modal.prev();
					}
				}
			}
		};

		/**
		 * Cancels the current touch event resetting the swipe property values.
		 */
		this.cancelTouch = function () {
			_this.FooBox.modal.element.off('touchmove', _this.handlers.onTouchMove);
			startX = null;
			_this.isMoving = false;
		};
	};

	FooBox.addons.register(FooBox.Swipe, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Pan - An addon providing simple panning support for the FooBox plugin
 * @version 1.0.0
 * @copyright Steven Usher & Brad Vincent 2016
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the pan addon. */
	var defaults = {
		pan: {
			/** @type {boolean} - A Boolean indicating whether or not the pan feature is enabled. */
			enabled: true,
			/** @type {boolean} - A Boolean indicating whether or not the overview is enabled. */
			showOverview: true,
			/** @type {string} - The class name used to position the overview within the modal. fbx-top-left, fbx-top-right, fbx-bottom-left, fbx-bottom-right */
			position: 'fbx-top-right'
		}
	};

	/**
	 * The core logic for the FooBox.Pan addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Pan = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.Pan} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		var active = false;

		var panning = false;

		/**
		 * @type {number} - Holds the starting page X position for the pan.
		 * @private
		 */
		var startX;

		/**
		 * @type {number} - Holds the starting page X position for the pan.
		 * @private
		 */
		var startY;

		/**
		 * @type {number} - Holds the starting scrollLeft position for the pan.
		 * @private
		 */
		var startScrollLeft;

		/**
		 * @type {number} - Holds the starting scrollTop position for the pan.
		 * @private
		 */
		var startScrollTop;

		var item = null;

		var $current = null;

		var $stage = null;

		var $overview = null;

		var $viewport = null;

		var ratio = 0;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on({
				'foobox.initialized foobox.reinitialized': _this.handlers.initialized
			});
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function () {
			_this.handlers.unbind(true);
		};

		this.setOverview = function(){
			var max_ov_w = parseInt($overview.css('max-width')),
				max_ov_h = parseInt($overview.css('max-height')),
				rx = max_ov_w / item.width,
				ry = max_ov_h / item.height;

			ratio = rx > ry ? ry : rx;

			var ovw = item.width * ratio,
				ovh = item.height * ratio;

			$overview.width(ovw).height(ovh).css('background-image', 'url(' + item.url + ')');
		};

		this.setViewport = function(){
			var cw = $stage.width(),
				ch = $stage.height(),
				vw = cw * ratio,
				vh = ch * ratio;
			$viewport.width(vw).height(vh);
		};

		this.disableCaptionImageClick = function(){

			var modal = _this.FooBox.modal.element,
				captions = FooBox.objects.get('addons', _this.FooBox, function(addon){
					return addon instanceof FooBox.Captions;
				});

			if (captions){
				modal.find('.fbx-item-current, .fbx-item-next').off('click.captions', captions.handlers.toggleCaptions);
			}

		};

		this.enableCaptionImageClick = function(){

			var modal = _this.FooBox.modal.element,
				captions = FooBox.objects.get('addons', _this.FooBox, function(addon){
					return addon instanceof FooBox.Captions;
				});

			if (captions){
				modal.find('.fbx-item-current, .fbx-item-next').on('click.captions', captions.handlers.toggleCaptions);
			}

		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function (all) {
				all = all || false;
				if (all) {
					_this.FooBox.element.off({'foobox.initialized foobox.reinitialized': _this.handlers.initialized});
				}
				_this.FooBox.element.off({
					'foobox.afterLoad': _this.handlers.onAfterLoad,
					'foobox.showOverflow': _this.handlers.onShowOverflow,
					'foobox.hideOverflow foobox.beforeLoad': _this.handlers.onHideOverflow
				});
				if (_this.FooBox.modal instanceof FooBox.Modal && FooBox.isjQuery(_this.FooBox.modal.element)) {
					_this.FooBox.modal.element.find('.fbx-stage').off({
						'mousedown': _this.handlers.onMouseDown,
						'mousemove': _this.handlers.onMouseMove,
						'mouseup': _this.handlers.onMouseUp
					}).removeClass('fbx-pannable fbx-panning');
				}
			},
			/**
			 * Handles the foobox.initialized event binding the various events needed by this addon to function.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			initialized: function (e) {
				_this.handlers.unbind();
				if (e.fb.options.pan.enabled === true) {
					_this.FooBox.element.on({
						'foobox.close': _this.handlers.onMouseUp,
						'foobox.afterLoad': _this.handlers.onAfterLoad,
						'foobox.showOverflow': _this.handlers.onShowOverflow,
						'foobox.hideOverflow foobox.beforeLoad': _this.handlers.onHideOverflow
					});
				}
			},
			onAfterLoad: function (e) {
				if (active === true && e.fb.item.handler.type == 'image'){
					_this.setOverview();
					_this.setViewport();
				}
			},
			onShowOverflow: function (e) {
				if (e.fb.item.handler.type == 'image'){
					if (active === true){
						_this.setOverview();
						_this.setViewport();
					} else {
						active = true;
						_this.disableCaptionImageClick();
						item = e.fb.item;


						$stage = _this.FooBox.modal.element.addClass('fbx-pannable-item').find('.fbx-stage').on({
							'mousedown': _this.handlers.onMouseDown,
							'mousemove': _this.handlers.onMouseMove,
							'mouseup mouseleave': _this.handlers.onMouseUp
						}).addClass('fbx-pannable');

						$current = e.fb.container;

						if (e.fb.options.pan.showOverview === true) {
							$overview = $('<div/>', {'class': 'fbx-pan-overview'}).addClass(e.fb.options.pan.position).on('click', _this.handlers.onOverviewClick).appendTo($stage);
							$viewport = $('<div/>', {'class': 'fbx-pan-viewport'}).appendTo($overview);
							$current.on('scroll', _this.handlers.onScroll);
							_this.setOverview();
							_this.setViewport();
						}
					}
				}
			},
			onHideOverflow: function (e) {
				if (active === true){
					active = false;
					_this.enableCaptionImageClick();

					if (e.fb.options.pan.showOverview === true) {
						$overview.remove();
					}
					$current.off('scroll', _this.handlers.onScroll);
					$stage.off({
						'mousedown': _this.handlers.onMouseDown,
						'mousemove': _this.handlers.onMouseMove,
						'mouseup mouseleave': _this.handlers.onMouseUp
					}).removeClass('fbx-pannable fbx-panning');

					_this.FooBox.modal.element.removeClass('fbx-pannable-item');
				}
			},
			onMouseDown: function (e) {
				if (active === true) {
					e.preventDefault();
					$stage.addClass('fbx-panning');
					var current = $current.get(0);
					panning = true;
					startX = e.pageX;
					startY = e.pageY;
					startScrollLeft = current.scrollLeft;
					startScrollTop = current.scrollTop;
				}
			},
			onMouseMove: function (e) {
				if (panning === true && active === true) {
					var current = $current.get(0);
					current.scrollLeft = startScrollLeft - (e.pageX - startX);
					current.scrollTop = startScrollTop - (e.pageY - startY);
				}
			},
			onMouseUp: function () {
				if (active === true) {
					panning = false;
					$stage.removeClass('fbx-panning');
					startX = 0;
					startY = 0;
					startScrollLeft = 0;
					startScrollTop = 0;
				}
			},
			onScroll: function(){
				var current = $current.get(0),
					t = (current.scrollTop/item.height) * 100,
					l = (current.scrollLeft/item.width) * 100;
				$viewport.css({top: t + '%', left: l + '%'});
			},
			onOverviewClick: function(e){
				var offset = $(this).offset(),
					x = (e.pageX - offset.left) / ratio,
					y = (e.pageY - offset.top) / ratio,
					left = x - ($current.width() / 2),
					top = y - ($current.height() / 2);

				$current.animate({scrollTop: top, scrollLeft: left}, 300);
			}
		};
	};

	FooBox.addons.register(FooBox.Pan, defaults);

})(jQuery, window.FooBox);;/* jslint devel: true, browser: true, unparam: true, debug: false, es5: true, white: false, maxerr: 1000 */
/**!
 * FooBox Wordpress - An addon providing Wordpress support for the FooBox plugin
 * @version 2.0.0
 * @copyright Steven Usher & Brad Vincent 2013
 */
(function ($, FooBox) {

	/** @type {Object} - Contains the default option values for the fullscreen addon. */
	var defaults = {
		/** @type {Object} - An object containing fullscreen related options. */
		wordpress: {
			/** @type {boolean} - A Boolean indicating whether or not the fullscreen feature is enabled. */
			enabled: false
		}
	};

	/**
	 * The core logic for the FooBox.Wordpress addon.
	 * @param {FooBox.Instance} instance - The parent FooBox instance for this addon.
	 * @constructor
	 */
	FooBox.Wordpress = function (instance) {

		/** @type {FooBox.Instance} - The parent FooBox instance for this addon. */
		this.FooBox = instance;

		/**
		 * @type {FooBox.Wordpress} - Hold a reference to this instance of the object to avoid scoping issues.
		 * @private
		 */
		var _this = this;

		/**
		 * This method is called before the core FooBox plugin is created. This allows addons to bind to all events being raised, including those raised during initialization.
		 * @param {jQuery} element - The jQuery element the parent FooBox instance will be created and raising events on.
		 * @param {Object} options - The options used to initialize the FooBox instance.
		 */
		this.preinit = function (element) {
			_this.handlers.unbind(true);
			element.on('foobox.createCaption', _this.handlers.onCreateCaption);
		};

		/**
		 * This method is called when the core FooBox plugin is destroyed. This allows addons to unbind all events and remove any additional DOM elements added outside the modal.
		 */
		this.destroy = function(){
			_this.handlers.unbind(true);
		};

		/** @namespace - Contains all the event handlers used by this addon. */
		this.handlers = {
			/**
			 * This unbinds event handlers used by this addon.
			 * @param {boolean} [all=false] - Whether or not to unbind all handlers or just those used during init and reinit.
			 */
			unbind: function(all){
				all = all || false;
				if (all){
					_this.FooBox.element.off('foobox.createCaption', _this.handlers.onCreateCaption);
				}
			},
			/**
			 * Handles the foobox.createCaption event.
			 * @param {jQuery.Event} e - A jQuery.Event object augmented with additional FooBox properties.
			 */
			onCreateCaption: function (e) {
				var opt = e.fb.options,
					cont = e.fb.instance.element,
					el = e.fb.item.element,
					caption = '', 
					title = e.fb.item.title,
					desc = e.fb.item.description;

				if (opt.wordpress.enabled != true || !FooBox.isjQuery(el)) { return; }
				var t = el.data('captionTitle') || el.data('title'),
					d = el.data('captionDesc') || el.data('description');
				if (cont.hasClass('gallery')) {
					if (opt.captions.overrideTitle === false) {
						title = t
							|| el.parents(".gallery-item:first").find(".wp-caption-text:first").html()
							|| el.parents(".gallery-item:first").find(".gallery-caption:first").html()
							|| title
							|| '';
					}
					if (opt.captions.overrideDesc === false) {
						desc = d
							|| el.find('img').attr('alt')
							|| desc
							|| '';
					}
				} else if (cont.hasClass('wp-caption') || el.hasClass('wp-caption')) {
					if (opt.captions.overrideTitle === false) {
						title = t
							|| el.find('img').attr('title')
							|| el.parents(".wp-caption:first").find(".wp-caption-text:first").html()
							|| title
							|| '';
					}
					if (opt.captions.overrideDesc === false) {
						desc = d
							|| el.find('img').attr('alt')
							|| desc
							|| '';
					}
				} else if (el.parents(".wp-caption:first").length > 0) {
					if (opt.captions.overrideTitle === false) {
						title = t
							|| el.parents(".wp-caption:first").find('img').attr('title')
							|| el.parents(".wp-caption:first").find(".wp-caption-text:first").html()
							|| title
							|| '';
					}
					if (opt.captions.overrideDesc === false) {
						desc = d
							|| el.parents(".wp-caption:first").find('img').attr('alt')
							|| desc
							|| '';
					}
				} else if (cont.hasClass('tiled-gallery')) {
					if (opt.captions.overrideTitle === false) {
						title = t
							|| el.parents(".tiled-gallery-item:first").find(".tiled-gallery-caption").html()
							|| el.find('img').data('image-title')
							|| el.find('img').attr('title')
							|| title
							|| '';
					}
					if (opt.captions.overrideDesc === false) {
						desc = d
							|| FooBox.trim(el.find('img').data('image-description')).replace(/(^<p>)|(<\/p>$)/ig, '') //replace opening and closing P tags that are generated by tiled gallery
							|| desc
							|| '';
					}
				} else if (cont.hasClass('wp-block-gallery') || cont.hasClass('wp-block-image')) {
					if (opt.captions.overrideTitle === false) {
						title = t
								|| el.parents("figure:first").find("figcaption").html()
								|| title
								|| '';
					}
					if (opt.captions.overrideDesc === false) {
						desc = d || desc || '';
					}
				} else {
					return;
				}

				// ensure working with strings
				title = title + '';
				desc = desc + '';

				title = title || '';
				desc = desc || '';

				var $tmp = $('<div/>'),
					text_title = $tmp.html(title).text(),
					text_desc = $tmp.html(desc).text();

				if (FooBox.trim(title) === FooBox.trim(desc) || text_title === text_desc) desc = null;

				caption = typeof title === 'string' && title.length > 0 ? '<div class="fbx-caption-title">' + title + '</div>' : caption;
				caption = typeof desc === 'string' && desc.length > 0 ? caption + '<div class="fbx-caption-desc">' + desc + '</div>' : caption;

				e.fb.item.title = title;
				e.fb.item.description = desc;
				e.fb.item.caption = caption;
			}
		};
	};

	FooBox.addons.register(FooBox.Wordpress, defaults);

})(jQuery, window.FooBox);;(function($, _, cfg){

	if ( !cfg ){
		console.log("No configuration for FooBox found.");
		return;
	}

	// handle merging of any options set using the Custom JS Options output
	if ( cfg.o && cfg.customOptions ){
		$.extend(true, cfg.o, cfg.customOptions);
	}

	// this function should be called whenever the page is loaded or content has changed that may contain FooBox targets
	cfg.init = function(){
		// remove all existing fbx-link classes so the elements may be parsed again
		$(".fbx-link").removeClass("fbx-link");

		// handle the Custom JavaScript (Pre) options' output
		if ( FooBox.isFn( cfg.pre ) ){
			try { cfg.pre( $ ); }
			catch(err){ console.error(err); }
		}

		// try disable thickbox, maps back to the Disable Other Lightboxes option
		if ( cfg.disableOthers ){
			$("a.thickbox").removeClass("thickbox").unbind("click");
			$("#TB_overlay, #TB_load, #TB_window").remove();
		}

		// loop through all selectors and init FooBox using the options
		$.each( cfg.selectors, function(i, selector){
			$( selector ).foobox( cfg.o );
		} );

		// try disable prettyphoto, maps back to the Disable Other Lightboxes option
		if ( cfg.disableOthers ){
			$(".fbx-link").unbind(".prettyphoto").unbind(".fb");
		}

		// handle the Custom JavaScript (Post) and Custom Caption Code options' output
		if ( FooBox.isFn( cfg.post ) ){
			try { cfg.post( $ ); }
			catch(err){ console.error(err); }
		}
	};

	/**
	 * Executed once the DOM is ready this function auto initializes FooBox using the
	 * window.FOOBOX configuration object output by the WordPress component of the plugin.
	 *
	 * This method should only be called once, use FooBox.init once initialized.
	 */
	_.once = function(){
		var $body = $( "body" );
		cfg.init();
		$( document ).trigger( "foobox-after-init" );
		$body.on( "post-load", cfg.init );
	};

	// handle the Use Custom Ready Event option to bypass errors thrown by other plugins
	if ( cfg.ready ){
		_.ready( _.once );
	} else {
		$( _.once );
	}

	// handle the Custom Extra JS options' output
	if ( FooBox.isFn( cfg.custom ) ){
		try { cfg.custom( $ ); }
		catch(err){ console.error(err); }
	}

})( jQuery, window.FooBox, window.FOOBOX );