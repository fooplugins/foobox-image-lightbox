(function (FOOBOX_FOTOMOTO, $, undefined) {

	FOOBOX_FOTOMOTO.waitIntervalId = null;
	FOOBOX_FOTOMOTO.waitItem = null;
	FOOBOX_FOTOMOTO.waitContainer = null;

	FOOBOX_FOTOMOTO.show = function(item, container) {
		if (typeof(FOTOMOTO) != 'undefined') {
			if (item.image) {
				FOTOMOTO.runInit2(item.image, container.get(0), !0);
			}
		} else {
			FOOBOX_FOTOMOTO.waitItem = item;
			FOOBOX_FOTOMOTO.waitContainer = container;
			FOOBOX_FOTOMOTO.retry();
		}
	};

	FOOBOX_FOTOMOTO.retry = function() {
		if (typeof(FOTOMOTO) != 'undefined') {

			FOOBOX_FOTOMOTO.show(FOOBOX_FOTOMOTO.waitItem, FOOBOX_FOTOMOTO.waitContainer);

			if (FOOBOX_FOTOMOTO.waitIntervalId !== null) {
				clearInterval(FOOBOX_FOTOMOTO.waitIntervalId);
			}

		} else if (FOOBOX_FOTOMOTO.waitIntervalId === null) {

			//retry again in 1000ms
			FOOBOX_FOTOMOTO.waitIntervalId = setInterval(FOOBOX_FOTOMOTO.retry, 1000);

		}
	};

}(window.FOOBOX_FOTOMOTO = window.FOOBOX_FOTOMOTO || {}, jQuery));